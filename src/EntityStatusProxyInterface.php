<?php

namespace Drupal\cms_content_sync;

/**
 * Provides an interface for defining Sync entity entities with all
 * functionality that's available for groups of status entities that span
 * multiple pools.
 *
 * @ingroup cms_content_sync_entity_status
 */
interface EntityStatusProxyInterface
{
    public function resetStatus();

    /**
     * Get the entity this entity status belongs to.
     *
     * @return \Drupal\Core\Entity\EntityInterface
     */
    public function getEntity();

    /**
     * Returns the information if the entity has been pushed before but the last push date was reset.
     *
     * @param bool $set
     *                  Optional parameter to set the value for LastPushReset
     *
     * @return bool
     */
    public function wasLastPushReset($set = null);

    /**
     * Returns the information if the entity has been pulled before but the last import date was reset.
     *
     * @param bool $set
     *                  Optional parameter to set the value for LastPullReset
     *
     * @return bool
     */
    public function wasLastPullReset($set = null);

    /**
     * Returns the information if the last push of the entity failed.
     *
     * @param bool       $set
     *                            Optional parameter to set the value for PushFailed
     * @param bool       $soft
     *                            A soft fail- this was intended according to configuration. But the user might want to know why to debug different
     *                            expectations.
     * @param null|array $details
     *                            If $set is TRUE, you can provide additional details on why the push failed. Can be gotten via
     *                            ->whyDidPushFail()
     *
     * @return bool
     */
    public function didPushFail($set = null, $soft = false, $details = null);

    /**
     * Get the details provided to ->didPushFail( TRUE, ... ) before.
     *
     * @return null|array
     */
    public function whyDidPushingFail();

    /**
     * Returns the information if the last pull of the entity failed.
     *
     * @param bool       $set
     *                            Optional parameter to set the value for PullFailed
     * @param bool       $soft
     *                            A soft fail- this was intended according to configuration. But the user might want to know why to debug different
     *                            expectations.
     * @param null|array $details
     *                            If $set is TRUE, you can provide additional details on why the pull failed. Can be gotten via
     *                            ->whyDidPullFail()
     *
     * @return bool
     */
    public function didPullFail($set = null, $soft = false, $details = null);

    /**
     * Get the details provided to ->didPullFail( TRUE, ... ) before.
     *
     * @return null|array
     */
    public function whyDidPullingFail();

    /**
     * Returns the information if the entity has been chosen by the user to
     * be pushed with this flow and pool.
     *
     * @param bool $set
     *                            Optional parameter to set the value for PushEnabled
     * @param bool $setDependency
     *                            Optional parameter to set the value for DependencyPushEnabled
     *
     * @return bool
     */
    public function isPushEnabled($set = null, $setDependency = null);

    /**
     * Returns the information if the entity has been chosen by the user to
     * be pushed with this flow and pool.
     *
     * @return bool
     */
    public function isManualPushEnabled();

    /**
     * Returns the information if the entity has been pushed with this flow and
     * pool as a dependency.
     *
     * @return bool
     */
    public function isPushedAsDependency();

    /**
     * Returns the information if the user override the entity locally.
     *
     * @param bool $set
     *                         Optional parameter to set the value for EditOverride
     * @param bool $individual
     *
     * @return bool
     */
    public function isOverriddenLocally($set = null, $individual = false);

    /**
     * Returns the information if the entity has originally been created on this
     * site.
     *
     * @param bool  $set
     *                          Optional parameter to set the value for IsSourceEntity
     * @param mixed $individual
     *
     * @return bool
     */
    public function isSourceEntity($set = null, $individual = false);

    /**
     * Returns the information if the user allowed the push.
     *
     * @param bool $set
     *                  Optional parameter to set the value for UserEnabledPush
     *
     * @return bool
     */
    public function didUserEnablePush($set = null);

    /**
     * Returns the information if the entity is deleted.
     *
     * @param bool $set
     *                  Optional parameter to set the value for Deleted
     *
     * @return bool
     */
    public function isDeleted($set = null);

    /**
     * Returns whether the entity was pushed embedded into another parent entity.
     * This is always done for field collections but can also be enabled for other
     * entities like paragraphs or media entities. This can save a lot of requests
     * when entities aren't all syndicated individually.
     *
     * @param bool $set
     *                  Optional parameter to set the value for the flag
     *
     * @return bool
     */
    public function wasPushedEmbedded($set = null);

    /**
     * Returns whether the entity was pulled embedded in another parent entity.
     * This is always done for field collections but can also be enabled for other
     * entities like paragraphs or media entities. This can save a lot of requests
     * when entities aren't all syndicated individually.
     *
     * @param bool $set
     *                  Optional parameter to set the value for the flag
     *
     * @return bool
     */
    public function wasPulledEmbedded($set = null);

    /**
     * If an entity is pushed or pulled embedded into another entity, we store
     * that parent entity here. This is required so that at a later point we can
     * still force pull and force push the embedded entity although it doesn't
     * exist individually.
     * This is also required to reset e.g. embedded paragraphs after the
     * "Overwrite content locally" checkbox is unchecked.
     *
     * @param string $type
     * @param string $uuid
     */
    public function setParentEntity($type, $uuid);

    /**
     * See above.
     *
     * @return null|\Drupal\Core\Entity\EntityInterface
     */
    public function getParentEntity();

    /**
     * Returns the timestamp for the last pull.
     *
     * @return int
     */
    public function getLastPull();

    /**
     * Set the last pull timestamp.
     *
     * @param int $timestamp
     */
    public function setLastPull($timestamp);

    /**
     * Returns the UUID of the entity this information belongs to.
     *
     * @return string
     */
    public function getUuid();

    /**
     * Returns the entity type name of the entity this information belongs to.
     *
     * @return string
     */
    public function getEntityTypeName();

    /**
     * Returns the timestamp for the last push.
     *
     * @return int
     */
    public function getLastPush();

    /**
     * Set the last pull timestamp.
     *
     * @param int $timestamp
     */
    public function setLastPush($timestamp);

    /**
     * Get the flow.
     *
     * @return Flow
     */
    public function getFlow();

    /**
     * Returns the entity type version.
     *
     * @return string
     */
    public function getEntityTypeVersion();

    /**
     * Set the last pull timestamp.
     *
     * @param string $version
     */
    public function setEntityTypeVersion($version);

    /**
     * Returns the entity's source url.
     *
     * @return string
     */
    public function getSourceUrl();

    /**
     * Provide the entity's source url.
     *
     * @param string $url
     */
    public function setSourceUrl($url);

    /**
     * Get a previously saved key=>value pair.
     *
     * @see self::setData()
     *
     * @param null|string|string[] $key
     *                                  The key to retrieve
     *
     * @return mixed whatever you previously stored here or NULL if the key
     *               doesn't exist
     */
    public function getData($key = null);

    /**
     * Set a key=>value pair.
     *
     * @param string|string[] $key
     *                               The key to set (for hierarchical usage, provide
     *                               an array of indices
     * @param mixed           $value
     *                               The value to set. Must be a valid value for Drupal's
     *                               "map" storage (so basic types that can be serialized).
     */
    public function setData($key, $value);

    /**
     * @return null|string
     */
    public function getEntityPushHash();

    /**
     * @param string $hash
     */
    public function setEntityPushHash($hash);

    /**
     * Save the status entities.
     */
    public function save();

    /**
     * Get all known translation source URLs.
     *
     * @return null|array
     */
    public function getAllTranslationSourceUrls();

    /**
     * Get the source URL for the entity in the given language.
     *
     * @param string $language the language code to get the source URL for
     * @param bool $return_default_if_null if TRUE, will return $this->getSourceUrl() if there's no more specific URL available for the given translation language
     *
     * @return null|string
     */
    public function getTranslationSourceUrl(string $language, $return_default_if_null = true);

    /**
     * Set the source URL for the entity in the given language.
     *
     * @param string $language the language code to Set the source URL for
     * @param string $url the URL to set
     */
    public function setTranslationSourceUrl(string $language, string $url);
}
