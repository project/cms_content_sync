<?php

namespace Drupal\cms_content_sync\Helper;

use Drupal\cms_content_sync\Controller\FlowControllerSimple;
use Drupal\cms_content_sync\Entity\Flow;

class SimpleFlowSetupHelper
{
    /**
     * @var Flow
     */
    protected $flow;

    public function __construct(Flow $flow)
    {
        $this->flow = $flow;
    }

    public function save()
    {
        $this->flow->save();

        return $this;
    }

    public function getFlow()
    {
        return $this->flow;
    }

    public function enableBundle(string $entity_type_name, string $bundle, bool $dependency = false, ?array $filter_by_tags = null)
    {
        $this->flow->simple_settings['entityTypeSettings'][$entity_type_name]['perBundle'][$bundle] = [
            'mode' => $dependency ? FlowControllerSimple::MODE_DEPENDENT : 'default',
        ];
        if (!empty($filter_by_tags)) {
            /**
             * @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
             */
            $entityFieldManager = \Drupal::service('entity_field.manager');
            $fields = $entityFieldManager->getFieldDefinitions($entity_type_name, $bundle);
            foreach ($fields as $field_name => $field) {
                if ('entity_reference' == $field->getType() && 'taxonomy_term' == $field->getSetting('target_type')) {
                    $bundles = $field->getSetting('target_bundles');
                    if (is_array($bundles)) {
                        $terms = [];
                        foreach ($filter_by_tags as $tag) {
                            if (in_array($tag->bundle(), $bundles)) {
                                $terms[] = [
                                    'namespaceMachineName' => 'taxonomy_term',
                                    'machineName' => $tag->bundle(),
                                    'remoteUuid' => $tag->uuid(),
                                ];
                            }
                        }
                        if (count($terms)) {
                            $this->flow->simple_settings['entityTypeSettings'][$entity_type_name]['perBundle'][$bundle]['filterByReference'][] = [
                                'type' => 'includes-reference',
                                'fieldMachineName' => $field_name,
                                'values' => $terms,
                            ];
                        }
                    }
                }
            }
        }
    }

    public function setUpdateBehavior(string $behavior)
    {
        $this->flow->simple_settings['updateBehavior'] = $behavior;
    }
}
