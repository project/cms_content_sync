<?php

namespace Drupal\cms_content_sync\Controller;

use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\Entity\Pool;
use Drupal\cms_content_sync\Form\JsonForm;
use Drupal\cms_content_sync\Form\MigrationForm;
use Drupal\cms_content_sync\Plugin\Type\EntityHandlerPluginManager;
use Drupal\cms_content_sync\SyncCoreInterface\DrupalApplication;
use Drupal\cms_content_sync\SyncCoreInterface\SyncCoreFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use EdgeBox\SyncCore\Exception\UnauthorizedException;
use EdgeBox\SyncCore\Interfaces\Embed\IEmbedService;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Embed provides helpers to embed Sync Core functionality into the site.
 */
class Embed extends ControllerBase
{
    protected $core;
    protected $settings;
    protected $params;
    protected $embedService;

    public function syndicationDashboard()
    {
        $this->init();
        if (!$this->settings->getSiteUuid()) {
            \Drupal::messenger()->addWarning($this->t('Please register your site first.'));

            return RedirectResponse::create(Url::fromRoute('cms_content_sync.site', ['absolute' => true])->toString());
        }
        $embed = $this->embedService->syndicationDashboard([
            'selectedFlowMachineName' => isset($this->params['flow']) ? $this->params['flow'] : null,
            'startNew' => isset($this->params['startNew']) ? 'true' === $this->params['startNew'] : false,
            'forbidStartNew' => !Migration::alwaysUseV2(),
            'query' => $this->params,
        ]);

        return $this->run($embed, DrupalApplication::get()->getSiteUuid() ? ['form' => \Drupal::formBuilder()->getForm(MigrationForm::class)] : []);
    }

    public function migrate()
    {
        $this->init();

        $all_pools = Pool::getAll();
        $pools = [];
        foreach ($all_pools as $id => $pool) {
            $pools[] = [
                'machineName' => $pool->id(),
                'name' => $pool->label(),
                'exported' => $pool->v2Ready(),
            ];
        }

        $all_flows = Flow::getAll();
        $flows = [];
        $pushes = false;
        foreach ($all_flows as $id => $flow) {
            if (Flow::TYPE_PULL !== $flow->type) {
                $pushes = true;
            }
            $flows[] = [
                'machineName' => $flow->id(),
                'name' => $flow->label(),
                'exported' => $flow->v2Ready(),
            ] + Migration::getFullFlowStatus($flow);
        }

        $health_module_enabled = \Drupal::moduleHandler()->moduleExists('cms_content_sync_health');

        $embed = $this->embedService->migrate($this->params + [
            'pools' => $pools,
            'flows' => $flows,
            'migrated' => false,
            'settings' => [
                'mustEnableHealthSubmodule' => $pushes && !$health_module_enabled,
                'pushToV2Url' => Url::fromRoute('view.content_sync_entity_status.entity_status_overview', [], ['query' => ['v2' => 'yes'], 'absolute' => true])->toString(),
                'pullManuallyFromV2Url' => $health_module_enabled ? Url::fromRoute('entity.cms_content_sync.content', [], ['query' => ['v2' => 'yes'], 'absolute' => true])->toString() : null,
                'switched' => Migration::alwaysUseV2(),
            ],
        ]);

        return $this->run($embed, DrupalApplication::get()->getSiteUuid() ? ['form' => \Drupal::formBuilder()->getForm(MigrationForm::class)] : []);
    }

    public function site()
    {
        $this->init(true);

        // Already registered if this exists.
        $uuid = $this->settings->getSiteUuid();

        $force = empty($this->params['force']) ? null : $this->params['force'];

        if (IEmbedService::MIGRATE === $force || (!Migration::useV2() && empty($force))) {
            return $this->migrate();
        }

        $this->params['migrated'] = Migration::didMigrate();

        $this->params['domains'] = _cms_content_sync_get_domains();

        $embed = $uuid && (empty($this->params['force']) || IEmbedService::REGISTER_SITE !== $this->params['force'])
        ? $this->embedService->siteRegistered($this->params)
        : $this->embedService->registerSite($this->params);

        if ($uuid && empty($this->params)) {
            $step = Migration::getActiveStep();
            if (Migration::STEP_DONE !== $step) {
                $link = \Drupal::l(t('migrate now'), \Drupal\Core\Url::fromRoute('cms_content_sync_flow.migration'));
                \Drupal::messenger()->addMessage(t('You have Sync Cores from our old v1 release. Support for this Sync Core will end on December 31st, 2021. Please @link.', ['@link' => $link]), 'warning');
            }
        }

        try {
            $result = $this->run($embed);

            // Site was just registered.
            if (!empty($this->params['uuid'])) {
                // Clear feature flag cache.
                SyncCoreFactory::clearCache();

                // Report the domains to the Sync Core right after.
                _cms_content_sync_report_domains();
            }

            return $result;
        }
        // The site registration JWT expires after only 5 minutes, then the user must get a new one.
        catch (UnauthorizedException $e) {
            \Drupal::messenger()->addMessage('Your registration token expired. Please try again.', 'warning');

            $url = \Drupal::request()->getRequestUri();
            // Remove invalid query params so the user can try again.
            $url = explode('?', $url)[0];

            return RedirectResponse::create($url);
        }
    }

    public function flowEditForm(Flow $cms_content_sync_flow)
    {
        if (Flow::VARIANT_PER_BUNDLE === $cms_content_sync_flow->variant) {
            $request = \Drupal::request();
            // Drupal is a little funny in that it will ignore whatever you pass to the redirect response and
            // instead use the destination query parameter if given. So we have to remove it.
            if ($request->query->get('destination')) {
                $destination = $request->query->get('destination');
                $request->query->remove('destination');
            }

            return RedirectResponse::create(Url::fromRoute('entity.cms_content_sync_flow.edit_form_advanced', ['cms_content_sync_flow' => $cms_content_sync_flow->id], ['absolute' => true, 'query' => empty($destination) ? [] : ['destination' => $destination]])->toString());
        }

        return $this->flowForm($cms_content_sync_flow);
    }

    public function flowForm(?Flow $flow)
    {
        $this->init();

        if (!$this->settings->getSiteUuid()) {
            \Drupal::messenger()->addWarning($this->t('Please register your site first.'));

            return RedirectResponse::create(Url::fromRoute('cms_content_sync.site', ['absolute' => true])->toString());
        }

        $controller = $flow ? $flow->getController() : null;

        $embed = $this->embedService->flowForm($controller && $controller instanceof FlowControllerSimple ? $controller->getFormValues() : FlowControllerSimple::getFormValuesForNewFlow($flow));

        return $this->run($embed, [
            'form' => \Drupal::formBuilder()->getForm(JsonForm::class),
        ]);
    }

    public function pullDashboard()
    {
        $this->init();

        $embed = $this->embedService->pullDashboard([
            'query' => $this->params,
        ]);

        return $this->run($embed);
    }

    public function nodeStatus(NodeInterface $node)
    {
        $this->init();

        $user = \Drupal::currentUser();

        $params = [
            'configurationAccess' => $user->hasPermission('administer cms content sync'),
            'namespaceMachineName' => $node->getEntityTypeId(),
            'machineName' => $node->bundle(),
        ];

        if (EntityHandlerPluginManager::mapById($node->getEntityType())) {
            $params['remoteUniqueId'] = $node->id();
        } else {
            $params['remoteUuid'] = $node->uuid();
        }

        if (!Migration::alwaysUseV2()) {
            \Drupal::messenger()->addWarning($this->t('Please switch your site to use the new Sync Core to enable this feature.'));
        }

        $embed = $this->embedService->entityStatus($params);

        return $this->run($embed);
    }

    public function updateStatusBox(EntityInterface $entity)
    {
        $this->init();

        $params = [
            'namespaceMachineName' => $entity->getEntityTypeId(),
            'machineName' => $entity->bundle(),
        ];

        if (EntityHandlerPluginManager::mapById($entity->getEntityType())) {
            $params['remoteUniqueId'] = $entity->id();
        } else {
            $params['remoteUuid'] = $entity->uuid();
        }

        if (!Migration::alwaysUseV2()) {
            return [];
        }

        $embed = $this->embedService->updateStatusBox($params);

        return $this->run($embed, [], 'line');
    }

    protected function init($expectJwtWithSyncCoreUrl = false)
    {
        $this->params = \Drupal::request()->query->all();

        if ($expectJwtWithSyncCoreUrl) {
            if (isset($this->params['uuid'], $this->params['jwt'])) {
                $tks = explode('.', $this->params['jwt']);
                list(, $bodyb64) = $tks;
                $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64));
                if (!empty($payload->syncCoreBaseUrl)) {
                    DrupalApplication::get()->setSyncCoreUrl($payload->syncCoreBaseUrl);
                }
            }
        }

        $this->core = SyncCoreFactory::getDummySyncCoreV2();

        $this->settings = ContentSyncSettings::getInstance();

        $this->embedService = $this->core->getEmbedService();
    }

    protected function run($embed, $extras = [], $size = 'page')
    {
        $result = $embed->run();

        $redirect = $result->getRedirectUrl();

        if ($redirect) {
            return RedirectResponse::create($redirect);
        }

        $html = $result->getRenderedHtml();

        // As the are issuing JWTs that are included in the render array, we don't
        // want to cache anything.
        \Drupal::service('page_cache_kill_switch')->trigger();

        $style = 'line' === $size ? 'margin-top: 3px;' : 'margin-top: 20px;';

        // TODO: Put this in a theme file.
        return [
            '#type' => 'inline_template',
            '#template' => '<div className="content-sync-embed-wrapper" style="'.$style.'">{{ html|raw }}</div>',
            '#context' => [
                'html' => $html,
            ],
            '#cache' => [
                'max-age' => 0,
            ],
            '#attached' => [
                'library' => [
                    'core/jquery',
                ],
            ],
        ] + $extras;
    }
}
