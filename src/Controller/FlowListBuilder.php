<?php

namespace Drupal\cms_content_sync\Controller;

use Drupal\cms_content_sync\Entity\Flow;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Flow.
 */
class FlowListBuilder extends ConfigEntityListBuilder
{
    /**
     * Constructs a new EntityListBuilder object.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface    $entity_type
     *                                                                   The entity type definition
     * @param \Drupal\Core\Entity\EntityStorageInterface $storage
     *                                                                   The entity storage class
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *                                                                   The config factory
     */
    public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ConfigFactoryInterface $config_factory)
    {
        parent::__construct($entity_type, $storage);
        $this->configFactory = $config_factory;
    }

    /**
     * {@inheritdoc}
     */
    public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type)
    {
        return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('config.factory')
    );
    }

    /**
     * {@inheritdoc}
     */
    public function buildHeader()
    {
        $header['name'] = $this->t('Synchronization');
        $header['id'] = $this->t('Machine name');
        $header['status'] = $this->t('Status');

        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity)
    {
        /**
         * @var \Drupal\cms_content_sync\Entity\Flow $entity
         */
        $row['name'] = $entity->label();
        $row['id'] = $entity->id();

        $config = $this->configFactory->get('cms_content_sync.flow.'.$entity->id());
        $overwritten = $config->get('status') != $entity->status();

        $active = $overwritten ? !$entity->status() : $entity->status();
        $status = $active ? $this->t('Active') : $this->t('Inactive');

        if ($overwritten) {
            $status .= ' <i>('.$this->t('Overwritten').')</i>';
        }

        if ($active) {
            if ($entity->getController()->needsEntityTypeUpdate()) {
                $status .= ' <i>('.$this->t('Requires export').')</i>';
            }
        }

        $row['status'] = new FormattableMarkup($status, []);

        return $row + parent::buildRow($entity);
    }

    public function render()
    {
        $build = parent::render();
        $status_overwritten = false;
        $version_mismatch = false;
        $rows = $build['table']['#rows'];

        // Check if the site has already been registered.
        $settings = ContentSyncSettings::getInstance();
        if (Migration::alwaysUseV2() && !$settings->getSiteUuid()) {
            $link = Link::fromTextAndUrl($this->t('registered'), Url::fromRoute('cms_content_sync.site'))->toString();
            $this->messenger()->addWarning($this->t('The site needs to be @link before creating a flow.', ['@link' => $link]));
        }

        $build['advanced'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Advanced'),
            'add_form_advanced' => [
                '#type' => 'link',
                '#title' => $this->t('Add advanced Flow (not recommended)'),
                '#url' => Url::fromRoute('entity.cms_content_sync_flow.add_form_advanced'),
                '#attributes' => [
                    'class' => ['button', 'button-action', 'button--small'],
                ],
            ],
            'copy_remote' => [
                '#type' => 'link',
                '#title' => $this->t('Copy advanced Flow from other site'),
                '#url' => Url::fromRoute('entity.cms_content_sync_flow.copy_remote'),
                '#attributes' => [
                    'class' => ['button', 'button-action', 'button--small'],
                ],
            ],
            'assignment_form' => [
                '#type' => 'link',
                '#title' => $this->t('Change Pool assignment of advanced Flow'),
                '#url' => Url::fromRoute('cms_content_sync.pool_assignment_form'),
                '#attributes' => [
                    'class' => ['button', 'button-action', 'button--small'],
                ],
            ],
        ];

        if (!empty($rows)) {
            foreach ($rows as $row) {
                if (strpos($row['status']->__toString(), 'Overwritten')) {
                    $status_overwritten = true;
                }
                if (strpos($row['status']->__toString(), 'Requires export')) {
                    $version_mismatch = true;
                }
            }

            if ($status_overwritten) {
                $build['explanation_overwritten'] = [
                    '#markup' => '<i>'.$this->t('Overwritten: The status of this flow has been overwritten in a settings file.').'</i><br>',
                ];
            }
            if ($version_mismatch) {
                $always_v2 = Migration::alwaysUseV2();
                $message = $this->t('Requires export: Entity types were changed, so the Flow must be exported again.');
                $build['explanation_version_mismatch'] = [
                    '#markup' => '<i>'.$message.'</i>',
                ];
            }

            $build['hints'] = [
                '#markup' => '<h3>'.$this->t('Hints').'</h3>'.$this->t('You can enable / disable Flows using your settings.php file:'),
            ];

            $overwrite_status = '<li>'.$this->t('Status: <i>@overwrite_status</i>', ['@overwrite_status' => '$config["cms_content_sync.flow.<flow_machine_name>"]["status"] = FALSE;']).'</li>';

            $build['hints_examples'] = [
                '#markup' => '<ul>'.$overwrite_status.'</ul>',
            ];
        }

        return $build;
    }
}
