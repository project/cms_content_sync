<?php

namespace Drupal\cms_content_sync\Controller;

use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\Entity\Pool;
use Drupal\cms_content_sync\PullIntent;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Try to migrate the advanced Flow to a simple Flow as advanced Flows are
 * deprecated and no longer supported in 3.x.
 */
class AdvancedToSimpleFlow extends ControllerBase
{
    public static function iterateProperty($config, $path, $current_path, &$perform_check)
    {
        if (empty($path)) {
            $perform_check($config, $current_path);

            return;
        }

        $property = array_shift($path);
        if ('*' === $property) {
            if (!is_array($config)) {
                /*$invalid_values[] = [
                          'value' => $config,
                          'paths' => [
                            implode('.', $current_path),
                          ],
                        ];*/
                return;
            }

            foreach ($config as $actual_property => $sub_config) {
                $sub_path = [...$current_path, $actual_property];
                self::iterateProperty($sub_config, $path, $sub_path, $perform_check);
            }

            return;
        }

        $current_path[] = $property;

        if (!isset($config[$property])) {
            /*$invalid_values[] = [
                      'value' => $config,
                      'paths' => [
                        implode('.', $current_path),
                      ],
                    ];*/
            return;
        }

        self::iterateProperty($config[$property], $path, $current_path, $perform_check);
    }

    public static function filterIgnored(&$value, $ignore_values)
    {
        if (null !== $ignore_values) {
            if (is_array($value)) {
                foreach ($value as $key => $item_value) {
                    if (in_array($item_value, $ignore_values)) {
                        unset($value[$key]);
                    }
                }
                if (empty($value)) {
                    return true;
                }
            } else {
                if (in_array($value, $ignore_values)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function analyze(Flow $flow)
    {
        static $reporter = [];
        if (empty($reporter[$flow->id()])) {
            $reporter[$flow->id()] = self::getReporter($flow);

            self::runChecks($flow, $reporter[$flow->id()]);
        }

        return $reporter[$flow->id()];
    }

    public static function canMigrate(Flow $flow)
    {
        return !self::analyze($flow)->hasError();
    }

    public static function migrate(Flow $flow)
    {
        if (!self::canMigrate($flow)) {
            //throw new \Exception("This Flow can't be migrated.");
        }

        $get_first_setting = function ($path, $modes = [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], $ignore_values = null, $entity_types = null) use (&$flow) {
            $result = null;

            $is_push = Flow::TYPE_PUSH === $flow->type;
            $mode = $is_push ? 'export' : 'import';
            $variants = [];
            $negate_entity_type_filter = null !== $entity_types && '!' === $entity_types[0];
            if ($negate_entity_type_filter) {
                array_shift($entity_types);
            }

            foreach ($flow->getController()->getEntityTypeConfig() as $entity_type_name => $entity_type_settings) {
                if (null !== $entity_types && ($negate_entity_type_filter ? in_array($entity_type_name, $entity_types) : !in_array($entity_type_name, $entity_types))) {
                    continue;
                }
                foreach ($entity_type_settings as $bundle_name => $bundle_settings) {
                    $current_path = ['per_bundle_settings', $entity_type_name, $bundle_name];
                    if (!in_array($bundle_settings[$mode], $modes)) {
                        continue;
                    }

                    if (empty($bundle_settings['properties']) && 'properties' === $path[0]) {
                        $bundle_settings['properties'] = $flow->per_bundle_settings[$entity_type_name][$bundle_name]['properties'];
                    }

                    $perform_check = function ($config, $current_path) use (&$variants, &$ignore_values, &$result) {
                        if (AdvancedToSimpleFlow::filterIgnored($config, $ignore_values)) {
                            return;
                        }

                        $result = $config;
                    };

                    AdvancedToSimpleFlow::iterateProperty($bundle_settings, $path, $current_path, $perform_check);

                    if (null !== $result) {
                        return $result;
                    }
                }
            }

            return $result;
        };

        $is_push = Flow::TYPE_PUSH === $flow->type;
        $mode = $is_push ? 'export' : 'import';

        $pools_in_flow = array_keys($get_first_setting([$mode.'_pools'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null, Pool::POOL_USAGE_FORBID]) ?? []);
        sort($pools_in_flow);
        $all_pools = array_keys(Pool::getAll());
        sort($all_pools);
        if (empty($pools_in_flow) || $pools_in_flow == $all_pools) {
            $pools_in_flow = null;
        }

        $per_bundle_settings = $flow->getController()->getEntityTypeConfig();

        $flow->variant = Flow::VARIANT_SIMPLE;
        $flow->simple_settings = [
            'poolAssignment' => in_array(Pool::POOL_USAGE_ALLOW, $get_first_setting([$mode.'_pools'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null, Pool::POOL_USAGE_FORBID]) ?? []) ? FlowControllerSimple::ASSIGN_POOLS_MANUALLY : FlowControllerSimple::ASSIGN_ALL_POOLS,
            'mode' => $get_first_setting([$mode], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]),
            'deletions' => (bool) $get_first_setting([$mode.'_deletion_settings', $mode.'_deletion'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]),
            'allowLocalDeletion' => $get_first_setting(['import_deletion_settings', 'allow_local_deletion_of_import'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]),
            'embeddedDeletions' => (bool) $get_first_setting([$mode.'_deletion_settings', $mode.'_deletion'], [FlowControllerSimple::MODE_DEPENDENT], [null]),
            'updateBehavior' => $get_first_setting(['import_updates'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]) ?? PullIntent::PULL_UPDATE_FORCE_AND_FORBID_EDITING,
            'ignoreUnpublishedChanges' => (bool) $get_first_setting(['handler_settings', 'ignore_unpublished'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]),
            'allowExplicitUnpublishing' => (bool) $get_first_setting(['handler_settings', 'allow_explicit_unpublishing'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]),
            'pushMenuItems' => (bool) $get_first_setting(['handler_settings', 'export_menu_items'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]),
            'pushPreviews' => (bool) $get_first_setting(['preview'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null, 'none', 'default']),
            'mergeLocalChanges' => (bool) $get_first_setting(['properties', '*', 'handler_settings', 'merge_local_changes'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], [null, 0]),
            'resolveUserReferences' => $get_first_setting(['properties', '*', 'handler_settings', 'identification']) ?? 'name',
            'poolSelectionWidget' => $get_first_setting(['pool_export_widget_type'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [null]) ?? 'checkboxes',
            'entityTypeSettings' => [],
            'pools' => $pools_in_flow,
        ];

        foreach ($per_bundle_settings as $entity_type_name => &$entity_type_settings) {
            $modes = [];
            $requires_per_bundle = false;
            foreach ($entity_type_settings as $bundle_name => &$check_bundle_settings) {
                if (empty($check_bundle_settings['properties']) && !empty($flow->per_bundle_settings[$entity_type_name][$bundle_name]['properties'])) {
                    $check_bundle_settings['properties'] = $flow->per_bundle_settings[$entity_type_name][$bundle_name]['properties'];
                }
                if (!in_array($check_bundle_settings[$mode], $modes)) {
                    $modes[] = $check_bundle_settings[$mode];
                }
                if ($requires_per_bundle) {
                    continue;
                }
                if (FlowControllerSimple::MODE_IGNORE === $check_bundle_settings[$mode]) {
                    continue;
                }
                if ($check_bundle_settings['preview'] && 'none' !== $check_bundle_settings['preview'] && 'disabled' !== $check_bundle_settings['preview'] && 'default' !== $check_bundle_settings['preview']) {
                    $requires_per_bundle = true;

                    continue;
                }
                if (!empty($check_bundle_settings['handler_settings']['restrict_menus'])) {
                    $requires_per_bundle = true;

                    continue;
                }
                foreach ($check_bundle_settings['properties'] as $property) {
                    if (!empty($property['handler_settings']['subscribe_only_to'])) {
                        $requires_per_bundle = true;

                        break;
                    }
                }
            }

            if (0 === count($modes)) {
                continue;
            }

            $requires_per_bundle |= count($modes) > 1;

            $flow->simple_settings['entityTypeSettings'][$entity_type_name] = [];
            if ($requires_per_bundle) {
                $flow->simple_settings['entityTypeSettings'][$entity_type_name]['perBundle'] = [];
                foreach ($entity_type_settings as $bundle_name => $bundle_settings) {
                    $bundle_mode = $bundle_settings[$mode];
                    $flow->simple_settings['entityTypeSettings'][$entity_type_name]['perBundle'][$bundle_name] = [
                        'mode' => FlowControllerSimple::MODE_DEPENDENT === $bundle_mode ? FlowControllerSimple::MODE_DEPENDENT : (in_array($bundle_mode, [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY]) ? FlowControllerSimple::MODE_DEFAULT : FlowControllerSimple::MODE_IGNORE),
                    ];
                    if (!empty($bundle_settings['preview']) && 'none' !== $bundle_settings['preview'] && 'default' !== $bundle_settings['preview']) {
                        $flow->simple_settings['entityTypeSettings'][$entity_type_name]['perBundle'][$bundle_name]['previewViewMode'] = $bundle_settings['preview'];
                    }
                    $filters = [];
                    if (!empty($bundle_settings['properties'])) {
                        foreach ($bundle_settings['properties'] as $key => $property) {
                            if (empty($property['handler_settings']['subscribe_only_to'])) {
                                continue;
                            }
                            $filter = [
                                'type' => 'includes-reference',
                                'fieldMachineName' => $key,
                                'values' => [],
                            ];
                            foreach ($property['handler_settings']['subscribe_only_to'] as $subscribe_to) {
                                $filter['values'][] = [
                                    'remoteUuid' => $subscribe_to['uuid'],
                                ];
                            }
                            if (!empty($filter['values'])) {
                                $filters[] = $filter;
                            }
                        }
                    }
                    if (!empty($bundle_settings['handler_settings']['restrict_menus'])) {
                        $filter = [
                            'type' => 'menu',
                            'values' => [],
                        ];
                        foreach ($bundle_settings['handler_settings']['restrict_menus'] as $name => $value) {
                            if ((int) $value) {
                                $filter['values'][] = $name;
                            }
                        }
                        if (!empty($filter['values'])) {
                            $filters[] = $filter;
                        }
                    }
                    if (count($filters)) {
                        $flow->simple_settings['entityTypeSettings'][$entity_type_name]['perBundle'][$bundle_name]['filters'] = $filters;
                    }
                }
            } else {
                $bundle_mode = $modes[0];
                $flow->simple_settings['entityTypeSettings'][$entity_type_name]['allBundles'] = [
                    'mode' => FlowControllerSimple::MODE_DEPENDENT === $bundle_mode ? FlowControllerSimple::MODE_DEPENDENT : (FlowControllerSimple::MODE_IGNORE === $bundle_mode ? FlowControllerSimple::MODE_IGNORE : FlowControllerSimple::MODE_DEFAULT),
                ];
            }
        }

        $flow->per_bundle_settings = null;

        $flow->save();

        \Drupal::messenger()->addStatus('The Flow was migrated successfully. Please review the changes.');
        \Drupal::messenger()->addWarning('If you are using Drupal\'s config management please be sure to export the migrated Flow before your next config import.');
    }

    public static function plainContent(Flow $flow, ?string $run = null)
    {
        if (Flow::VARIANT_SIMPLE === $flow->variant) {
            \Drupal::messenger()->addStatus(t('Flow %flow is already using the new Flow form.', ['%flow' => $flow->label()]));

            return true;
        }

        $reporter = self::analyze($flow);

        if ($reporter->hasError()) {
            \Drupal::messenger()->addError(t("Flow %flow cannot be migrated automatically. Please either fix the errors below or run the migration manually. In some cases you have to split your Flow into multiple Flows to work with the new Flow format. Please reach out to use if you need any help. Errors:\n%errors", ['%flow' => $flow->label(), '%errors' => $reporter->renderPlain(true)]));

            return false;
        }

        if ('1' === $run) {
            self::migrate($flow);

            return true;
        }

        \Drupal::messenger()->addStatus(t('Flow %flow is ready to be migrated.', ['%flow' => $flow->label()]));

        return true;
    }

    /**
     * @return array the content array to theme the compatibility tables
     */
    public function content(Flow $cms_content_sync_flow)
    {
        // Allow pre-processing to take place.
        $flow = Flow::getAll(false)[$cms_content_sync_flow->id()];

        if (Flow::VARIANT_PER_BUNDLE !== $flow->variant) {
            return [
                '#markup' => 'This Flow is already using the new Flow form.',
            ];
        }

        $run = \Drupal::request()->query->get('run');
        if ('1' === $run) {
            self::migrate($flow);
            $url = Url::fromRoute('entity.cms_content_sync_flow.edit_form', ['cms_content_sync_flow' => $flow->id()]);

            return RedirectResponse::create($url->toString());
        }

        $content = [];

        $content['headline'] = [
            '#prefix' => '<h1>Migrate Flow <em>',
            '#suffix' => '</em></h1>',
            '#plain_text' => $flow->label(),
        ];

        $reporter = self::analyze($flow);

        $content = array_merge($content, $reporter->render());

        if ($reporter->hasError()) {
            $content[] = [
                '#plain_text' => 'The Flow cannot be migrated because of the errors above. Please either fix the errors above or run the migration manually. In some cases you have to split your Flow into multiple Flows to work with the new Flow format. Please reach out to use if you need any help.',
                '#prefix' => '<h2>Summary</h2><div class="messages messages--error">',
                '#suffix' => '</div>',
            ];
        } else {
            $content[] = [
                '#plain_text' => 'The Flow is ready to be migrated.',
                '#prefix' => '<h2>Summary</h2><div class="messages messages--status">',
                '#suffix' => '</div>',
            ];
            $url = Url::fromRoute('entity.cms_content_sync_flow.advanced_to_simple', ['cms_content_sync_flow' => $flow->id()], [
                'query' => [
                    'run' => '1',
                ],
            ]);
            $link = Link::fromTextAndUrl(t('Migrate'), $url);
            $link = $link->toRenderable();
            $link['#attributes'] = ['class' => ['button', 'button--primary']];
            $content[] = $link;
        }

        return $content;
    }

    protected static function getReporter(Flow $flow)
    {
        $reporter = new class($flow) {
            protected $groups = [];
            protected $activeGroup;
            protected Flow $flow;

            public function __construct($flow)
            {
                $this->flow = $flow;
            }

            public function addGroup($name)
            {
                $this->groups[] = [
                    'name' => $name,
                    'checks' => [],
                ];
                $this->activeGroup = &$this->groups[count($this->groups) - 1];

                return $this;
            }

            public function addCheck($title, $ok, $differences = [], $invalid = [])
            {
                $this->activeGroup['checks'][] = [
                    'title' => $title,
                    'error' => !$ok,
                    'differences' => $differences,
                    'invalid' => $invalid,
                ];

                return $this;
            }

            public function expectEqualAcrossTypes($title, $path, $modes = [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], $allowed_values = null, $ignore_values = null, $entity_types = null)
            {
                $is_push = Flow::TYPE_PUSH === $this->flow->type;
                $mode = $is_push ? 'export' : 'import';
                $variants = [];
                $negate_entity_type_filter = null !== $entity_types && '!' === $entity_types[0];
                if ($negate_entity_type_filter) {
                    array_shift($entity_types);
                }
                $invalid_values = [];
                foreach ($this->flow->getController()->getEntityTypeConfig() as $entity_type_name => $entity_type_settings) {
                    if (null !== $entity_types && ($negate_entity_type_filter ? in_array($entity_type_name, $entity_types) : !in_array($entity_type_name, $entity_types))) {
                        continue;
                    }
                    foreach ($entity_type_settings as $bundle_name => $bundle_settings) {
                        $current_path = ['per_bundle_settings', $entity_type_name, $bundle_name];
                        if (!in_array($bundle_settings[$mode], $modes)) {
                            continue;
                        }

                        if (empty($bundle_settings['properties']) && 'properties' === $path[0]) {
                            $bundle_settings['properties'] = $this->flow->per_bundle_settings[$entity_type_name][$bundle_name]['properties'] ?? [];
                        }

                        $perform_check = function ($config, $current_path) use (&$variants, &$ignore_values, &$allowed_values, &$invalid_values) {
                            if (AdvancedToSimpleFlow::filterIgnored($config, $ignore_values)) {
                                return;
                            }

                            if (null !== $allowed_values) {
                                $new_invalid = [];
                                if (is_array($config)) {
                                    foreach ($config as $value) {
                                        if (!in_array($value, $allowed_values)) {
                                            $new_invalid[] = $value;
                                        }
                                    }
                                } elseif (!in_array($config, $allowed_values)) {
                                    $new_invalid[] = $config;
                                }
                                if (count($new_invalid)) {
                                    foreach ($new_invalid as $invalid_value) {
                                        $found = false;
                                        foreach ($invalid_values as &$existing_invalid) {
                                            if ($existing_invalid['value'] == $invalid_value) {
                                                $existing['paths'][] = implode('.', $current_path);
                                                $found = true;

                                                break;
                                            }
                                        }
                                        if (!$found) {
                                            $invalid_values[] = [
                                                'value' => $invalid_value,
                                                'paths' => [
                                                    implode('.', $current_path),
                                                ],
                                            ];
                                        }
                                    }
                                }
                            }

                            $found = false;
                            foreach ($variants as &$existing) {
                                if ($existing['value'] == $config) {
                                    $existing['paths'][] = implode('.', $current_path);
                                    $found = true;

                                    break;
                                }
                            }
                            if (!$found) {
                                $variants[] = [
                                    'value' => $config,
                                    'paths' => [
                                        implode('.', $current_path),
                                    ],
                                ];
                            }
                        };

                        AdvancedToSimpleFlow::iterateProperty($bundle_settings, $path, $current_path, $perform_check);
                    }
                }

                //\Drupal::messenger()->addStatus(json_encode([$path, $variants]));

                $this->addCheck($title, count($variants) < 2 && empty($invalid_value_extra), count($variants) < 2 ? [] : $variants, $invalid_values);

                return $this;
            }

            public function hasError()
            {
                foreach ($this->groups as $group) {
                    foreach ($group['checks'] as $check) {
                        if ($check['error']) {
                            return true;
                        }
                    }
                }

                return false;
            }

            public function renderPlain($errors_only = false)
            {
                $render_list = function ($items, $title, $indent = '') {
                    $list = '';
                    foreach ($items as $item) {
                        $render_item = $indent.'  - '.json_encode($item['value']).(empty($item['expected_value']) ? '' : ' but expected '.json_encode($item['expected_value']))."\n";
                        $sublist = '';
                        foreach ($item['paths'] as $path) {
                            $sublist .= $indent.'    - '.$path."\n";
                        }
                        $render_item .= $sublist;
                        $list .= $render_item;
                    }

                    return $indent.'- '.$title."\n".$list;
                };
                $output = '';
                foreach ($this->groups as $group) {
                    $group_output = '';
                    foreach ($group['checks'] as $check) {
                        if (!$check['error'] && $errors_only) {
                            continue;
                        }
                        $group_output .= '- '.($check['error'] ? 'INVALID' : 'VALID').': '.$check['title']."\n";
                        if (!empty($check['differences'])) {
                            $group_output .= $render_list($check['differences'], 'DIFFERENCES:', '  ');
                        }
                        if (!empty($check['invalid'])) {
                            $group_output .= $render_list($check['invalid'], 'INVALID CONFIG:', '  ');
                        }
                    }
                    if ($group_output) {
                        $output .= "\n".$group['name']."\n".$group_output;
                    }
                }

                return $output;
            }

            public function render()
            {
                $render_list = function ($items, $title) {
                    $list = [
                        '#prefix' => '<ul>',
                        '#suffix' => '</ul>',
                    ];
                    foreach ($items as $item) {
                        $render_item = [
                            '#prefix' => '<li>',
                            '#suffix' => '</li>',
                            '#plain_text' => json_encode($item['value']).(empty($item['expected_value']) ? '' : ' but expected '.json_encode($item['expected_value'])),
                        ];
                        $sublist = [
                            '#prefix' => '<ul>',
                            '#suffix' => '</ul>',
                        ];
                        foreach ($item['paths'] as $path) {
                            $sublist[] = [
                                '#prefix' => '<li>',
                                '#suffix' => '</li>',
                                '#plain_text' => $path,
                            ];
                        }
                        $render_item[] = $sublist;
                        $list[] = $render_item;
                    }

                    return [
                        '#prefix' => '<ul>',
                        '#suffix' => '</ul>',
                        [
                            '#prefix' => '<li>',
                            '#suffix' => '</li>',
                            '#plain_text' => $title,
                            [
                                '#prefix' => '<ul>',
                                '#suffix' => '</ul>',
                                ...$list,
                            ],
                        ],
                    ];
                };
                $output = [];
                foreach ($this->groups as $group) {
                    $output[] = [
                        '#plain_text' => $group['name'],
                        '#prefix' => '<h2>',
                        '#suffix' => '</h2>',
                    ];
                    foreach ($group['checks'] as $check) {
                        $output[] = [
                            '#plain_text' => $check['title'],
                            '#prefix' => '<div class="messages messages--'.($check['error'] ? 'error' : 'status').'">',
                            '#suffix' => '</div>',
                            ...empty($check['differences']) ? [] : [
                                $render_list($check['differences'], 'DIFFERENCES:'),
                            ],
                            ...empty($check['invalid']) ? [] : [
                                $render_list($check['invalid'], 'INVALID CONFIG:'),
                            ],
                        ];
                    }
                }

                return $output;
            }
        };

        return $reporter;
    }

    protected static function runChecks(Flow $flow, $reporter)
    {
        $is_push = Flow::TYPE_PUSH === $flow->type;

        $reporter
            ->addGroup('Mode')
            ->addCheck('A Flow may only push OR pull, but not both.', in_array($flow->type, [Flow::TYPE_PUSH, Flow::TYPE_PULL]))
            ->expectEqualAcrossTypes('All root entity types must be pushed/pulled manually OR automatically, but not both.', [$is_push ? 'export' : 'import'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY]);
        if (!$is_push) {
            $reporter
                ->expectEqualAcrossTypes('The update behavior must be identical everywhere.', ['import_updates'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], [
                    PullIntent::PULL_UPDATE_FORCE_AND_FORBID_EDITING,
                    PullIntent::PULL_UPDATE_FORCE_UNLESS_OVERRIDDEN,
                    PullIntent::PULL_UPDATE_IGNORE,
                    PullIntent::PULL_UPDATE_UNPUBLISHED,
                ]);
        }

        $reporter
            ->addGroup('Pools')
            ->expectEqualAcrossTypes('All root entity types must use the same Pools config.', [$is_push ? 'export_pools' : 'import_pools'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY])
            ->expectEqualAcrossTypes('All root entity types must either use ALLOW or FORCE, but not both.', [$is_push ? 'export_pools' : 'import_pools', '*'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY], null, [Pool::POOL_USAGE_FORBID])
            ->expectEqualAcrossTypes('All dependent entity types must use the same Pools config.', [$is_push ? 'export_pools' : 'import_pools'], [FlowControllerSimple::MODE_DEPENDENT])
            ->expectEqualAcrossTypes('All root entity types must either use ALLOW or FORCE, but not both.', [$is_push ? 'export_pools' : 'import_pools', '*'], [FlowControllerSimple::MODE_DEPENDENT], null, [Pool::POOL_USAGE_FORBID]);
        if ($is_push) {
            $reporter
                ->expectEqualAcrossTypes('The pool selection widget must be identical.', ['pool_export_widget_type'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT]);
        }

        $reporter
            ->addGroup('Deletion')
            ->expectEqualAcrossTypes('Deletion handling must be identical for root entities.', $is_push ? ['export_deletion_settings', 'export_deletion'] : ['import_deletion_settings', 'import_deletion'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY])
            ->expectEqualAcrossTypes('Deletion handling must be identical for dependent entities.', $is_push ? ['export_deletion_settings', 'export_deletion'] : ['import_deletion_settings', 'import_deletion'], [FlowControllerSimple::MODE_DEPENDENT]);
        if (!$is_push) {
            $reporter
                ->expectEqualAcrossTypes('Local deletion of pulled content must be identical.', ['import_deletion_settings', 'allow_local_deletion_of_import'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY]);
        }

        $reporter
            ->addGroup('Content (node entity type)')
            ->expectEqualAcrossTypes('Handling unpublished content must be identical for all bundles.', ['handler_settings', 'ignore_unpublished'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, null, ['node'])
            ->expectEqualAcrossTypes('Allowing to explicitly unpublish content must be identical for all bundles.', ['handler_settings', 'allow_explicit_unpublishing'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, null, ['node']);

        $reporter
            ->addGroup('Menus / Menu links')
            ->expectEqualAcrossTypes('Menu restrictions must be identical.', ['handler_settings', 'restrict_menus'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, null, [
                'menu_link_content',
            ]);
        if ($is_push) {
            $reporter
                ->expectEqualAcrossTypes('Pushing menu items must be identical everywhere.', ['handler_settings', 'export_menu_items'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, null, [
                    '!',
                    'brick',
                    'field_collection_item',
                    'menu_link_content',
                    'paragraph',
                    'file',
                    'media',
                    'crop',
                ]);
        }

        if ($is_push) {
            $reporter
                ->addGroup('File')
                ->expectEqualAcrossTypes('Files must automatically push related crop entities.', ['handler_settings', 'export_crop'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], [1], null, [
                    'file',
                ]);
        }

        $reporter
            ->addGroup('Fields & Properties')
            ->expectEqualAcrossTypes('User references must be resolved the same way everywhere.', ['properties', '*', 'handler_settings', 'identification'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, [null])
            ->expectEqualAcrossTypes('Custom blocks must be pushed if referenced.', ['properties', '*', 'handler_settings', 'export_referenced_custom_blocks'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, [null])
            ->expectEqualAcrossTypes('References must be pushed if their entity type mode is dependent.', ['properties', '*', 'handler_settings', 'export_referenced_entities'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, [null])
            ->expectEqualAcrossTypes('Paragraphs must be merged consistently.', ['properties', '*', 'handler_settings', 'merge_local_changes'], [FlowControllerSimple::MODE_AUTOMATICALLY, FlowControllerSimple::MODE_MANUALLY, FlowControllerSimple::MODE_DEPENDENT], null, [null]);

        $mode = $is_push ? 'export' : 'import';

        $entity_plugin_manager = \Drupal::service('plugin.manager.cms_content_sync_entity_handler');
        $field_plugin_manager = \Drupal::service('plugin.manager.cms_content_sync_field_handler');
        $entity_field_manager = \Drupal::service('entity_field.manager');
        $field_map = $entity_field_manager->getFieldMap();
        $entity_types = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();

        $reporter
            ->addGroup('Handlers');
        $invalid_entity = [];
        $invalid_field = [];
        foreach ($flow->getController()->getEntityTypeConfig() as $entity_type_name => $entity_type_settings) {
            if (empty($entity_types[$entity_type_name])) {
                continue;
            }
            foreach ($entity_type_settings as $bundle_name => $bundle_settings) {
                if (empty($entity_types[$entity_type_name][$bundle_name])) {
                    continue;
                }
                if (FlowControllerSimple::MODE_IGNORE === $bundle_settings[$mode]) {
                    continue;
                }
                $entity_handlers = $entity_plugin_manager->getHandlerOptions($entity_type_name, $bundle_name, true);
                if (!count($entity_handlers)) {
                    continue;
                }
                $entity_handler_names = array_keys($entity_handlers);
                $handler_id = reset($entity_handler_names);
                if ($bundle_settings['handler'] !== $handler_id) {
                    $invalid_entity[] = [
                        'value' => $bundle_settings['handler'],
                        'expected_value' => $handler_id,
                        'paths' => [
                            implode('.', [$entity_type_name, $bundle_name, 'handler']),
                        ],
                    ];
                }

                if (!isset($field_map[$entity_type_name])) {
                    continue;
                }

                $handler = $entity_plugin_manager->createInstance($handler_id, [
                    'entity_type_name' => $entity_type_name,
                    'bundle_name' => $bundle_name,
                    'settings' => $bundle_settings['handler_settings'] ?? [],
                    'sync' => null,
                ]);
                $forbidden_fields = $handler->getForbiddenFields();

                $properties = $flow->per_bundle_settings[$entity_type_name][$bundle_name]['properties'] ?? [];
                $fields = $entity_field_manager->getFieldDefinitions($entity_type_name, $bundle_name);
                foreach ($fields as $key => $field) {
                    $field_handlers = $field_plugin_manager->getHandlerOptions($entity_type_name, $bundle_name, $key, $field, true);
                    $ignore = in_array($key, $forbidden_fields) || empty($field_handlers);
                    $handler_id = $ignore ? 'ignore' : key($field_handlers);

                    if (($properties[$key]['handler'] ?? null) !== $handler_id) {
                        $invalid_field[] = [
                            'value' => $properties[$key]['handler'] ?? null,
                            'expected_value' => $handler_id,
                            'paths' => [
                                implode('.', [$entity_type_name, $bundle_name, 'properties', $key, 'handler']),
                            ],
                        ];
                    }
                    if ('ignore' !== $handler_id && 'automatically' !== ($properties[$key][$mode] ?? null)) {
                        $invalid_field[] = [
                            'value' => $properties[$key][$mode] ?? null,
                            'expected_value' => 'automatically',
                            'paths' => [
                                implode('.', [$entity_type_name, $bundle_name, 'properties', $key, $mode]),
                            ],
                        ];
                    }
                }
            }
        }
        $reporter->addCheck('All entity type handlers must be set to their expected default.', !count($invalid_entity), [], $invalid_entity);
        $reporter->addCheck('All field handlers must be set to their expected default.', !count($invalid_field), [], $invalid_field);
    }
}
