<?php

namespace Drupal\cms_content_sync\Controller;

use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\Entity\Pool;
use Drupal\cms_content_sync\Plugin\Type\EntityHandlerPluginManager;
use Drupal\cms_content_sync\PullIntent;
use Drupal\cms_content_sync\SyncCoreInterface\DrupalApplication;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use stdClass;

/**
 * Provides a listing of Flow.
 */
class ManualPull extends ControllerBase
{
    public static function hasAnyManualPullFlows()
    {
        static $has_any = null;

        if (null === $has_any) {
            $cache = \Drupal::cache();
            $cache_item = $cache->get(Flow::CACHE_ITEM_NAME_FLOWS);
            $cache_item_data = $cache_item ? $cache_item->data : null;

            if ($cache_item_data && isset($cache_item_data->hasAnyManualPull)) {
                $has_any = $cache_item_data->hasAnyManualPull;
            } else {
                $has_any = false;
                $flows = Flow::getAll();
                foreach ($flows as $flow) {
                    $pull_manually = $flow->getController()->getEntityTypesToPull(PullIntent::PULL_MANUALLY);
                    if (!empty($pull_manually)) {
                        $has_any = true;

                        break;
                    }
                }

                if (!$cache_item_data) {
                    $cache_item_data = new stdClass();
                }
                $cache_item_data->hasAnyManualPull = $has_any;
                $cache->set(Flow::CACHE_ITEM_NAME_FLOWS, $cache_item_data, CacheBackendInterface::CACHE_PERMANENT, [Flow::CACHE_TAG_ANY_FLOW]);
            }
        }

        return $has_any;
    }

    /**
     * Ensure that the pull tab is just show if a flow exists which contains
     * and entity type that has its pull set to "manual".
     */
    public function access()
    {
        return AccessResult::allowedIf(self::hasAnyManualPullFlows())
            ->addCacheTags([Flow::CACHE_TAG_ANY_FLOW]);
    }

    /**
     * Render the content synchronization Angular frontend.
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \EdgeBox\SyncCore\Exception\SyncCoreException
     *
     * @return array
     */
    public function content()
    {
        $settings = ContentSyncSettings::getInstance();

        if (Migration::alwaysUseV2() || 'yes' === \Drupal::request()->query->get('v2')) {
            $embed = Embed::create(\Drupal::getContainer());

            return $embed->pullDashboard();
        }

        $config = [
            'siteUrl' => $settings->getSiteBaseUrl(),
            'pools' => [],
            'flows' => [],
            'entity_types' => [],
        ];

        $pools = Pool::getAll();

        $cloud = _cms_content_sync_is_cloud_version() && $settings->isDirectSyncCoreAccessEnabled();
        $sync_core_settings = null;

        $connection_id = null;
        foreach (Flow::getAll() as $id => $flow) {
            $config['flows'][$flow->id] = [
                'id' => $flow->id,
                'name' => $flow->name,
            ];

            foreach ($flow->getController()->getEntityTypeConfig() as $entity_type_name => $bundles) {
                foreach ($bundles as $bundle_name => $definition) {
                    if (!$flow->getController()->canPullEntity($entity_type_name, $bundle_name, PullIntent::PULL_MANUALLY)) {
                        continue;
                    }

                    foreach ($flow->getController()->getEntityTypeConfig($entity_type_name, $bundle_name)['import_pools'] as $id => $option) {
                        if (Pool::POOL_USAGE_ALLOW != $option) {
                            continue;
                        }
                        $pool = $pools[$id];
                        $config['pools'][$pool->id] = [
                            'id' => $pool->id,
                            'label' => $pool->label,
                            'site_id' => DrupalApplication::get()->getSiteMachineName(),
                        ];

                        if (!$sync_core_settings) {
                            $sync_core_settings = $pool
                                ->getClient()
                                ->getSyndicationService()
                                ->configurePullDashboard();
                        }
                    }

                    $index = $entity_type_name.'.'.$bundle_name;
                    if (!isset($config['entity_types'][$index])) {
                        // Get the entity type and bundle name.
                        $entity_type_storage = \Drupal::entityTypeManager()->getStorage($entity_type_name);
                        $entity_type = $entity_type_storage->getEntityType();
                        $entity_type_label = $entity_type->getLabel()->render();
                        $bundle_info = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_name);
                        $bundle_label = $bundle_info[$bundle_name]['label'];

                        $config['entity_types'][$index] = [
                            'entity_type_name' => $entity_type_name,
                            'entity_type_label' => $entity_type_label,
                            'bundle_name' => $bundle_name,
                            'bundle_label' => $bundle_label,
                            'version' => $definition['version'],
                            'pools' => [],
                            'preview' => $definition['preview'] ?? Flow::PREVIEW_DISABLED,
                        ];
                    } else {
                        if (Flow::PREVIEW_DISABLED == $config['entity_types'][$index]['preview'] || Flow::PREVIEW_TABLE != $definition['preview']) {
                            $config['entity_types'][$index]['preview'] = $definition['preview'] ?? Flow::PREVIEW_DISABLED;
                        }
                    }

                    foreach ($definition['import_pools'] as $id => $action) {
                        if (!isset($config['entity_types'][$index]['pools'][$id])
                || Pool::POOL_USAGE_FORCE == $action
                || Pool::POOL_USAGE_FORBID == $config['entity_types'][$index]['pools'][$id]) {
                            $config['entity_types'][$index]['pools'][$id] = $action;
                        }
                    }
                }
            }
        }

        // Provide additional conditions for "subscribe only to" filters.
        if ($cloud) {
            $entity_type_ids = [];

            /**
             * @var \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
             */
            $entityFieldManager = \Drupal::service('entity_field.manager');

            foreach (Flow::getAll() as $flow) {
                foreach ($flow->getController()->getEntityTypeConfig() as $entity_type_name => $bundles) {
                    foreach ($bundles as $bundle_name => $definition) {
                        if (!$flow->getController()->canPullEntity($entity_type_name, $bundle_name, PullIntent::PULL_MANUALLY)) {
                            continue;
                        }

                        foreach ($definition['import_pools'] as $pool_id => $behavior) {
                            if (Pool::POOL_USAGE_ALLOW != $behavior) {
                                continue;
                            }

                            if (isset($entity_type_ids[$pool_id][$entity_type_name][$bundle_name])) {
                                continue;
                            }

                            if (EntityHandlerPluginManager::isEntityTypeFieldable($entity_type_name)) {
                                /**
                                 * @var \Drupal\Core\Field\FieldDefinitionInterface[] $fields
                                 */
                                $fields = $entityFieldManager->getFieldDefinitions($entity_type_name, $bundle_name);
                                foreach ($fields as $key => $field) {
                                    $field_config = $flow->getController()->getPropertyConfig($entity_type_name, $bundle_name, $key);
                                    if (empty($field_config)) {
                                        continue;
                                    }

                                    if (empty($field_config['handler_settings']['subscribe_only_to'])) {
                                        continue;
                                    }

                                    $allowed = [];

                                    foreach ($field_config['handler_settings']['subscribe_only_to'] as $ref) {
                                        $allowed[] = $ref['uuid'];
                                    }

                                    $sync_core_settings->ifTaggedWith(
                                        $pool_id,
                                        $entity_type_name,
                                        $bundle_name,
                                        $key,
                                        $allowed
                                    );
                                }
                            }

                            $entity_type_ids[$pool_id][$entity_type_name][$bundle_name] = true;
                        }
                    }
                }
            }
        }

        $config = array_merge($config, $sync_core_settings->getConfig());

        if (empty($config['entity_types'])) {
            \Drupal::messenger()->addMessage(t('There are no entity types to be pulled manually.'));
        }

        return [
            '#theme' => 'cms_content_sync_content_dashboard',
            '#configuration' => $config,
            '#attached' => ['library' => ['cms_content_sync/pull']],
        ];
    }
}
