<?php

namespace Drupal\cms_content_sync;

/**
 * Provides class for groups of status entities that span multiple pools.
 * Required to efficiently push/pull content that has multiple pools assigned.
 *
 * @ingroup cms_content_sync_entity_status
 */
class EntityStatusProxy implements EntityStatusProxyInterface
{
    /**
     * @var EntityStatusProxyInterface[]
     */
    protected $statusEntities;

    /**
     * @param EntityStatusProxyInterface[] $status_entities
     */
    public function __construct(array $status_entities)
    {
        $this->statusEntities = $status_entities;
    }

    public function resetStatus()
    {
        $this->proxyAll('resetStatus');
    }

    /**
     * Get the entity this entity status belongs to.
     *
     * @return \Drupal\Core\Entity\EntityInterface
     */
    public function getEntity()
    {
        return $this->proxyAny('getEntity');
    }

    /**
     * Returns the information if the entity has been pushed before but the last push date was reset.
     *
     * @param bool $set
     *                  Optional parameter to set the value for LastPushReset
     *
     * @return bool
     */
    public function wasLastPushReset($set = null)
    {
        return $this->proxyAll('wasLastPushReset', [$set], true);
    }

    /**
     * Returns the information if the entity has been pulled before but the last import date was reset.
     *
     * @param bool $set
     *                  Optional parameter to set the value for LastPullReset
     *
     * @return bool
     */
    public function wasLastPullReset($set = null)
    {
        return $this->proxyAll('wasLastPullReset', [$set], true);
    }

    /**
     * Returns the information if the last push of the entity failed.
     *
     * @param bool       $set
     *                            Optional parameter to set the value for PushFailed
     * @param bool       $soft
     *                            A soft fail- this was intended according to configuration. But the user might want to know why to debug different
     *                            expectations.
     * @param null|array $details
     *                            If $set is TRUE, you can provide additional details on why the push failed. Can be gotten via
     *                            ->whyDidPushFail()
     *
     * @return bool
     */
    public function didPushFail($set = null, $soft = false, $details = null)
    {
        return $this->proxyAll('didPushFail', [$set, $soft, $details], true);
    }

    /**
     * Get the details provided to ->didPushFail( TRUE, ... ) before.
     *
     * @return null|array
     */
    public function whyDidPushingFail()
    {
        return $this->proxyAny('whyDidPushingFail', []);
    }

    /**
     * Returns the information if the last pull of the entity failed.
     *
     * @param bool       $set
     *                            Optional parameter to set the value for PullFailed
     * @param bool       $soft
     *                            A soft fail- this was intended according to configuration. But the user might want to know why to debug different
     *                            expectations.
     * @param null|array $details
     *                            If $set is TRUE, you can provide additional details on why the pull failed. Can be gotten via
     *                            ->whyDidPullFail()
     *
     * @return bool
     */
    public function didPullFail($set = null, $soft = false, $details = null)
    {
        return $this->proxyAll('didPullFail', [$set, $soft, $details], true);
    }

    /**
     * Get the details provided to ->didPullFail( TRUE, ... ) before.
     *
     * @return null|array
     */
    public function whyDidPullingFail()
    {
        return $this->proxyAny('whyDidPullingFail', []);
    }

    /**
     * Returns the information if the entity has been chosen by the user to
     * be pushed with this flow and pool.
     *
     * @param bool $set
     *                            Optional parameter to set the value for PushEnabled
     * @param bool $setDependency
     *                            Optional parameter to set the value for DependencyPushEnabled
     *
     * @return bool
     */
    public function isPushEnabled($set = null, $setDependency = null)
    {
        return $this->proxyAll('isPushEnabled', [$set, $setDependency], true);
    }

    /**
     * Returns the information if the entity has been chosen by the user to
     * be pushed with this flow and pool.
     *
     * @return bool
     */
    public function isManualPushEnabled()
    {
        return $this->proxyAny('isManualPushEnabled', []);
    }

    /**
     * Returns the information if the entity has been pushed with this flow and
     * pool as a dependency.
     *
     * @return bool
     */
    public function isPushedAsDependency()
    {
        return $this->proxyAny('isPushedAsDependency', []);
    }

    /**
     * Returns the information if the user override the entity locally.
     *
     * @param bool $set
     *                         Optional parameter to set the value for EditOverride
     * @param bool $individual
     *
     * @return bool
     */
    public function isOverriddenLocally($set = null, $individual = false)
    {
        return $this->proxyAll('isOverriddenLocally', [$set, $individual], true);
    }

    /**
     * Returns the information if the entity has originally been created on this
     * site.
     *
     * @param bool  $set
     *                          Optional parameter to set the value for IsSourceEntity
     * @param mixed $individual
     *
     * @return bool
     */
    public function isSourceEntity($set = null, $individual = false)
    {
        return $this->proxyAll('isSourceEntity', [$set, $individual], true);
    }

    /**
     * Returns the information if the user allowed the push.
     *
     * @param bool $set
     *                  Optional parameter to set the value for UserEnabledPush
     *
     * @return bool
     */
    public function didUserEnablePush($set = null)
    {
        return $this->proxyAll('didUserEnablePush', [$set], true);
    }

    /**
     * Returns the information if the entity is deleted.
     *
     * @param bool $set
     *                  Optional parameter to set the value for Deleted
     *
     * @return bool
     */
    public function isDeleted($set = null)
    {
        return $this->proxyAll('isDeleted', [$set], true);
    }

    /**
     * Returns whether the entity was pushed embedded into another parent entity.
     * This is always done for field collections but can also be enabled for other
     * entities like paragraphs or media entities. This can save a lot of requests
     * when entities aren't all syndicated individually.
     *
     * @param bool $set
     *                  Optional parameter to set the value for the flag
     *
     * @return bool
     */
    public function wasPushedEmbedded($set = null)
    {
        return $this->proxyAll('wasPushedEmbedded', [$set], true);
    }

    /**
     * Returns whether the entity was pulled embedded in another parent entity.
     * This is always done for field collections but can also be enabled for other
     * entities like paragraphs or media entities. This can save a lot of requests
     * when entities aren't all syndicated individually.
     *
     * @param bool $set
     *                  Optional parameter to set the value for the flag
     *
     * @return bool
     */
    public function wasPulledEmbedded($set = null)
    {
        return $this->proxyAll('wasPulledEmbedded', [$set], true);
    }

    /**
     * If an entity is pushed or pulled embedded into another entity, we store
     * that parent entity here. This is required so that at a later point we can
     * still force pull and force push the embedded entity although it doesn't
     * exist individually.
     * This is also required to reset e.g. embedded paragraphs after the
     * "Overwrite content locally" checkbox is unchecked.
     *
     * @param string $type
     * @param string $uuid
     */
    public function setParentEntity($type, $uuid)
    {
        return $this->proxyAll('setParentEntity', [$type, $uuid], true);
    }

    /**
     * See above.
     *
     * @return null|\Drupal\Core\Entity\EntityInterface
     */
    public function getParentEntity()
    {
        return $this->proxyAny('getParentEntity');
    }

    /**
     * Returns the timestamp for the last pull.
     *
     * @return int
     */
    public function getLastPull()
    {
        return $this->proxyAny('getLastPull');
    }

    /**
     * Set the last pull timestamp.
     *
     * @param int $timestamp
     */
    public function setLastPull($timestamp)
    {
        return $this->proxyAll('setLastPull', [$timestamp], true);
    }

    /**
     * Returns the UUID of the entity this information belongs to.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->proxyAny('getUuid');
    }

    /**
     * Returns the entity type name of the entity this information belongs to.
     *
     * @return string
     */
    public function getEntityTypeName()
    {
        return $this->proxyAny('getEntityTypeName');
    }

    /**
     * Returns the timestamp for the last push.
     *
     * @return int
     */
    public function getLastPush()
    {
        return $this->proxyAny('getLastPush');
    }

    /**
     * Set the last pull timestamp.
     *
     * @param int $timestamp
     */
    public function setLastPush($timestamp)
    {
        return $this->proxyAll('setLastPush', [$timestamp], true);
    }

    /**
     * Get the flow.
     *
     * @return Flow
     */
    public function getFlow()
    {
        return $this->proxyAny('getFlow');
    }

    /**
     * Returns the entity type version.
     *
     * @return string
     */
    public function getEntityTypeVersion()
    {
        return $this->proxyAny('getEntityTypeVersion');
    }

    /**
     * Set the last pull timestamp.
     *
     * @param string $version
     */
    public function setEntityTypeVersion($version)
    {
        return $this->proxyAll('setEntityTypeVersion', [$version], true);
    }

    /**
     * Returns the entities source url.
     *
     * @return string
     */
    public function getSourceUrl()
    {
        return $this->proxyAny('getSourceUrl');
    }

    /**
     * Provide the entity's source url.
     *
     * @param string $url
     */
    public function setSourceUrl($url)
    {
        $this->proxyAll('setSourceUrl', [$url]);
    }

    /**
     * Get a previously saved key=>value pair.
     *
     * @see self::setData()
     *
     * @param null|string|string[] $key
     *                                  The key to retrieve
     *
     * @return mixed whatever you previously stored here or NULL if the key
     *               doesn't exist
     */
    public function getData($key = null)
    {
        return $this->proxyAny('getData', [$key]);
    }

    /**
     * Set a key=>value pair.
     *
     * @param string|string[] $key
     *                               The key to set (for hierarchical usage, provide
     *                               an array of indices
     * @param mixed           $value
     *                               The value to set. Must be a valid value for Drupal's
     *                               "map" storage (so basic types that can be serialized).
     */
    public function setData($key, $value)
    {
        return $this->proxyAll('setData', [$key, $value], true);
    }

    /**
     * @return null|string
     */
    public function getEntityPushHash()
    {
        return $this->proxyAny('getEntityPushHash');
    }

    /**
     * @param string $hash
     */
    public function setEntityPushHash($hash)
    {
        return $this->proxyAll('setEntityPushHash', [$hash], true);
    }

    public function save()
    {
        $this->proxyAll('save');
    }

    public function getAllTranslationSourceUrls()
    {
        $result = $this->proxyAll('getAllTranslationSourceUrls');

        return array_reduce($result, function ($a, $b) {
            return empty($b) ? $a : array_merge($a, $b);
        }, []);
    }

    public function getTranslationSourceUrl(string $language, $return_default_if_null = true)
    {
        $url = $this->proxyAny('getTranslationSourceUrl', [$language, false]);
        if ($url) {
            return $url;
        }
        if ($return_default_if_null) {
            return $this->getSourceUrl();
        }

        return null;
    }

    public function setTranslationSourceUrl(string $language, string $url)
    {
        $this->proxyAll('setTranslationSourceUrl', [$language, $url]);
    }

    protected function proxyAll($method, $args = [], $returnSingle = false)
    {
        $result = [];
        foreach ($this->statusEntities as $status_entity) {
            $result[] = call_user_func_array([$status_entity, $method], $args);
        }

        return $returnSingle
        ? array_reduce($result, function ($a, $b) {
            return empty($a) ? $b : $a;
        })
        : $result;
    }

    protected function proxyAny($method, $args = [])
    {
        $result = null;
        foreach ($this->statusEntities as $status_entity) {
            $result = call_user_func_array([$status_entity, $method], $args);
            if (!empty($result)) {
                return $result;
            }
        }

        return $result;
    }
}
