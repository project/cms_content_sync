<?php

namespace Drupal\cms_content_sync\Plugin\cms_content_sync\entity_handler;

use Drupal\cms_content_sync\Plugin\EntityHandlerBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class DefaultGroupContentHandler, providing a minimalistic implementation
 * for any content entity type.
 *
 * @EntityHandler(
 *   id = "cms_content_sync_default_group_content_handler",
 *   label = @Translation("Default Group Content"),
 *   weight = 90
 * )
 */
class DefaultGroupContentHandler extends EntityHandlerBase
{
    /**
     * {@inheritdoc}
     */
    public static function supports($entity_type, $bundle)
    {
        if ('group_content' === $entity_type) {
            /**
             * @var GroupContentType $bundle
             */
            $bundle = \Drupal::entityTypeManager()->getStorage('group_content_type')->load($bundle);
            $plugin_id = $bundle->getContentPluginId();
            // We don't support synchronizing memberships (user<>group), but
            // only node<>group references.
            if ('group_node:group_content' === $plugin_id) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllowedPreviewOptions()
    {
        return [
        ];
    }

    public function getViewUrl(EntityInterface $entity)
    {
        return parent::getViewUrl($entity);
    }

    /**
     * Check whether the entity type supports having a label.
     *
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     *
     * @return bool
     */
    protected function hasLabelProperty()
    {
        return true;
    }
}
