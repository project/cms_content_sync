<?php

namespace Drupal\cms_content_sync\Plugin\cms_content_sync\field_handler;

use Drupal\cms_content_sync\Plugin\FieldHandlerBase;
use Drupal\cms_content_sync\PullIntent;
use Drupal\cms_content_sync\SyncIntent;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Special handling of the moderation state to allow pulling unpublished content.
 *
 * @FieldHandler(
 *   id = "cms_content_sync_default_moderation_state_handler",
 *   label = @Translation("Default moderation state"),
 *   weight = 80
 * )
 */
class DefaultModerationStateHandler extends FieldHandlerBase
{
    /**
     * {@inheritdoc}
     */
    public static function supports($entity_type, $bundle, $field_name, FieldDefinitionInterface $field)
    {
        return 'moderation_state' === $field_name;
    }

    /**
     * {@inheritdoc}
     */
    public function pull(PullIntent $intent)
    {
        $action = $intent->getAction();

        // If the update behavior is set to pull unpublished revisions, we have
        // to assign the default workflow state instead of the published one
        // that the entity comes in with.
        if (SyncIntent::ACTION_UPDATE === $action) {
            $entity = $intent->getEntity();
            if ($entity) {
                $config = $intent->getFlow()->getController()->getEntityTypeConfig($entity->getEntityTypeId(), $entity->bundle());
                if (PullIntent::PULL_UPDATE_UNPUBLISHED === $config['import_updates']) {
                    /** @var \Drupal\content_moderation\ModerationInformationInterface $moderation_info */
                    $moderation_info = \Drupal::service('content_moderation.moderation_information');
                    $workflow = $moderation_info->getWorkflowForEntity($entity);
                    if ($workflow) {
                        $default_state = $workflow->getTypePlugin()->getConfiguration()['default_moderation_state'];
                        $entity->set($this->fieldName, [
                            [
                                'value' => $default_state,
                            ],
                        ]);

                        return true;
                    }
                }
            }
        }

        return parent::pull($intent);
    }
}
