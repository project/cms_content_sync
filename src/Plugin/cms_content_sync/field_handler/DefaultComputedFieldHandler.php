<?php

namespace Drupal\cms_content_sync\Plugin\cms_content_sync\field_handler;

use Drupal\cms_content_sync\PullIntent;
use Drupal\cms_content_sync\PushIntent;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provide a handler for computed fields.
 *
 * @FieldHandler(
 *   id = "cms_content_sync_default_computed_field_handler",
 *   label = @Translation("Default Computed Field"),
 *   weight = 80
 * )
 */
class DefaultComputedFieldHandler extends DefaultFieldHandler
{
    /**
     * {@inheritdoc}
     */
    public static function supports($entity_type, $bundle, $field_name, FieldDefinitionInterface $field)
    {
        $allowed = ['entitygroupfield'];

        return false !== in_array($field->getType(), $allowed);
    }

    /**
     * {@inheritdoc}
     */
    public function push(PushIntent $intent)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function pull(PullIntent $intent)
    {
    }
}
