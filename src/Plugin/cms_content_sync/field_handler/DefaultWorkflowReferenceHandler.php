<?php

namespace Drupal\cms_content_sync\Plugin\cms_content_sync\field_handler;

use Drupal\cms_content_sync\Plugin\EntityReferenceHandlerBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\workflow\Entity\WorkflowScheduledTransition;
use Drupal\workflow\Entity\WorkflowState;
use EdgeBox\SyncCore\Interfaces\Configuration\IDefineEntityType;

/**
 * Providing a minimalistic implementation for any field type.
 *
 * @FieldHandler(
 *   id = "cms_content_sync_default_workflow_reference_handler",
 *   label = @Translation("Default Workflow Reference"),
 *   weight = 90
 * )
 */
class DefaultWorkflowReferenceHandler extends EntityReferenceHandlerBase
{
    /**
     * {@inheritdoc}
     */
    public static function supports($entity_type, $bundle, $field_name, FieldDefinitionInterface $field)
    {
        $supported = [
            'workflow',
        ];

        if (!in_array($field->getType(), $supported)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function definePropertyAtType(IDefineEntityType $type_definition)
    {
        $type_definition->addReferenceProperty($this->fieldName, $this->fieldDefinition->getLabel(), true, $this->fieldDefinition->isRequired(), $this->fieldDefinition->getType());
    }

    /**
     * Get a list of array keys from $entity->field_* values that should be
     * ignored (unset before push).
     *
     * @return array
     */
    protected function getInvalidSubfields()
    {
        return ['_attributes', 'workflow_transition'];
    }

    /**
     * {@inheritDoc}
     */
    protected function loadReferencedEntityFromFieldValue($value)
    {
        if (empty($value['to_sid'])) {
            if (empty($value['value'])) {
                return null;
            }

            return WorkflowState::load($value['value']);
        }

        return WorkflowState::load($value['to_sid']);
    }

    /**
     * @see \Drupal\workflow\...\WorkflowTransitionElement
     *
     * @param mixed $schedule_values
     */
    protected function getScheduledTime($schedule_values)
    {
        // Fetch the (scheduled) timestamp to change the state.
        // Override $timestamp.
        $scheduled_date_time = implode(' ', [
            $schedule_values['workflow_scheduled_date'],
            $schedule_values['workflow_scheduled_hour'],
        ]);
        $timezone = $schedule_values['workflow_scheduled_timezone'];
        $old_timezone = date_default_timezone_get();
        date_default_timezone_set($timezone);
        $timestamp = strtotime($scheduled_date_time);
        date_default_timezone_set($old_timezone);

        return $timestamp;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFieldValuesForReference($reference, $intent, $details)
    {
        // The WorkflowManager doesn't respect the values set for 'scheduled', so
        // we need to manually create a transition in this case.
        if (!empty($details['workflow_scheduling']['scheduled']) && '1' === $details['workflow_scheduling']['scheduled']) {
            $state = WorkflowState::load($details['to_sid']);
            $transition = new WorkflowScheduledTransition();
            $transition->wid = $state->getWorkflowId();
            $transition->workflow = $state->getWorkflow();
            $transition->from_sid = $details['value'];
            $transition->field_name = $this->fieldName;
            $transition->setTargetEntity($intent->getEntity());
            $transition->setValues($details['to_sid'], null, $this->getScheduledTime($details['workflow_scheduling']['date_time']), $details['comment'], $details['force']);
            $transition->save();

            return [
                'workflow_transition' => $transition,
            ];
        }

        return [
        ];
    }
}
