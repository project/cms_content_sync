<?php

namespace Drupal\cms_content_sync\Plugin;

use Drupal\cms_content_sync\Controller\ContentSyncSettings;
use Drupal\cms_content_sync\Controller\Migration;
use Drupal\cms_content_sync\Entity\EntityStatus;
use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\EntityStatusProxy;
use Drupal\cms_content_sync\Event\BeforeEntityPull;
use Drupal\cms_content_sync\Event\BeforeEntityPush;
use Drupal\cms_content_sync\Exception\SyncException;
use Drupal\cms_content_sync\Plugin\Type\EntityHandlerPluginManager;
use Drupal\cms_content_sync\PullIntent;
use Drupal\cms_content_sync\PushIntent;
use Drupal\cms_content_sync\SyncIntent;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\SynchronizableInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Render\RenderContext;
use Drupal\crop\Entity\Crop;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;
use Drupal\node\NodeInterface;
use EdgeBox\SyncCore\V2\Raw\Model\RemoteEntityListResponse;
use EdgeBox\SyncCore\V2\Raw\Model\RemoteEntitySummary;
use EdgeBox\SyncCore\V2\Raw\Model\RemoteEntityTranslationDetails;
use EdgeBox\SyncCore\V2\Raw\Model\RemoteRequestQueryParamsEntityList;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Common base class for entity handler plugins.
 *
 * @see \Drupal\cms_content_sync\Annotation\EntityHandler
 * @see \Drupal\cms_content_sync\Plugin\EntityHandlerInterface
 * @see plugin_api
 *
 * @ingroup third_party
 */
abstract class EntityHandlerBase extends PluginBase implements ContainerFactoryPluginInterface, EntityHandlerInterface
{
    public const USER_PROPERTY = null;
    public const USER_REVISION_PROPERTY = null;
    public const REVISION_TRANSLATION_AFFECTED_PROPERTY = null;

    /**
     * A logger instance.
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    protected $entityTypeName;
    protected $bundleName;
    protected $settings;

    /**
     * A sync instance.
     *
     * @var \Drupal\cms_content_sync\Entity\Flow
     */
    protected $flow;

    /**
     * Constructs a Drupal\rest\Plugin\ResourceBase object.
     *
     * @param array                    $configuration
     *                                                    A configuration array containing information about the plugin instance
     * @param string                   $plugin_id
     *                                                    The plugin_id for the plugin instance
     * @param mixed                    $plugin_definition
     *                                                    The plugin implementation definition
     * @param \Psr\Log\LoggerInterface $logger
     *                                                    A logger instance
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->logger = $logger;
        $this->entityTypeName = $configuration['entity_type_name'];
        $this->bundleName = $configuration['bundle_name'];
        $this->settings = $configuration['settings'];
        $this->flow = $configuration['sync'];
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('cms_content_sync')
    );
    }

    /**
     * {@inheritdoc}
     */
    public function getAllowedPushOptions()
    {
        return [
            PushIntent::PUSH_DISABLED,
            PushIntent::PUSH_AUTOMATICALLY,
            PushIntent::PUSH_AS_DEPENDENCY,
            PushIntent::PUSH_MANUALLY,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAllowedPullOptions()
    {
        return [
            PullIntent::PULL_DISABLED,
            PullIntent::PULL_MANUALLY,
            PullIntent::PULL_AUTOMATICALLY,
            PullIntent::PULL_AS_DEPENDENCY,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function updateEntityTypeDefinition(&$definition)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getHandlerSettings($current_values, $type = 'both')
    {
        $options = [];

        $no_menu_link_push = [
            'brick',
            'field_collection_item',
            'menu_link_content',
            'paragraph',
        ];

        if (!in_array($this->entityTypeName, $no_menu_link_push) && 'pull' !== $type) {
            $options['export_menu_items'] = [
                '#type' => 'checkbox',
                '#title' => 'Push menu items',
                '#default_value' => isset($current_values['export_menu_items']) && 0 === $current_values['export_menu_items'] ? 0 : 1,
            ];
        }

        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function validateHandlerSettings(array &$form, FormStateInterface $form_state, string $entity_type_name, string $bundle_name, $current_values)
    {
        // No settings means no validation.
    }

    /**
     * Pull the remote entity.
     *
     * {@inheritdoc}
     */
    public function pull(PullIntent $intent)
    {
        $action = $intent->getAction();

        /**
         * @var null|\Drupal\Core\Entity\EntityInterface $entity
         */
        $entity = $intent->getEntity();

        $intent->startTimer('before-pull-event');
        // Allow other modules to extend the EntityHandlerBase pull.
        // Dispatch ExtendEntityPull.
        $event = new BeforeEntityPull($entity, $intent);
        \Drupal::service('event_dispatcher')->dispatch(BeforeEntityPull::EVENT_NAME, $event);
        $intent->stopTimer('before-pull-event');

        // Allow other modules to ask Content Sync to ignore the pull operation.
        if ($event->ignore) {
            $intent->setIgnoreMessage('An event handler chose to ignore this entity explicitly.');

            return false;
        }

        if ($this->ignorePull($intent)) {
            // Still pull updates on translations if only the root language should
            // be ignored. E.g. because the skip-unchanged flag is set and the
            // root translation didn't change but another translation did.
            if ($entity && $this->isEntityTypeTranslatable($entity) && SyncIntent::ACTION_DELETE !== $action) {
                if ($bundle_entity_type = $entity->getEntityType()->getBundleEntityType()) {
                    $bundle_entity_type = \Drupal::entityTypeManager()->getStorage($bundle_entity_type)->load($entity->bundle());
                    if (($bundle_entity_type instanceof RevisionableEntityBundleInterface && $bundle_entity_type->shouldCreateNewRevision()) || 'field_collection' == $bundle_entity_type->getEntityTypeId() || 'paragraph' == $entity->getEntityTypeId()) {
                        $entity->setNewRevision(true);
                    }
                }

                // Return TRUE if a translation is updated OR deleted.
                return $this->pullTranslations($intent, $entity);
            }

            return false;
        }

        if (SyncIntent::ACTION_DELETE == $action) {
            if ($entity) {
                return $this->deleteEntity($entity);
            }
            // Already done means success.
            if ($intent->getEntityStatus()->isDeleted()) {
                return true;
            }

            $intent->setIgnoreMessage('The entity does not exist.');

            return false;
        }

        if ($entity) {
            if ($bundle_entity_type = $entity->getEntityType()->getBundleEntityType()) {
                $bundle_entity_type = \Drupal::entityTypeManager()->getStorage($bundle_entity_type)->load($entity->bundle());
                if (($bundle_entity_type instanceof RevisionableEntityBundleInterface && $bundle_entity_type->shouldCreateNewRevision()) || 'field_collection' == $bundle_entity_type->getEntityTypeId() || 'paragraph' == $entity->getEntityTypeId()) {
                    $entity->setNewRevision(true);
                }
            }
        } else {
            $intent->startTimer('create-new');
            $entity = $this->createNew($intent);
            $intent->stopTimer('create-new');

            if (!$entity) {
                throw new SyncException(SyncException::CODE_ENTITY_API_FAILURE);
            }

            $intent->setEntity($entity);
        }

        if ($entity instanceof FieldableEntityInterface && !$this->setEntityValues($intent)) {
            $intent->setIgnoreMessage('Failed to set entity values.');

            return false;
        }

        $new_version = $intent->getVersionId($this->isEntityTypeTranslatable($entity) && $entity->isDefaultTranslation());
        if ($new_version) {
            $intent->getEntityStatus()->setData([EntityStatus::DATA_TRANSLATIONS, $entity->language()->getId(), EntityStatus::DATA_TRANSLATIONS_LAST_PULLED_VERSION_ID], $new_version);
        }

        return true;
    }

    /**
     * @param \Drupal\cms_content_sync\PushIntent $intent
     *
     * @throws \Drupal\cms_content_sync\Exception\SyncException
     *
     * @return string
     */
    public function getViewUrl(EntityInterface $entity)
    {
        if (!$entity->hasLinkTemplate('canonical')) {
            if (!$entity->hasLinkTemplate('edit-form')) {
                throw new SyncException('No canonical link template found for entity '.$entity->getEntityTypeId().'.'.$entity->bundle().' '.$entity->id().'. Please overwrite the handler to provide a URL.');
            }

            try {
                $url = $entity->toUrl('edit-form', [
                    'absolute' => true,
                    'language' => $entity->language(),
                    // Workaround for PathProcessorAlias::processOutbound to explicitly ignore us
                    // as we always want the pure, unaliased e.g. /node/:id path because
                    // we don't use the URL for end-users but for editors and it has to
                    // be reliable (aliases can be removed or change).
                    'alias' => true,
                ]);

                return $url->toString();
            } catch (\Exception $e) {
                throw new SyncException(SyncException::CODE_UNEXPECTED_EXCEPTION, $e);
            }
        }

        try {
            $url = $entity->toUrl('canonical', [
                'absolute' => true,
                'language' => $entity->language(),
                // Workaround for PathProcessorAlias::processOutbound to explicitly ignore us
                // as we always want the pure, unaliased e.g. /node/:id path because
                // we don't use the URL for end-users but for editors and it has to
                // be reliable (aliases can be removed or change).
                'alias' => true,
            ]);

            return $url->toString();
        } catch (\Exception $e) {
            throw new SyncException(SyncException::CODE_UNEXPECTED_EXCEPTION, $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getForbiddenFields()
    {
        /**
         * @var \Drupal\Core\Entity\EntityTypeInterface $entity_type_entity
         */
        $entity_type_entity = \Drupal::service('entity_type.manager')
            ->getStorage($this->entityTypeName)
            ->getEntityType();

        return [
            // These basic fields are already taken care of, so we ignore them
            // here.
            ...EntityHandlerPluginManager::mapById($entity_type_entity) ? [] : [$entity_type_entity->getKey('id')],
            $entity_type_entity->getKey('revision'),
            $entity_type_entity->getKey('bundle'),
            $entity_type_entity->getKey('uuid'),
            $entity_type_entity->getKey('label'),
            // These are not relevant or misleading when synchronized.
            'revision_default',
            'revision_translation_affected',
            'content_translation_outdated',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function push(PushIntent $intent, EntityInterface $entity = null)
    {
        if ($this->ignorePush($intent)) {
            return false;
        }

        if (!$entity) {
            $entity = $intent->getEntity();
        }

        // Base info.
        $name = $this->getEntityName($entity, $intent);
        // Focal point for example has no label.
        if (!$name) {
            $name = 'Unnamed '.$entity->getEntityTypeId().'.'.$entity->bundle();
        }
        $intent->getOperation()->setName($name, $intent->getActiveLanguage());

        // Menu items.
        if ($this->pushReferencedMenuItems()) {
            $intent->startTimer('menu-items');
            $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
            /**
             * @var \Drupal\Core\Menu\MenuLinkManager $menu_link_manager
             */
            $menu_items = $menu_link_manager->loadLinksByRoute('entity.'.$this->entityTypeName.'.canonical', [$this->entityTypeName => $entity->id()]);
            $values = [];

            $form_values = _cms_content_sync_submit_cache($entity->getEntityTypeId(), $entity->uuid());

            foreach ($menu_items as $menu_item) {
                if (!($menu_item instanceof MenuLinkContent)) {
                    continue;
                }

                /**
                 * @var \Drupal\menu_link_content\Entity\MenuLinkContent $item
                 */
                $item = \Drupal::service('entity.repository')
                    ->loadEntityByUuid('menu_link_content', $menu_item->getDerivativeId());
                if (!$item) {
                    continue;
                }

                // Menu item has just been disabled => Ignore push in this case.
                if (isset($form_values['menu']) && $form_values['menu']['id'] == 'menu_link_content:'.$item->uuid()) {
                    if (!$form_values['menu']['enabled']) {
                        continue;
                    }
                }

                $details = [];
                $details['enabled'] = $item->get('enabled')->value;

                try {
                    $values[] = $intent->addDependency($item, $details);
                }
                // Menu items are not a hard dependency, so we ignore failures (e.g. the handler denying the push as the menu is restricted).
                catch (\Exception $e) {
                }
            }

            $intent->setProperty('menu_items', $values);
            $intent->stopTimer('menu-items');
        }

        // Preview.
        $view_mode = $this->flow->getController()->getPreviewType($entity->getEntityTypeId(), $entity->bundle());
        if (Flow::PREVIEW_DISABLED != $view_mode) {
            $intent->startTimer('preview');
            // Always use the standard theme for rendering the previews.
            $config_factory = \Drupal::service('config.factory');
            $theme_manager = \Drupal::service('theme.manager');
            $theme_initialization = \Drupal::service('theme.initialization');
            $default_theme = $config_factory->get('system.theme')->get('default');
            $current_active_theme = $theme_manager->getActiveTheme();
            if ($default_theme && $default_theme != $current_active_theme->getName()) {
                $theme_manager->setActiveTheme($theme_initialization->initTheme($default_theme));
            }

            try {
                $entityTypeManager = \Drupal::entityTypeManager();
                $view_builder = $entityTypeManager->getViewBuilder($this->entityTypeName);

                $preview = $view_builder->view($entity, $view_mode);
                $rendered = \Drupal::service('renderer');
                $html = $rendered->executeInRenderContext(
                    new RenderContext(),
                    function () use ($rendered, $preview) {
                        return $rendered->render($preview);
                    }
                );
                $this->setPreviewHtml($html, $intent);
            } finally {
                // Revert the active theme, this is done inside a finally block so it is
                // executed even if an exception is thrown during rendering.
                if ($default_theme != $current_active_theme->getName()) {
                    $theme_manager->setActiveTheme($current_active_theme);
                }
            }
            $intent->stopTimer('preview');
        }

        // Source URL.
        $intent->getOperation()->setSourceDeepLink($this->getViewUrl($entity), $intent->getActiveLanguage());

        // Fields.
        if ($entity instanceof FieldableEntityInterface) {
            $intent->startTimer('fields');
            /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
            $entityFieldManager = \Drupal::service('entity_field.manager');
            $type = $entity->getEntityTypeId();
            $bundle = $entity->bundle();
            $field_definitions = $entityFieldManager->getFieldDefinitions($type, $bundle);

            foreach ($field_definitions as $key => $field) {
                if ($intent->shouldIgnoreProperty($key)) {
                    continue;
                }

                $handler = $this->flow->getController()->getFieldHandler($type, $bundle, $key);

                if (!$handler) {
                    continue;
                }

                $handler->push($intent);
            }
            $intent->stopTimer('fields');
        }

        // Translations.
        if (!$intent->getActiveLanguage()
      && $this->isEntityTypeTranslatable($entity) && !$intent->isIndividualTranslation()) {
            $languages = array_keys($entity->getTranslationLanguages(false));
            $allowed_languages = $this->flow->getController()->getAllowedLanguages();

            foreach ($languages as $language) {
                if (!empty($allowed_languages) && !in_array($language, $allowed_languages)) {
                    continue;
                }

                $timer_name = 'translation-'.$language;
                $intent->startTimer($timer_name);

                $intent->changeTranslationLanguage($language);
                /**
                 * @var \Drupal\Core\Entity\FieldableEntityInterface $translation
                 */
                $translation = $entity->getTranslation($language);
                $this->push($intent, $translation);

                $intent->stopTimer($timer_name);
            }

            $intent->changeTranslationLanguage();
        }

        // Paragraphs doesn't use the entity created / entity changed interfaces unfortunately but the property is named
        // changed / created, too. So we declare it as an integer property but then send an object which we have to fix here.
        if (!Migration::useV2() && in_array($entity->getEntityTypeId(), ['paragraphs_library_item', 'paragraph'])) {
            if (is_array($intent->getProperty('created'))) {
                $this->setDateProperty($intent, 'created', $entity->get('created')->value);
            }
            if (is_array($intent->getProperty('changed'))) {
                $this->setDateProperty($intent, 'changed', $entity->get('changed')->value);
            }
        }

        $intent->startTimer('before-push-event');
        // Allow other modules to extend the EntityHandlerBase push.
        // Dispatch entity push event.
        \Drupal::service('event_dispatcher')->dispatch(BeforeEntityPush::EVENT_NAME, new BeforeEntityPush($entity, $intent));
        $intent->stopTimer('before-push-event');

        return true;
    }

    /**
     * Check if the entity should not be ignored from the push.
     *
     * @param \Drupal\cms_content_sync\SyncIntent          $intent
     *                                                             The Sync Core Request
     * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
     *                                                             The entity that could be ignored
     * @param string                                       $reason
     *                                                             The reason why the entity should be ignored from the push
     * @param string                                       $action
     *                                                             The action to apply
     *
     * @throws \Exception
     *
     * @return bool
     *              Whether or not to ignore this push request
     */
    public function ignorePush(PushIntent $intent)
    {
        $reason = $intent->getReason();
        $action = $intent->getAction();

        if (PushIntent::PUSH_AUTOMATICALLY == $reason) {
            if (PushIntent::PUSH_MANUALLY == $this->settings['export']) {
                $intent->setIgnoreMessage('The entity is supposed to be pushed manually but was pushed automatically.');

                return true;
            }
        }

        if (SyncIntent::ACTION_UPDATE == $action) {
            $status_proxy = new EntityStatusProxy(EntityStatus::getInfosForEntity($intent->getEntityType(), $intent->getUuid()));
            if ($status_proxy->isOverriddenLocally()) {
                $intent->setIgnoreMessage('The entity is overridden locally.');

                return true;
            }
        }

        $allowed_languages = $this->flow->getController()->getAllowedLanguages();
        if (!empty($allowed_languages) && !in_array($intent->getEntity()->language()->getId(), $allowed_languages)) {
            $intent->setIgnoreMessage('The language of the entity ('.$intent->getEntity()->language()->getId().') is not allowed.');

            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getSyncCoreList(Flow $flow, RemoteRequestQueryParamsEntityList $queryObject)
    {
        $entity_type = $queryObject->getNamespaceMachineName();
        $bundle = $queryObject->getMachineName();

        $page = (int) $queryObject->getPage();
        if (!$page) {
            $page = 0;
        }

        $items_per_page = (int) $queryObject->getItemsPerPage();
        if (!$items_per_page) {
            $items_per_page = 0;
        }

        // Need to convert miliseconds to seconds.
        $changed_after = $queryObject->getChangedAfter() ? floor((int) $queryObject->getChangedAfter() / 1000) : null;

        $search = empty($query['search']) ? null : $query['search'];

        $skip = $page * $items_per_page;

        $database = \Drupal::database();

        $entity_type_storage = \Drupal::entityTypeManager()->getStorage($entity_type);
        $bundle_key = $entity_type_storage->getEntityType()->getKey('bundle');
        $id_key = $entity_type_storage->getEntityType()->getKey('id');
        $langcode_key = $entity_type_storage->getEntityType()->getKey('langcode');
        if ($entity_type_storage instanceof SqlContentEntityStorage) {
            $base_table = $entity_type_storage->getBaseTable();
            $data_table = $entity_type_storage->getDataTable();
            $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);
            $prefix = '';
        } elseif ($entity_type_storage instanceof ConfigEntityStorage) {
            $base_table = 'config';
            $definitions = [];
            $prefix = $entity_type_storage->getEntityType()->getConfigPrefix().'.';
        } else {
            throw new \Exception('Entity type '.$entity_type.' uses unknown storage.');
        }

        $query = $database->select($base_table, 'bt');
        if ($entity_type_storage instanceof ConfigEntityStorage) {
            $query
                ->fields('bt', ['name']);
            $query
                ->condition('name', $query->escapeLike($prefix).'%', 'LIKE');
        } else {
            if (!empty($bundle_key)) {
                $query
                    ->condition('bt.'.$bundle_key, $bundle);
            }
            $query
                ->fields('bt', [$id_key]);
        }

        // Join data table if the entity has one (files for example have none).
        if (!empty($data_table)) {
            $query->join($data_table, 'dt', 'dt.'.$id_key.'=bt.'.$id_key.($langcode_key ? ' AND dt.'.$langcode_key.'=bt.'.$langcode_key : ''));
        }
        $property_table_prefix = empty($data_table) ? 'bt' : 'dt';

        $label_property = $entity_type_storage->getEntityType()->getKey('label');
        if (!empty($search) && !empty($label_property)) {
            $query
                ->condition($property_table_prefix.'.'.$label_property, '%'.$database->escapeLike($search).'%', 'LIKE');
        }

        // Ignore unpublished entities based on the flow configuration.
        $entity_type_config = $flow->getController()->getEntityTypeConfig($entity_type, $bundle);
        $handler_settings = $entity_type_config['handler_settings'];
        $status_key = 'config' === $base_table ? null : $entity_type_storage->getEntityType()->getKey('status');
        if (true == $handler_settings['ignore_unpublished'] && !empty($status_key)) {
            if (true == $handler_settings['allow_explicit_unpublishing']) {
                // Join the entity status table to check if the entity has been exported before to allow explizit unpublishing.
                $query->leftJoin('cms_content_sync_entity_status', 'cses', 'cses.entity_uuid = bt.uuid');

                // If status is 0 and the entity has been exported before.
                $and = $query->andConditionGroup()
                    ->condition($property_table_prefix.'.'.$status_key, '0')
                    ->condition('cses.last_export', 0, '>');

                $or = $query->orConditionGroup()
                    ->condition($and)
                    ->condition($property_table_prefix.'.'.$status_key, '1');

                $query->condition($or);
            } else {
                $query
                    ->condition($property_table_prefix.'.'.$status_key, '1');
            }
        }

        if (!empty($definitions['created'])) {
            if ($changed_after) {
                $query
                    ->condition($property_table_prefix.'.created', $changed_after, '>');
            }
            $query
                ->orderBy($property_table_prefix.'.created', 'ASC');
        } elseif ('config' === $base_table) {
            $query->orderBy('bt.name', 'ASC');
        } else {
            $query->orderBy('bt.'.$id_key, 'ASC');
        }
        $total_number_of_items = (int) $query->countQuery()->execute()->fetchField();

        $items = [];

        if ($total_number_of_items && $items_per_page) {
            $ids = $query
                ->range($skip, $items_per_page)
                ->execute()
                ->fetchAll(\PDO::FETCH_COLUMN);
            if ($prefix) {
                foreach ($ids as $i => $id) {
                    $ids[$i] = substr($id, strlen($prefix));
                }
            }
            $entities = $entity_type_storage->loadMultiple($ids);
            foreach ($entities as $entity) {
                $items[] = $this->getSyncCoreListItem($flow, $entity, EntityStatus::getInfosForEntity($entity->getEntityTypeId(), $entity->uuid(), ['flow' => $flow->id()]));
            }
        }

        if (!$items_per_page) {
            $number_of_pages = $total_number_of_items;
        } else {
            $number_of_pages = ceil($total_number_of_items / $items_per_page);
        }

        $result = new RemoteEntityListResponse();
        $result->setPage($page);
        $result->setNumberOfPages($number_of_pages);
        $result->setItemsPerPage($items_per_page);
        $result->setTotalNumberOfItems($total_number_of_items);
        $result->setItems($items);

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function getSyncCoreListItem(?Flow $flow, ?object $entity, array $statuses)
    {
        $item = new RemoteEntitySummary();

        $pools = [];
        $last_push = null;
        foreach ($statuses as $status) {
            $pools[] = $status->get('pool')->value;
            if ($status->getLastPush()) {
                if (!$last_push || $status->getLastPush() > $last_push->getLastPush()) {
                    $last_push = $status;
                }
            }
        }

        $item->setPoolMachineNames($pools);

        $item->setIsSource(empty($statuses) || (bool) $last_push);

        if ($entity) {
            $item->setEntityTypeNamespaceMachineName($entity->getEntityTypeId());
            $item->setEntityTypeMachineName($entity->bundle());
            $item->setEntityTypeVersion(Flow::getEntityTypeVersion($entity->getEntityTypeId(), $entity->bundle()));
            $item->setRemoteUuid($entity->uuid());
            if (EntityHandlerPluginManager::mapById($entity->getEntityTypeId())) {
                $item->setRemoteUniqueId($entity->id());
            }
            $item->setLanguage($entity->language()->getId());
            $item->setName($entity->label());

            $item->setIsDeleted(false);

            if ($flow) {
                $config = $flow->getController()->getEntityTypeConfig($entity->getEntityTypeId(), $entity->bundle());
                $handler = $flow->getController()->getEntityTypeHandler($entity->getEntityTypeId(), $entity->bundle(), $config);
            } else {
                $entity_plugin_manager = \Drupal::service('plugin.manager.cms_content_sync_entity_handler');
                $entity_handlers = $entity_plugin_manager->getHandlerOptions($entity->getEntityTypeId(), $entity->bundle(), true);
                $entity_handler_names = array_keys($entity_handlers);
                $handler_id = reset($entity_handler_names);
                $handler = $entity_plugin_manager->createInstance($handler_id, [
                    'entity_type_name' => $entity->getEntityTypeId(),
                    'bundle_name' => $entity->bundle(),
                    'settings' => [],
                    'sync' => null,
                ]);
            }
            $item->setViewUrl($handler->getViewUrl($entity));

            if ($flow && $entity instanceof TranslatableInterface) {
                $translations = [];
                $config = $flow->getController()->getEntityTypeConfig($entity->getEntityTypeId(), $entity->bundle());
                $handler = $flow->getController()->getEntityTypeHandler($entity->getEntityTypeId(), $entity->bundle(), $config);
                foreach ($entity->getTranslationLanguages(false) as $language) {
                    $translation_dto = new RemoteEntityTranslationDetails();
                    $translation_dto->setLanguage($language->getId());
                    $view_url = $handler->getViewUrl($entity->getTranslation($language->getId()));
                    $translation_dto->setViewUrl($view_url);
                    $translations[] = $translation_dto;
                }
                $item->setTranslations($translations);
            }
        } else {
            $status = $last_push ? $last_push : reset($statuses);
            $item->setEntityTypeNamespaceMachineName($status->getEntityTypeName());
            $item->setEntityTypeMachineName('*');
            $item->setLanguage('*');
            $item->setName('?');
            $item->setEntityTypeVersion($status->getEntityTypeVersion());
            $item->setRemoteUuid($status->getUuid());

            $item->setIsDeleted($status->isDeleted());
        }

        return $item;
    }

    /**
     * Whether or not menu item references should be pushed.
     *
     * @return bool
     */
    protected function pushReferencedMenuItems()
    {
        if (!isset($this->settings['handler_settings']['export_menu_items'])) {
            return true;
        }

        return 0 !== $this->settings['handler_settings']['export_menu_items'];
    }

    /**
     * Check if the pull should be ignored.
     *
     * @return bool
     *              Whether or not to ignore this pull request
     */
    protected function ignorePull(PullIntent $intent)
    {
        $reason = $intent->getReason();
        $action = $intent->getAction();

        if (PullIntent::PULL_AUTOMATICALLY == $reason) {
            if (PullIntent::PULL_MANUALLY == $this->settings['import']) {
                // Once pulled manually, updates will arrive automatically.
                if ((PullIntent::PULL_AUTOMATICALLY != $reason || PullIntent::PULL_MANUALLY != $this->settings['import']) || SyncIntent::ACTION_CREATE == $action) {
                    $intent->setIgnoreMessage('The entity is supposed to be pulled manually but was pulled automatically.');

                    return true;
                }
            }
        }

        if (SyncIntent::ACTION_UPDATE == $action) {
            $behavior = $this->settings['import_updates'];
            if (PullIntent::PULL_UPDATE_IGNORE == $behavior) {
                $intent->setIgnoreMessage('The Flow ignores entity updates.');

                return true;
            }
        }

        if (SyncIntent::ACTION_DELETE != $action) {
            $entity_type = \Drupal::entityTypeManager()->getDefinition($intent->getEntityType());
            if ($entity_type->getKey('langcode')) {
                $langcode = $intent->getProperty($entity_type->getKey('langcode'));
                while (is_array($langcode)) {
                    $langcode = reset($langcode);
                }

                if (!empty($langcode)) {
                    if (!\Drupal::service('language_manager')->getLanguage($langcode)) {
                        $intent->setIgnoreMessage('The default language of this entity ('.$langcode.') does not exist.');

                        return true;
                    }

                    $allowed_languages = $this->flow->getController()->getAllowedLanguages();
                    if (!empty($allowed_languages) && !in_array($langcode, $allowed_languages)) {
                        $intent->setIgnoreMessage('The default language of this entity ('.$langcode.') is not allowed.');

                        return true;
                    }
                }
            }

            $entity = $intent->getEntity();
            if ($entity) {
                $new_version = $intent->getVersionId($this->isEntityTypeTranslatable($entity) && $entity->isDefaultTranslation());
                if ($intent->getSkipUnchanged() && $new_version && $entity) {
                    $previous_version = $intent->getEntityStatus()->getData([EntityStatus::DATA_TRANSLATIONS, $entity->language()->getId(), EntityStatus::DATA_TRANSLATIONS_LAST_PULLED_VERSION_ID]);
                    if ($previous_version && $previous_version === $new_version) {
                        // If this is the default language, we may have untranslatable entity reference fields like
                        // paragraph fields that still must be updated for a new revision. So this optimization is
                        // not allowed for the default language.
                        // This is now handled by providing TRUE in ->getVersionId() above.
                        //if (!$entity->isDefaultTranslation()) {
                        $intent->setIgnoreMessage("The entity version hash didn't change.");

                        return true;
                        //}
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check whether the entity type supports having a label.
     *
     * @return bool
     */
    protected function hasLabelProperty()
    {
        return true;
    }

    /**
     * Get the base entity properties that must be passed to the entities constructor.
     *
     * * @return array
     */
    protected function getBaseEntityProperties(PullIntent $intent)
    {
        $entity_type = \Drupal::entityTypeManager()->getDefinition($intent->getEntityType());

        $base_data = [];

        if (EntityHandlerPluginManager::mapById($intent->getEntityType())) {
            $base_data['id'] = $intent->getId();
        }

        if ($this->hasLabelProperty()) {
            $base_data[$entity_type->getKey('label')] = $intent->getOperation()->getName();
        }

        // Required as field collections share the same property for label and bundle.
        $base_data[$entity_type->getKey('bundle')] = $intent->getBundle();

        $base_data[$entity_type->getKey('uuid')] = $intent->getUuid();
        if ($entity_type->getKey('langcode')) {
            $base_data[$entity_type->getKey('langcode')] = $intent->getProperty($entity_type->getKey('langcode'));
        }

        return $base_data;
    }

    /**
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     *
     * @return \Drupal\Core\Entity\EntityInterface
     */
    protected function createNew(PullIntent $intent)
    {
        $base_data = $this->getBaseEntityProperties($intent);

        $storage = \Drupal::entityTypeManager()->getStorage($intent->getEntityType());

        return $storage->create($base_data);
    }

    /**
     * Delete a entity.
     *
     * @param \Drupal\Core\Entity\EntityInterface $entity
     *                                                    The entity to delete
     *
     * @throws \Drupal\cms_content_sync\Exception\SyncException
     *
     * @return bool
     *              Returns TRUE or FALSE for the deletion process
     */
    protected function deleteEntity(EntityInterface $entity)
    {
        try {
            $entity->delete();
        } catch (\Exception $e) {
            throw new SyncException(SyncException::CODE_ENTITY_API_FAILURE, $e);
        }

        return true;
    }

    /**
     * @param \Drupal\Core\Entity\EntityInterface $entity
     * @param \Drupal\cms_content_sync\PullIntent $intent
     *
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
    protected function saveEntity($entity, $intent)
    {
        $entity->save();
    }

    /**
     * Set the values for the pulled entity.
     *
     * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
     *                                                             The translation of the entity
     *
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Drupal\cms_content_sync\Exception\SyncException
     *
     * @return bool
     *              Returns TRUE when the values are set
     *
     * @see Flow::PULL_*
     */
    protected function setEntityValues(PullIntent $intent, FieldableEntityInterface $entity = null)
    {
        if (!$entity) {
            $entity = $intent->getEntity();
        }

        if ($entity instanceof SynchronizableInterface) {
            $entity->setSyncing(true);
        }

        /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
        $entityFieldManager = \Drupal::service('entity_field.manager');
        $type = $entity->getEntityTypeId();
        $bundle = $entity->bundle();
        $field_definitions = $entityFieldManager->getFieldDefinitions($type, $bundle);

        $entity_type = \Drupal::entityTypeManager()->getDefinition($intent->getEntityType());
        $label = $entity_type->getKey('label');
        if ($label && !$intent->shouldMergeChanges() && $this->hasLabelProperty()) {
            $entity->set($label, $intent->getOperation()->getName($intent->getActiveLanguage()));
        }

        $static_fields = $this->getStaticFields();

        $is_translatable = $this->isEntityTypeTranslatable($entity);
        $is_translation = $is_translatable && !$entity->isDefaultTranslation();
        $skip_not_translatable_fields = !$intent->isIndividualTranslation() && $is_translatable && $is_translation;
        $skip_save = !$intent->isIndividualTranslation() && $is_translatable && $is_translation;

        $user = \Drupal::currentUser();
        if (static::USER_PROPERTY && $entity->hasField(static::USER_PROPERTY) && !$intent->getEntityStatus()->isOverriddenLocally()) {
            $entity->set(static::USER_PROPERTY, [['target_id' => $user->id()]]);
        }
        if (static::USER_REVISION_PROPERTY && $entity->hasField(static::USER_REVISION_PROPERTY)) {
            $entity->set(static::USER_REVISION_PROPERTY, [['target_id' => $user->id()]]);
        }
        if (static::REVISION_TRANSLATION_AFFECTED_PROPERTY && $entity->hasField(static::REVISION_TRANSLATION_AFFECTED_PROPERTY)) {
            $entity->set(static::REVISION_TRANSLATION_AFFECTED_PROPERTY, 1);
        }

        $is_update = SyncIntent::ACTION_UPDATE === $intent->getAction() && !$intent->getIsNewTranslation();

        $intent->startTimer('fields');
        foreach ($field_definitions as $key => $field) {
            if ($intent->shouldIgnoreProperty($key)) {
                continue;
            }

            $handler = $this->flow->getController()->getFieldHandler($type, $bundle, $key);

            if (!$handler) {
                continue;
            }

            // This field cannot be updated.
            if (in_array($key, $static_fields) && $is_update) {
                continue;
            }

            if ($skip_not_translatable_fields && !$field->isTranslatable()) {
                continue;
            }

            if ('image' == $field->getType() || 'file' == $field->getType()) {
                // Focal Point takes information from the image field directly
                // so we have to set it before the entity is saved the first time.
                $data = $intent->getProperty($key);
                if (null === $data) {
                    $data = [];
                }
                foreach ($data as &$value) {
                    /**
                     * @var \Drupal\file\Entity\File $file
                     */
                    $file = $intent->loadEmbeddedEntity($value);
                    if ($file) {
                        if ('image' == $field->getType()) {
                            $moduleHandler = \Drupal::service('module_handler');
                            if ($moduleHandler->moduleExists('crop') && $moduleHandler->moduleExists('focal_point')) {
                                /**
                                 * @var \Drupal\crop\Entity\Crop $crop
                                 */
                                $crop = Crop::findCrop($file->getFileUri(), 'focal_point');
                                if ($crop) {
                                    $position = $crop->position();

                                    // Convert absolute to relative.
                                    $size = getimagesize($file->getFileUri());
                                    $value['focal_point'] = ($position['x'] / $size[0] * 100).','.($position['y'] / $size[1] * 100);
                                }
                            }
                        }
                    }
                }

                $intent->overwriteProperty($key, $data);
            }

            $handler->pull($intent);
        }
        $intent->stopTimer('fields');

        if (PullIntent::PULL_UPDATE_UNPUBLISHED === $this->flow->getController()->getEntityTypeConfig($this->entityTypeName, $this->bundleName)['import_updates']) {
            if ($entity instanceof NodeInterface) {
                if ($entity->id()) {
                    $entity->isDefaultRevision(false);
                } else {
                    $entity->setPublished(false);
                }
            }
        }

        if (!$intent->getActiveLanguage()) {
            $created = $this->getDateProperty($intent, 'created');
            // See https://www.drupal.org/project/drupal/issues/2833378
            if ($created && method_exists($entity, 'getCreatedTime') && method_exists($entity, 'setCreatedTime')) {
                if ($created !== $entity->getCreatedTime()) {
                    $entity->setCreatedTime($created);
                }
            }
            if ($entity instanceof EntityChangedInterface) {
                $entity->setChangedTime(time());
            }
        }

        if ($is_translatable && !$intent->getActiveLanguage()) {
            $this->pullTranslations($intent, $entity);
        }

        if (!$skip_save) {
            try {
                $intent->startTimer('save');
                $this->saveEntity($entity, $intent);
                $intent->stopTimer('save');
            } catch (\Exception $e) {
                throw new SyncException(SyncException::CODE_ENTITY_API_FAILURE, $e);
            }
        }

        if ($entity instanceof SynchronizableInterface) {
            $entity->setSyncing(false);
        }

        return true;
    }

    /**
     * Pull the translations. This will process translations that are embedded
     * in the request but also delete translations that are no longer present.
     *
     * @return bool whether anything was updated
     */
    protected function pullTranslations(PullIntent $intent, EntityInterface $entity)
    {
        $changed = false;

        $languages = $intent->getTranslationLanguages();

        if (!$intent->isIndividualTranslation()) {
            $allowed_languages = $this->flow->getController()->getAllowedLanguages();
            foreach ($languages as $language) {
                if (!\Drupal::service('language_manager')->getLanguage($language)) {
                    continue;
                }

                if (!empty($allowed_languages) && !in_array($language, $allowed_languages)) {
                    continue;
                }

                // Skip original translation that was already processed outside of this method.
                if ($entity->getUntranslated()->language()->getId() === $language) {
                    continue;
                }

                $timer_name = 'translation-'.$language;
                $intent->startTimer($timer_name);

                /**
                 * If the provided entity is fieldable, translations are as well.
                 *
                 * @var \Drupal\Core\Entity\FieldableEntityInterface $translation
                 */
                if ($entity->hasTranslation($language)) {
                    $translation = $entity->getTranslation($language);
                } else {
                    $translation = $entity->addTranslation($language);
                    $intent->setIsNewTranslation(true);
                }

                $intent->changeTranslationLanguage($language);
                if (!$this->ignorePull($intent)) {
                    if ($this->setEntityValues($intent, $translation)) {
                        $changed = true;
                        $new_version = $intent->getVersionId($entity->isDefaultTranslation());
                        if ($new_version) {
                            $intent->getEntityStatus()->setData([EntityStatus::DATA_TRANSLATIONS, $language, EntityStatus::DATA_TRANSLATIONS_LAST_PULLED_VERSION_ID], $new_version);
                        }
                    }
                }
                $intent->setIsNewTranslation(false);

                $intent->stopTimer($timer_name);
            }
        }

        // Delete translations that were deleted on master site.
        // If the entity was pulled embedded we have to be more careful as we
        // might run into timing issues- if users pull an older version of an
        // embedded entity we don't want to delete it's translations.
        if (boolval($this->settings['import_deletion_settings']['import_deletion']) && !$intent->wasPulledEmbedded()) {
            $existing = $entity->getTranslationLanguages(false);
            $remove = [];
            foreach ($existing as $language) {
                if (!in_array($language->getId(), $languages) && $language->getId() !== $entity->language()->getId()) {
                    $remove[] = $language->getId();
                }
            }

            if (count($remove)) {
                $changed = true;

                foreach ($remove as $language) {
                    $entity->removeTranslation($language);
                }

                try {
                    $this->saveEntity($entity, $intent);
                } catch (\Exception $e) {
                    throw new SyncException(SyncException::CODE_ENTITY_API_FAILURE, $e);
                }
            }
        }

        if (!$intent->isIndividualTranslation()) {
            $intent->changeTranslationLanguage();
        }

        return $changed;
    }

    protected function setDateProperty(SyncIntent $intent, string $name, int $timestamp)
    {
        if (Migration::useV2()) {
            $intent->setProperty($name, ['value' => $timestamp]);
        } else {
            $intent->setProperty($name, $timestamp);
        }
    }

    protected function getDateProperty(SyncIntent $intent, string $name)
    {
        $value = $intent->getProperty($name);
        if (is_array($value)) {
            return isset($value[0]['value']) ? $value[0]['value'] : $value['value'];
        }

        return (int) $value;
    }

    /**
     * Get a list of fields that can't be updated.
     *
     * @return string[]
     */
    protected function getStaticFields()
    {
        return [];
    }

    protected function getEntityName(EntityInterface $entity, PushIntent $intent)
    {
        return $entity->label();
    }

    protected function setPreviewHtml($html, PushIntent $intent)
    {
        if ($html) {
            // Turn relative URLs into absolute URLs using the current site's
            // base URL.
            // For images to show up in the pull dashboard the webserver may
            // still have to be configured to allow cross-origin embeds of files
            // i.e. from https://embed.cms-content-sync.io
            $base_url = ContentSyncSettings::getInstance()->getSiteBaseUrl();
            $parts = parse_url($base_url);
            $base_url_without_path = $parts['scheme'].'://'.$parts['host'].'/';
            $html = preg_replace_callback('@ (src|href)=("|\')/([^\'"]+)("|\')@', function ($matches) use ($base_url_without_path) {
                list($match, $attribute, $quote1, $path, $quote2) = $matches;

                return ' '.$attribute.'='.$quote1.$base_url_without_path.$path.$quote2;
            }, $html);
        }

        try {
            $intent->getOperation()->setPreviewHtml($html, $intent->getActiveLanguage());
        } catch (\Exception $error) {
            $entity = $intent->getEntity();

            $messenger = \Drupal::messenger();
            $messenger->addWarning(
                t(
                    'Failed to save preview for %label: %error',
                    [
                        '%error' => $error->getMessage(),
                        '%label' => $entity->label(),
                    ]
                )
            );

            \Drupal::logger('cms_content_sync')->error('Failed to save preview when pushing @type.@bundle @id @label: @error', [
                '@type' => $entity->getEntityTypeId(),
                '@bundle' => $entity->bundle(),
                '@id' => $entity->id(),
                '@label' => $entity->label(),
                '@error' => $error->getMessage(),
            ]);
        }
    }

    /**
     * @param \Drupal\Core\Entity\EntityInterface $entity
     *
     * @return bool
     */
    protected function isEntityTypeTranslatable($entity)
    {
        return $entity instanceof TranslatableInterface && $entity->getEntityType()->getKey('langcode');
    }
}
