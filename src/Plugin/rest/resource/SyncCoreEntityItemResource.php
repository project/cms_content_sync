<?php

namespace Drupal\cms_content_sync\Plugin\rest\resource;

use Drupal\cms_content_sync\Controller\Migration;
use Drupal\cms_content_sync\Entity\EntityStatus;
use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\Entity\Pool;
use Drupal\cms_content_sync\Exception\SyncException;
use Drupal\cms_content_sync\Plugin\Type\EntityHandlerPluginManager;
use Drupal\cms_content_sync\PullIntent;
use Drupal\cms_content_sync\PushIntent;
use Drupal\cms_content_sync\SyncCoreInterface\SyncCoreFactory;
use Drupal\cms_content_sync\SyncIntent;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Render\Renderer;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use EdgeBox\SyncCore\Interfaces\IApplicationInterface;
use EdgeBox\SyncCore\V2\Syndication\PushSingle;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides entity interfaces for Content Sync, allowing Sync Core v2 to
 * request and manipulate entities.
 *
 * @RestResource(
 *   id = "cms_content_sync_sync_core_entity_item",
 *   label = @Translation("Content Sync: Sync Core: Entity item"),
 *   uri_paths = {
 *     "canonical" = "/rest/cms-content-sync/v2/{flow_id}/{entity_type}/{entity_bundle}/{shared_entity_id}",
 *     "create" = "/rest/cms-content-sync/v2/{flow_id}/{entity_type}/{entity_bundle}/{shared_entity_id}"
 *   }
 * )
 */
class SyncCoreEntityItemResource extends ResourceBase
{
    /**
     * @var int CODE_OK All good
     */
    public const CODE_OK = 200;

    /**
     * @var int CODE_BAD_REQUEST The request is invalid
     */
    public const CODE_BAD_REQUEST = 400;

    /**
     * @var int CODE_INVALID_DATA The provided data could not be interpreted
     */
    public const CODE_INVALID_DATA = 401;

    /**
     * @var int CODE_NOT_FOUND The entity doesn't exist or can't be accessed
     */
    public const CODE_NOT_FOUND = 404;

    /**
     * @var int CODE_INTERNAL_SERVER_ERROR Unexpected error; try again later
     */
    public const CODE_INTERNAL_SERVER_ERROR = 500;

    /**
     * @var \Drupal\Core\Entity\EntityTypeBundleInfo
     */
    protected $entityTypeBundleInfo;

    /**
     * @var \Drupal\Core\Entity\EntityTypeManager
     */
    protected $entityTypeManager;

    /**
     * @var \Drupal\Core\Render\Renderer
     */
    protected $renderedManager;

    /**
     * @var \Drupal\Core\Entity\EntityRepositoryInterface
     */
    protected $entityRepository;

    /**
     * Constructs an object.
     *
     * @param array                                          $configuration
     *                                                                                A configuration array containing information about the plugin instance
     * @param string                                         $plugin_id
     *                                                                                The plugin_id for the plugin instance
     * @param mixed                                          $plugin_definition
     *                                                                                The plugin implementation definition
     * @param array                                          $serializer_formats
     *                                                                                The available serialization formats
     * @param \Psr\Log\LoggerInterface                       $logger
     *                                                                                A logger instance
     * @param \Drupal\Core\Entity\EntityTypeBundleInfo       $entity_type_bundle_info
     *                                                                                An entity type bundle info instance
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *                                                                                An entity type manager instance
     * @param \Drupal\Core\Render\Renderer                   $render_manager
     *                                                                                A rendered instance
     * @param \Drupal\Core\Entity\EntityRepositoryInterface  $entity_repository
     *                                                                                The entity repository interface
     */
    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        EntityTypeBundleInfo $entity_type_bundle_info,
        EntityTypeManagerInterface $entity_type_manager,
        Renderer $render_manager,
        EntityRepositoryInterface $entity_repository
    ) {
        parent::__construct(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $serializer_formats,
            $logger
        );

        $this->entityTypeBundleInfo = $entity_type_bundle_info;
        $this->entityTypeManager = $entity_type_manager;
        $this->renderedManager = $render_manager;
        $this->entityRepository = $entity_repository;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(
        ContainerInterface $container,
        array $configuration,
        $plugin_id,
        $plugin_definition
    ) {
        return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('entity.repository')
    );
    }

    public function get($flow_id, $entity_type, $entity_bundle, $shared_entity_id)
    {
        $flow = Flow::getAll()[$flow_id] ?? null;
        if (empty($flow)) {
            if (IApplicationInterface::FLOW_NONE === $flow_id) {
                $flow = new Flow([
                    'id' => $flow_id,
                    'name' => 'Virtual',
                    'type' => Flow::TYPE_PUSH,
                    'variant' => Flow::VARIANT_SIMPLE,
                    'simple_settings' => [
                        'poolAssignment' => 'force',
                        'mode' => 'automatically',
                        'deletions' => false,
                        'updateBehavior' => 'ignore',
                        'ignoreUnpublishedChanges' => false,
                        'allowExplicitUnpublishing' => false,
                        'pushMenuItems' => false,
                        'pushPreviews' => false,
                        'mergeLocalChanges' => false,
                        'resolveUserReferences' => 'name',
                        'poolSelectionWidget' => 'checkboxes',
                        'entityTypeSettings' => [
                            $entity_type => [
                                'perBundle' => [
                                    $entity_bundle => [
                                        'mode' => 'default',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ], 'cms_content_sync_flow');
                $flow->getController()->isVirtual(true);
            } else {
                $message = t("The flow @flow_id doesn't exist.", ['@flow_id' => $flow_id])->render();
                \Drupal::logger('cms_content_sync')->notice('@not GET @shared_entity_id: @message', [
                    '@shared_entity_id' => $shared_entity_id,
                    '@not' => 'NO',
                    '@message' => $message,
                ]);

                return $this->respondWith(
                    ['message' => $message],
                    self::CODE_NOT_FOUND
                );
            }
        }

        $infos = EntityStatus::getInfosForEntity($entity_type, $shared_entity_id, ['flow' => $flow_id]);
        foreach ($infos as $info) {
            if ($info->isDeleted()) {
                return $this->respondWith(
                    ['message' => 'This entity has been deleted.'],
                    self::CODE_NOT_FOUND
                );
            }
        }

        $entity = \Drupal::service('entity.repository')->loadEntityByUuid(
            $entity_type,
            $shared_entity_id
        );
        if (!$entity) {
            return $this->respondWith(
                ['message' => 'This entity does not exist.'],
                self::CODE_NOT_FOUND
            );
        }

        $always_v2 = Migration::alwaysUseV2();

        if (!$always_v2) {
            Migration::useV2(true);
        }

        $individual_translation = 'true' === \Drupal::request()->query->get('individualTranslation');
        $language = \Drupal::request()->query->get('language');
        if ($language) {
            $language = preg_replace('@[^a-zA-Z-0-9_]@', '', $language);

            if ($entity instanceof TranslatableInterface && $entity->language()->getId() !== $language) {
                $entity = $entity->getTranslation($language);
            }
        }

        try {
            /**
             * @var PushIntent $intent
             */
            $intent = PushIntent::pushEntity($entity, PushIntent::PUSH_ANY, SyncIntent::ACTION_CREATE, $flow, null, true, $individual_translation ? $language : null);

            if (!$intent) {
                return $this->respondWith(
                    ['message' => 'This entity is not configured to be pushed.'],
                    self::CODE_NOT_FOUND
                );
            }

            /**
             * @var PushSingle $operation
             */
            $operation = $intent->getOperation();
            $body = $operation->getData();

            $intent->afterPush(SyncIntent::ACTION_CREATE, $entity);

            if (!$always_v2) {
                Migration::useV2(false);
            }

            return $this->respondWith(
                json_decode(json_encode($body), true),
                self::CODE_OK
            );
        } catch (SyncException $e) {
            return $this->respondWith(
                $e->serialize(),
                self::CODE_INTERNAL_SERVER_ERROR
            );
        } catch (\Exception $e) {
            return $this->respondWith(
                [
                    'message' => 'Unexpected error: '.$e->getMessage(),
                    'stack' => $e->getTraceAsString(),
                ],
                self::CODE_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function delete($flow_id, $entity_type, $entity_bundle, $shared_entity_id)
    {
        return $this->handleIncomingEntity($flow_id, $entity_type, $entity_bundle, $shared_entity_id, json_decode(file_get_contents('php://input'), true), SyncIntent::ACTION_DELETE);
    }

    public function post($flow_id, $entity_type, $entity_bundle, $shared_entity_id, array $data)
    {
        return $this->handleIncomingEntity($flow_id, $entity_type, $entity_bundle, $shared_entity_id, $data, SyncIntent::ACTION_CREATE);
    }

    protected function respondWith($body, $status, $serialize = false)
    {
        $response = new ModifiedResourceResponse(
            $serialize ? null : $body,
            $status
        );
        if ($serialize) {
            $response->setContent(
                json_encode($body)
            );
        }

        return $response;
    }

    /**
     * Save that the pull for the given entity failed.
     *
     * @param string $pool_id
     *                        The Pool ID
     * @param $entity_type
     *   The Entity Type ID
     * @param $entity_bundle
     *   The bundle name
     * @param $entity_type_version
     *   The requested entity type version
     * @param $entity_uuid
     *   The entity UUID
     * @param $failure_reason
     * @param $action
     * @param $reason
     * @param null $flow_id
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
    protected function saveFailedPull($pool_id, $entity_type, $entity_bundle, $entity_type_version, $entity_uuid, $failure_reason, $action, $reason, $flow_id = null)
    {
        $entity_status = EntityStatus::getInfoForEntity($entity_type, $entity_uuid, $flow_id, $pool_id);

        if (!$entity_status) {
            $entity_status = EntityStatus::create([
                'flow' => $flow_id ? $flow_id : EntityStatus::FLOW_NO_FLOW,
                'pool' => $pool_id,
                'entity_type' => $entity_type,
                'entity_uuid' => $entity_uuid,
                'entity_type_version' => $entity_type_version,
                'flags' => 0,
                'source_url' => null,
            ]);
        }

        $soft_fails = [
            PullIntent::PULL_FAILED_UNKNOWN_POOL,
            PullIntent::PULL_FAILED_NO_FLOW,
            PullIntent::PULL_FAILED_HANDLER_DENIED,
        ];

        $soft = in_array($failure_reason, $soft_fails);

        $entity_status->didPullFail(true, $soft, [
            'error' => $failure_reason,
            'action' => $action,
            'reason' => $reason,
            'bundle' => $entity_bundle,
        ]);

        $entity_status->save();
    }

    private function deleteRemovedEntity($item, Flow $flow, $parent_type, $parent_uuid)
    {
        $menu_pools = [];
        $statuses = EntityStatus::getInfosForEntity($item->getEntityTypeId(), $item->uuid(), [
            'flow' => $flow->id(),
        ]);
        foreach ($statuses as $status) {
            if (!$status->getLastPull()) {
                continue;
            }
            $status_pool = $status->getPool();
            if (!$status_pool) {
                continue;
            }
            // Menu items that were pulled automatically at any point must take
            // precedence over pulls that happened as dependencies. Otherwise
            // if a customer changes configs or has overlapping Flows we delete
            // items we should not delete, so we rather keep them unintentionally
            // than deleting them unintentionally.
            if (!$status->wasPulledEmbedded()) {
                return;
            }
            $menu_pools[] = $status_pool;
        }
        if (empty($menu_pools)) {
            return;
        }
        $menu_operation = new class($item) {
            /**
             * @var Drupal\menu_link_content\Entity\MenuLinkContent
             */
            protected $item;

            public function __construct($item)
            {
                $this->item = $item;
            }

            public function getUuid()
            {
                return $this->item->uuid();
            }

            public function getSourceUrl()
            {
                return '';
            }

            public function getUsedTranslationLanguages()
            {
                return [];
            }

            public function getName()
            {
                return $this->item->label();
            }

            public function getProperty($name)
            {
                try {
                    return $this->item->get($name)->getValue();
                } catch (\Exception $e) {
                    return null;
                }
            }
        };
        $delete_intent = new PullIntent($flow, $menu_pools, PullIntent::PULL_FORCED, SyncIntent::ACTION_DELETE, $item->getEntityTypeId(), $item->bundle(), $menu_operation, $parent_type, $parent_uuid);
        $delete_intent->execute();
    }

    private function handleIncomingEntity($flow_id, $entity_type_name, $entity_bundle, $shared_entity_id, array $data, $action)
    {
        $flow = Flow::getAll()[$flow_id];
        if (empty($flow)) {
            $message = t("The flow @flow_id doesn't exist.", ['@flow_id' => $flow_id])->render();
            \Drupal::logger('cms_content_sync')->notice('@not PULL @action @shared_entity_id: @message', [
                '@action' => $action,
                '@shared_entity_id' => $shared_entity_id,
                '@not' => 'NO',
                '@message' => $message,
            ]);

            return $this->respondWith(
                ['message' => $message],
                self::CODE_NOT_FOUND,
                SyncIntent::ACTION_DELETE == $action
            );
        }

        \Drupal::logger('cms_content_sync')->notice('received @shared_entity_id via @flow_id with @body', ['@shared_entity_id' => $shared_entity_id, '@flow_id' => $flow_id, '@body' => json_encode($data, JSON_PRETTY_PRINT)]);

        $reason = PullIntent::PULL_FORCED;

        if (!$flow->getController()->canPullEntity($entity_type_name, $entity_bundle, $reason, $action)) {
            $message = t("The flow @flow_id isn't configured to pull this entity.", ['@flow_id' => $flow_id])->render();
            \Drupal::logger('cms_content_sync')->notice('@not PULL @action @shared_entity_id: @message', [
                '@action' => $action,
                '@shared_entity_id' => $shared_entity_id,
                '@not' => 'NO',
                '@message' => $message,
            ]);

            return $this->respondWith(
                ['message' => $message],
                self::CODE_NOT_FOUND,
                SyncIntent::ACTION_DELETE == $action
            );
        }

        $core = SyncCoreFactory::getSyncCoreV2();
        $all_pools = Pool::getAll();
        $pools = [];
        $operation = $core
            ->getSyndicationService()
            ->handlePull($flow->id, null, null, $data, SyncIntent::ACTION_DELETE === $action);

        $entity_type_name = $operation->getEntityTypeNamespaceMachineName();
        $entity_bundle = $operation->getEntityTypeMachineName();
        $entity_type_version = $operation->getEntityTypeVersionId();

        if (EntityHandlerPluginManager::mapById($entity_type_name)) {
            $existing = \Drupal::entityTypeManager()
                ->getStorage($entity_type_name)
                ->load($operation->getId());
            if ($existing) {
                $entity_uuid = $existing->uuid();
            } else {
                $entity_uuid = null;
            }
        } else {
            $entity_uuid = $shared_entity_id;
        }

        // Delete doesn't come with pools
        $pool_machine_names = $operation->getPoolIds();
        if (empty($pool_machine_names) && $entity_uuid) {
            $pool_machine_names = [];
            $statuses = EntityStatus::getInfosForEntity($operation->getEntityTypeNamespaceMachineName(), $entity_uuid, [
                'flow' => $flow_id,
            ]);
            // Maybe the entity type is overloaded (multiple Flows for the same type) and the Sync Core uses a
            // different Flow for the delete request because none of the Flows matches.
            if (empty($statuses)) {
                $statuses = EntityStatus::getInfosForEntity($operation->getEntityTypeNamespaceMachineName(), $entity_uuid);
            }
            foreach ($statuses as $status) {
                $status_pool = $status->getPool();
                if ($status_pool) {
                    $pool_machine_names[] = $status_pool->id();
                }
            }
        }

        $allowed_pools = $flow->getController()->getUsedPoolsForPulling($entity_type_name, $entity_bundle);

        foreach ($pool_machine_names as $machine_name) {
            if (!isset($all_pools[$machine_name])) {
                $message = t("The pool @machine_name doesn't exist.", ['@machine_name' => $machine_name])->render();
                \Drupal::logger('cms_content_sync')->notice('@not PULL @action @shared_entity_id: @message', [
                    '@action' => $action,
                    '@shared_entity_id' => $shared_entity_id,
                    '@not' => 'NO',
                    '@message' => $message,
                ]);

                if ($entity_uuid) {
                    $this->saveFailedPull(
                        $machine_name,
                        $entity_type_name,
                        $entity_bundle,
                        $entity_type_version,
                        $entity_uuid,
                        PullIntent::PULL_FAILED_UNKNOWN_POOL,
                        $action,
                        $reason
                    );
                }

                return $this->respondWith(
                    ['message' => $message],
                    self::CODE_NOT_FOUND,
                    SyncIntent::ACTION_DELETE == $action
                );
            }

            if (SyncIntent::ACTION_DELETE !== $action && !in_array($all_pools[$machine_name], $allowed_pools)) {
                continue;
            }

            $pools[] = $all_pools[$machine_name];
        }

        if (empty($pools)) {
            return $this->respondWith(['message' => "No pools were given and the entity doesn't exist on this site with any pool."], self::CODE_NOT_FOUND, SyncIntent::ACTION_DELETE == $action);
        }

        $skip_unchanged = 'true' === \Drupal::request()->query->get('skipUnchanged');

        try {
            $individual_translation = 'true' === \Drupal::request()->query->get('individualTranslation');
            $language = \Drupal::request()->query->get('language');
            if ($language) {
                $language = preg_replace('@[^a-zA-Z-0-9_]@', '', $language);

                if ($language) {
                    $language_obj = \Drupal::languageManager()->getLanguage($language);
                    if (!$language_obj && SyncIntent::ACTION_DELETE !== $action) {
                        $root_translation = 'true' === \Drupal::request()->query->get('isTranslationRoot');
                        // Don't err out so that following languages are still tried when using the "request per translation" feature.
                        if ($individual_translation && !$root_translation) {
                            // Create intent but with default language to create a view URL that can be used as a fallback.
                            $intent = new PullIntent($flow, $pools, $reason, $action, $entity_type_name, $entity_bundle, $operation);
                            $intent->setSkipUnchanged($skip_unchanged);
                            $url = SyncIntent::ACTION_DELETE === $action ? null : $intent->getViewUrl();

                            $response_body = $operation->getResponseBody($url);

                            return $this->respondWith($response_body, self::CODE_OK);
                        }

                        return $this->respondWith(
                            array_merge(
                                [
                                    'message' => 'This entity is not configured to be pulled in the given language: the langue doesn\'t exist.',
                                ],
                            ),
                            self::CODE_NOT_FOUND
                        );
                    }
                }
            }

            $intent = new PullIntent($flow, $pools, $reason, $action, $entity_type_name, $entity_bundle, $operation, null, null, $individual_translation ? $language : null);
            $intent->setSkipUnchanged($skip_unchanged);
            $status = $intent->execute();

            $parent = $intent->getEntity();
            if ($parent) {
                $entity_uuid = $parent->uuid();
            }
            $parent_type = $parent ? $parent->getEntityTypeId() : null;
            $parent_uuid = $parent ? $parent->uuid() : null;

            while ($embed = $operation->getNextUnprocessedEmbed()) {
                // Paragraphs require a parent/container, so them not being processed is fine and we can just skip it completely.
                // This can happen if either a handler denies pulling the parent entity OR if the parent entity is skipped as an optimization.
                if ('paragraph' === $embed->getEntityTypeNamespaceMachineName()) {
                    continue;
                }
                if (!$flow->getController()->canPullEntity($embed->getEntityTypeNamespaceMachineName(), $embed->getEntityTypeMachineName(), $reason, $action)) {
                    continue;
                }

                $embed_pools = [];
                $allowed_pools = $flow->getController()->getUsedPoolsForPulling($embed->getEntityTypeNamespaceMachineName(), $embed->getEntityTypeMachineName());
                foreach ($embed->getPoolIds() as $pool_id) {
                    if (isset($all_pools[$pool_id]) && in_array($all_pools[$pool_id], $allowed_pools)) {
                        $embed_pools[] = $all_pools[$pool_id];
                    }
                }

                if (empty($embed_pools)) {
                    continue;
                }

                $embed_intent = new PullIntent($flow, $embed_pools, $reason, $action, $embed->getEntityTypeNamespaceMachineName(), $embed->getEntityTypeMachineName(), $embed, $parent_type, $parent_uuid, $individual_translation ? $language : null);
                $embed_intent->setSkipUnchanged($skip_unchanged);
                $embed_intent->execute();
            }

            if ($parent) {
                // Delete group<>node references that no longer exist.
                if (\Drupal::moduleHandler()->moduleExists('group') && $parent instanceof ContentEntityInterface) {
                    $group_contents = \Drupal::entityTypeManager()
                        ->getStorage('group_content')
                        ->loadByEntity($parent);
                    foreach ($group_contents as $item) {
                        if (!$operation->isEmbedded($item->getEntityTypeId(), $item->uuid())) {
                            $this->deleteRemovedEntity($item, $flow, $parent_type, $parent_uuid);
                        }
                    }
                }

                // Delete menu items that no longer exist.
                $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
                /**
                 * @var Drupal\menu_link_content\Plugin\Menu\MenuLinkContent[] $menu_items
                 */
                $menu_items = $menu_link_manager->loadLinksByRoute('entity.'.$parent_type.'.canonical', [$parent_type => $parent->id()]);
                foreach ($menu_items as $plugin_item) {
                    /**
                     * @var Drupal\menu_link_content\Entity\MenuLinkContent $item
                     */
                    // We need to get an Entity at this point,
                    // but 'getEntity' is protected for some reason.
                    // So we don't have other choice here but use a reflection.
                    $menu_link_reflection = new \ReflectionMethod('\Drupal\menu_link_content\Plugin\Menu\MenuLinkContent', 'getEntity');
                    $menu_link_reflection->setAccessible(true);
                    $item = $menu_link_reflection->invoke($plugin_item, 'getEntity');

                    if (!$operation->isEmbedded($item->getEntityTypeId(), $item->uuid())) {
                        $this->deleteRemovedEntity($item, $flow, $parent_type, $parent_uuid);
                    }
                }
            }

            if (!Migration::alwaysUseV2()) {
                Migration::entityUsedV2($flow->id, $entity_type_name, $entity_bundle, $entity_uuid, EntityHandlerPluginManager::mapById($entity_type_name) ? $shared_entity_id : null, false);
            }
        }
        // TODO: Log explicitly if this was due to an embedded entity.
        catch (SyncException $e) {
            $message = $e->getSyncExceptionMessage();

            \Drupal::logger('cms_content_sync')->error('@not PULL @action @entity_type:@bundle @uuid @reason: @message'."\n".'@trace'."\n".'@request_body<br>Flow: @flow_id | Pool: @pool_id', [
                '@reason' => $reason,
                '@action' => $action,
                '@entity_type' => $entity_type_name,
                '@bundle' => $entity_bundle,
                '@uuid' => $entity_uuid,
                '@not' => 'NO',
                '@flow_id' => $flow_id,
                '@pool_id' => $pools[0]->id(),
                '@message' => $message,
                '@trace' => ($e->parentException ? $e->parentException->getTraceAsString()."\n\n\n" : '').$e->getTraceAsString(),
                '@request_body' => json_encode($data),
            ]);

            if ($entity_uuid) {
                $this->saveFailedPull(
                    $pools[0]->id(),
                    $entity_type_name,
                    $entity_bundle,
                    $entity_type_version,
                    $entity_uuid,
                    PullIntent::PULL_FAILED_CONTENT_SYNC_ERROR,
                    $action,
                    $reason,
                    $flow->id
                );
            }

            return $this->respondWith(
                $e->serialize(),
                self::CODE_INTERNAL_SERVER_ERROR,
                SyncIntent::ACTION_DELETE == $action
            );
        } catch (\Exception $e) {
            $message = $e->getMessage();

            \Drupal::logger('cms_content_sync')->error('@not PULL @action @entity_type:@bundle @uuid @reason: @message'."\n".'@trace'."\n".'@request_body<br>Flow: @flow_id | Pool: @pool_id', [
                '@reason' => $reason,
                '@action' => $action,
                '@entity_type' => $entity_type_name,
                '@bundle' => $entity_bundle,
                '@uuid' => $entity_uuid,
                '@not' => 'NO',
                '@flow_id' => $flow_id,
                '@pool_id' => $pools[0]->id(),
                '@message' => $message,
                '@trace' => $e->getTraceAsString(),
                '@request_body' => json_encode($data),
            ]);

            if ($entity_uuid) {
                $this->saveFailedPull(
                    $pools[0]->id,
                    $entity_type_name,
                    $entity_bundle,
                    $entity_type_version,
                    $entity_uuid,
                    PullIntent::PULL_FAILED_INTERNAL_ERROR,
                    $action,
                    $reason,
                    $flow->id
                );
            }

            return $this->respondWith(
                [
                    'message' => 'Unexpected error: '.$e->getMessage(),
                    'stack' => $e->getTraceAsString(),
                ],
                self::CODE_INTERNAL_SERVER_ERROR,
                SyncIntent::ACTION_DELETE == $action
            );
        }

        if (!$status && $entity_uuid) {
            $this->saveFailedPull(
                $pools[0]->id,
                $entity_type_name,
                $entity_bundle,
                $entity_type_version,
                $entity_uuid,
                PullIntent::PULL_FAILED_HANDLER_DENIED,
                $action,
                $reason,
                $flow->id
            );
        }

        if ($status) {
            $url = SyncIntent::ACTION_DELETE === $action ? null : $intent->getViewUrl();

            $response_body = $operation->getResponseBody($url);

            // If we send data for DELETE requests, the Drupal Serializer will throw
            // a random error. So we just leave the body empty then.
            return $this->respondWith($response_body, self::CODE_OK, SyncIntent::ACTION_DELETE == $action);
        }

        return $this->respondWith(
            array_merge(
                [
                    'message' => 'This entity is not configured to be pulled.',
                ],
                PullIntent::getNoPullMessage($entity_type_name, $shared_entity_id) ? [
                    'reason' => PullIntent::getNoPullMessage($entity_type_name, $shared_entity_id),
                    'possibleReasons' => [
                        'You have not exported the Flow to the Sync Core after making configuration changes.',
                        'You are using a custom entity handler that chose to ignore this entity.',
                        'You are using a custom handler for the BeforeEntityPull event and chose to ignore this entity.',
                    ],
                ] : [
                    'possibleReasons' => array_merge(
                        // Simple, always possible.
                        [
                            'The entity\'s default language does not exist on this site.',
                            'You have not exported the Flow to the Sync Core after making configuration changes.',
                        ],
                        // Node specific
                        'node' === $entity_type_name ? [
                            'The node is not published yet but you are only allowing published content to be pulled (default setting).',
                        ] : [],
                        // Menu item specific
                        'menu_link_content' === $entity_type_name ? [
                            'The menu item is disabled but you are only allowing enabled menu items to be pulled (default setting).',
                            'You are not allowing all menus and the menu of this item is not allowed.',
                        ] : [],
                        // Advanced usage, not likely for most customers
                        [
                            'You are using a custom entity handler that chose to ignore this entity.',
                            'You are using a custom handler for the BeforeEntityPull event and chose to ignore this entity.',
                        ]
                    ), ],
            ),
            self::CODE_NOT_FOUND,
            SyncIntent::ACTION_DELETE == $action
        );
    }
}
