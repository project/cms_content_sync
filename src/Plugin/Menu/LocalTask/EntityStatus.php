<?php

namespace Drupal\cms_content_sync\Plugin\Menu\LocalTask;

use Drupal\Core\Menu\LocalTaskDefault;
use Symfony\Component\HttpFoundation\Request;

/**
 * Local task plugin to render dynamic tab title dynamically.
 */
class EntityStatus extends LocalTaskDefault
{
    /**
     * {@inheritdoc}
     */
    public function getTitle(Request $request = null)
    {
        $current_user = \Drupal::currentUser();
        if (!$current_user->hasPermission('view cms content sync syndication status')) {
            return t('Content Cloud');
        }

        return _cms_content_sync_get_repository_name();
    }
}
