<?php

namespace Drupal\cms_content_sync;

use Drupal\cms_content_sync\Entity\Pool;
use Drupal\Core\Entity\EntityInterface;

interface IFlowController
{
    /**
     * @return null|string
     */
    public function getType();

    /**
     * Get the config for the given entity type or all entity types.
     *
     * @param null|string $entity_type
     * @param null|string $entity_bundle
     * @param bool $used_only Return only the configs where a handler is set
     * @param bool $include_new_versions Set the entity type version to the current entity type definition version. Otherwise, return the version that was exported last.
     *
     * @return null|array
     */
    public function getEntityTypeConfig($entity_type = null, $entity_bundle = null, $used_only = false, $include_new_versions = false);

    /**
     * Cache the current version per entity type.
     */
    public function updateEntityTypeVersions();

    /**
     * Check whether the current Flow must be exported.
     *
     * @return bool
     */
    public function needsEntityTypeUpdate();

    /**
     * Get the config for the given property of the given entity type + bundle.
     *
     * @return null|array
     */
    public function getPropertyConfig(string $entity_type, string $entity_bundle, string $property);

    /**
     * Return all entity type configs with pull enabled.
     *
     * @param null|string $pull_type
     *
     * @return array
     */
    public function getEntityTypesToPull($pull_type = null);

    /**
     * Ask this Flow whether or not it can push the given entity type and optionally bundle.
     *
     * @param string          $entity_type_name
     * @param null|string     $bundle_name
     * @param string|string[] $reason
     * @param string          $action
     * @param null|Pool       $pool
     *
     * @return bool
     */
    public function canPushEntityType($entity_type_name, $bundle_name, $reason, $action = SyncIntent::ACTION_CREATE, $pool = null);

    /**
     * Ask this Flow whether or not it can push the given entity.
     *
     * @param string|string[] $reason
     * @param string          $action
     * @param null|Pool       $pool
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     *
     * @return bool
     */
    public function canPushEntity(EntityInterface $entity, $reason, $action = SyncIntent::ACTION_CREATE, $pool = null);

    /**
     * Ask this Flow whether or not the given entity should be added as a dependency.
     * This will strictly allow "dependent" mode only.
     *
     * @return bool
     */
    public function canAddEntityAsDependency(EntityInterface $entity);

    /**
     * Get a list of all pools that are used for pushing this entity, either
     * automatically or manually selected.
     *
     * @param string|string[] $reason
     *                                        {@see Flow::PUSH_*}
     * @param string          $action
     *                                        {@see ::ACTION_*}
     * @param bool            $include_forced
     *                                        Include forced pools. Otherwise only use-selected / referenced ones.
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     *
     * @return Pool[]
     */
    public function getPoolsToPushTo(EntityInterface $entity, $reason, $action, $include_forced = true);

    /**
     * Get a list of all pools that are used for pushing this entity, either
     * automatically or manually selected.
     *
     * @param string $entity_type
     * @param string $bundle
     *
     * @return Pool[]
     */
    public function getUsedPoolsForPulling($entity_type, $bundle);

    /**
     * Get a list of all pools this Flow is using.
     *
     * @return Pool[]
     */
    public function getUsedPools();

    /**
     * Check if the given pool is used by this Flow. If any handler set the flow
     * as FORCE or ALLOW, this will return TRUE.
     *
     * @param Pool $pool
     *
     * @return bool
     */
    public function usesPool($pool);

    /**
     * Ask this Flow whether or not it can push the provided entity.
     *
     * @param string $entity_type_name
     * @param string $bundle_name
     * @param string $reason
     * @param string $action
     * @param bool   $strict
     *                                 If asking for DEPENDENCY as a $reason, then $strict will NOT include a Flow that pulls AUTOMATICALLY
     *
     * @return bool
     */
    public function canPullEntity($entity_type_name, $bundle_name, $reason, $action = SyncIntent::ACTION_CREATE, $strict = false);

    /**
     * Ask this synchronization whether it supports the provided entity.
     * Returns false if either the entity type is not known or the config handler
     * is set to {@see Flow::HANDLER_IGNORE}.
     *
     * @return bool
     */
    public function supportsEntity(EntityInterface $entity);

    /**
     * The the entity type handler for the given config.
     *
     * @param $config
     *   {@see Flow::getEntityTypeConfig()}
     *
     * @return \Drupal\cms_content_sync\Plugin\EntityHandlerInterface
     */
    public function getEntityTypeHandler(string $entity_type_name, string $bundle_name, $config);

    /**
     * Get the correct field handler instance for this entity type and field
     * config.
     *
     * @param $entity_type_name
     * @param $bundle_name
     * @param $field_name
     *
     * @return \Drupal\cms_content_sync\Plugin\FieldHandlerInterface
     */
    public function getFieldHandler($entity_type_name, $bundle_name, $field_name);

    /**
     * Get the preview type.
     *
     * @param $entity_type_name
     * @param $bundle_name
     *
     * @return string
     */
    public function getPreviewType($entity_type_name, $bundle_name);

    /**
     * Get language restrictions. NULL or an empty Array [] means no restriction.
     *
     * @return null|array
     */
    public function getAllowedLanguages();

    /**
     * Whether or not entities that were embedded should also syndicate their
     * deletion.
     *
     * @return bool
     */
    public function canPushEmbeddedDeletion();
}
