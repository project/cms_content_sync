<?php

namespace Drupal\cms_content_sync\Form;

use Drupal\cms_content_sync\Controller\PoolExport;
use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\Entity\Pool;
use Drupal\cms_content_sync\SyncCoreFlowExport;
use Drupal\cms_content_sync\SyncCorePoolExport;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Content Sync general settings form.
 */
class JsonForm extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'json_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#attributes']['style'][] = 'display: none;';

        $form['json'] = [
            '#title' => 'JSON',
            '#type' => 'textarea',
        ];

        $form['action'] = [
            '#title' => 'Action',
            '#type' => 'radios',
            '#options' => [
                'save-flow' => 'Save Flow',
            ],
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => 'Submit',
            '#button_type' => 'primary',
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        if ('save-flow' === $form_state->getValue('action')) {
            $data = json_decode($form_state->getValue('json'), true);
            $values = $data['values'];
            $flows = Flow::getAll(false);
            $machine_name = $values['machineName'];

            // Create any new Pools that don't exist yet.
            $pools = Pool::getAll();
            foreach ($data['pools'] as $pool_definition) {
                $pool_machine_name = $pool_definition['machineName'];
                if (!isset($pools[$pool_machine_name])) {
                    $pool = Pool::create([
                        'id' => $pool_machine_name,
                        'label' => $pool_definition['name'],
                    ]);
                    $pool->save();
                    $pools[$pool_machine_name] = $pool;
                }
            }

            // Update existing or create new Flow.
            if (isset($flows[$machine_name])) {
                $flow = $flows[$machine_name];
            } else {
                $flow = Flow::create([
                    'variant' => Flow::VARIANT_SIMPLE,
                    'type' => $values['type'],
                    'id' => $machine_name,
                    'name' => $values['name'],
                ]);
            }

            // The simple flow form doesn't offer a setting for this. So if this
            // was overwritten in the settings.php, we want to restore the
            // previous status instead of saving the overwritten status to the
            // database.
            $previous = \Drupal::config('cms_content_sync.flow.'.$flow->id);
            if ($previous) {
                $previous_status = $previous->getOriginal('status', false);
                if (null !== $previous_status) {
                    $flow->set('status', $previous_status);
                }
            }

            $flow->getController()->setFormValues($values);
            $flow->save();

            Flow::applyOverrides($machine_name, $flow);

            // Export to the Sync Core
            foreach (Pool::getAll() as $pool) {
                if (!PoolExport::validateBaseUrl($pool)) {
                    throw new \Exception('The site does not have a valid base url. The base url must not contain "localhost" and is not allowed to be an IP address. The base url of the site can be configured at the CMS Content Sync settings page.');
                }

                $exporter = new SyncCorePoolExport($pool);
                $sites = $exporter->verifySiteId();

                if ($sites && count($sites)) {
                    throw new \Exception('Another site with id '.array_keys($sites)[0].' and base url '.array_values($sites)[0].' already exists for the pool "'.$pool->id.'"');
                }
            }
            $flow->getController()->updateEntityTypeVersions();

            $active = $flow->get('status');

            if ($active) {
                $exporter = new SyncCoreFlowExport($flow);
                $batch = $exporter->prepareBatch(false);
                $batch->executeAll();
            }

            \Drupal::messenger()->addMessage(\Drupal::translation()->translate('Your Flow %name has been saved and exported to the Sync Core.', ['%name' => $flow->label()]));

            // Conditionally redirect to the migration page.
            if ($data['migrateNext'] && $active) {
                // If this was set, our redirect would be ignored. It's automatically set by Drupal if you hit Edit on the list page.
                \Drupal::request()->query->remove('destination');
                $form_state->setRedirect('cms_content_sync.syndication', [], [
                    'query' => [
                        'flow' => $flow->id,
                        'startNew' => 'true',
                    ],
                ]);
            } else {
                // In this case it's fine if a ?destination query param overwrites our default to go to the Flow list page.
                $form_state->setRedirect('entity.cms_content_sync_flow.collection');
            }
        }
    }
}
