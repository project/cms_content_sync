<?php

namespace Drupal\cms_content_sync\Form;

use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\PushIntent;
use Drupal\cms_content_sync\SyncIntent;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a node deletion confirmation form.
 *
 * @internal
 */
class PushChangesConfirm extends ConfirmFormBase
{
    /**
     * The tempstore factory.
     *
     * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
     */
    protected $tempStoreFactory;

    /**
     * The node storage.
     *
     * @var \Drupal\Core\Entity\EntityStorageInterface
     */
    protected $storage;

    /**
     * The selected nodes to push.
     *
     * @var EntityInterface[]
     */
    protected $entities;

    /**
     * The Flow to use for pushing, if any.
     *
     * @var null|Flow
     */
    protected $flow;

    /**
     * The entities in their default language along with all the translations to be pushed.
     *
     * @var array
     */
    protected $tree;

    /**
     * Constructs a DeleteMultiple form object.
     *
     * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
     *                                                                           The tempstore factory
     * @param \Drupal\Core\Entity\EntityTypeManager          $manager
     *                                                                           The entity manager
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManager $manager)
    {
        $this->tempStoreFactory = $temp_store_factory;
        $this->storage = $manager->getStorage('node');
        $this->entities = $this->tempStoreFactory->get('node_cms_content_sync_push_changes_confirm')->get('entities');
        $flow_id = $this->tempStoreFactory->get('node_cms_content_sync_push_changes_confirm')->get('flow_id');
        $this->flow = $flow_id ? Flow::getAll()[$flow_id] : null;

        $this->tree = [];
        foreach ($this->entities as $entity) {
            foreach ($this->tree as $item) {
                if ($item['root']->id() === $entity->id()) {
                    continue 2;
                }
            }

            $item = [
                'root' => $entity,
                'translations' => [],
            ];

            if ($entity instanceof TranslatableInterface) {
                $languages = $entity->getTranslationLanguages();
                foreach ($languages as $id => $language) {
                    $item['translations'][$id] = $entity->getTranslation($id);
                }
            }

            $this->tree[] = $item;
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager')
    );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'node_cms_content_sync_push_changes_confirm';
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestion()
    {
        return 'Are you sure you want to push this content?';
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelUrl()
    {
        return new Url('system.admin_content');
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmText()
    {
        return t('Push');
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        if (empty($this->entities)) {
            return new RedirectResponse($this->getCancelUrl()->setAbsolute()->toString());
        }

        $items = [];
        foreach ($this->entities as $entity) {
            $id = $entity instanceof TranslatableInterface ? $entity->id().'-'.$entity->language()->getId() : $entity->id();
            $items[$id] = $entity->label();
        }
        $form['entities-selected'] = [
            '#prefix' => '<h2>'.t('You have selected these items to be pushed:').'</h2>',
            '#theme' => 'item_list',
            '#items' => $items,
        ];

        $items = [];
        foreach ($this->tree as $tree_item) {
            $translation_items = [];
            foreach ($tree_item['translations'] as $entity) {
                $translation_items[$entity->id().'-'.$entity->language()->getId()] = $entity->label().' ('.$entity->language()->getName().')';
            }
            $entity = $tree_item['root'];
            $id = $entity instanceof TranslatableInterface ? $entity->id().'-'.$entity->language()->getId() : $entity->id();
            $items[$id] = [
                '#prefix' => $entity->label(),
                '#theme' => 'item_list',
                '#items' => $translation_items,
            ];
        }
        $form['entities-pushed'] = [
            '#prefix' => '<h2>'.t('These are all the items that will be pushed based on your selection:').'</h2>',
            '#theme' => 'item_list',
            '#items' => $items,
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        if ($form_state->getValue('confirm')) {
            $ignored = 0;

            foreach ($this->tree as $item) {
                $entity = $item['root'] instanceof TranslatableInterface ? $item['root']->getUntranslated() : $item['root'];
                if (!PushIntent::pushEntityFromUi(
                    $entity,
                    PushIntent::PUSH_FORCED,
                    SyncIntent::ACTION_UPDATE,
                    $this->flow
                )) {
                    ++$ignored;
                }
            }

            // @todo Improve "ignore" messages (see individual "Push" operation)
            if ($ignored || 1 !== count($this->entities)) {
                \Drupal::messenger()->addMessage(t('Pushed @count content items.', ['@count' => count($this->entities) - $ignored]));
            }
            if ($ignored) {
                \Drupal::messenger()->addWarning(t('@count content items have been ignored as they\'re not configured to be pushed.', ['@count' => $ignored]));
            }
            $this->logger('cms_content_sync')->notice('Pushed @count content, ignored @ignored.', ['@count' => count($this->entities) - $ignored, '@ignored' => $ignored]);
            $this->tempStoreFactory->get('node_cms_content_sync_push_changes_confirm')->delete('entities');
            $this->tempStoreFactory->get('node_cms_content_sync_push_changes_confirm')->delete('flow_id');
        }
        $form_state->setRedirect('system.admin_content');
    }
}
