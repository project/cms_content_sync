<?php

namespace Drupal\cms_content_sync\Form;

use Drupal\cms_content_sync\Controller\ContentSyncSettings;
use Drupal\cms_content_sync\SyncCoreInterface\SyncCoreFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use EdgeBox\SyncCore\Interfaces\ISyncCore;
use EdgeBox\SyncCore\V1\Entity\Entity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Content Sync advanced debug form.
 */
class DebugForm extends ConfigFormBase
{
    /**
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
     */
    protected $bundleInfoService;

    /**
     * @var \Drupal\Core\Entity\EntityFieldManager
     */
    protected $entityFieldManager;

    /**
     * Constructs an object.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface        $config_factory
     *                                                                                The factory for configuration objects
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface    $entity_type_manager
     *                                                                                The entity query
     * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
     *                                                                                The bundle info service
     * @param \Drupal\Core\Entity\EntityFieldManager            $entity_field_manager
     *                                                                                The entity field manager
     */
    public function __construct(
        ConfigFactoryInterface $config_factory,
        EntityTypeManagerInterface $entity_type_manager,
        EntityTypeBundleInfoInterface $bundle_info_service,
        EntityFieldManager $entity_field_manager
    ) {
        parent::__construct($config_factory);

        $this->entityTypeManager = $entity_type_manager;
        $this->bundleInfoService = $bundle_info_service;
        $this->entityFieldManager = $entity_field_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'cms_content_sync_debug_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#tree'] = true;
        $settings = ContentSyncSettings::getInstance();

        $can_refresh_login = false;
        foreach (SyncCoreFactory::getAllSyncCores() as $core) {
            if ($core->featureEnabled(ISyncCore::FEATURE_REFRESH_AUTHENTICATION)) {
                $can_refresh_login = true;

                break;
            }
        }
        if ($can_refresh_login) {
            $form['cms_content_sync_login'] = [
                '#type' => 'submit',
                '#value' => $this->t('Login at Sync Core'),
            ];
        }

        // The extended entity export/import logging is stored within the key value table
        // since we do not want this stored within the Drupal configuration.
        $form['cms_content_sync_extended_entity_export_logging'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Extended Entity Export logging'),
            '#default_value' => $settings->getExtendedEntityExportLogging() ?? 0,
            '#description' => $this->t('When the "Extended Entity Export logging" is enabled, Content Sync is going to add a log entry to Watchdog
                                        showing all entity values processed by content sync after the <b><u>export</u></b>. This is helpful to debug outgoing entities.<br>
                                        <b>This will create many large log messages, so only use this for a short period of time and disable it immediately after your debugging session.</b>'),
        ];

        $form['cms_content_sync_extended_entity_import_logging'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Extended Entity Import logging'),
            '#default_value' => $settings->getExtendedEntityImportLogging() ?? 0,
            '#description' => $this->t('When the "Extended Entity Import logging" is enabled, Content Sync is going to add a log entry to Watchdog
                                        showing all entity values processed by content sync after the <b><u>import</u></b>. This is helpful to identify if an entity
                                        has changed after content sync has processed it.<br>
                                        <b>This will create many large log messages, so only use this for a short period of time and disable it immediately after your debugging session.</b>'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $settings = ContentSyncSettings::getInstance();
        $settings->setExtendedEntityExportLogging($form_state->getValue('cms_content_sync_extended_entity_export_logging'));
        $settings->setExtendedEntityImportLogging($form_state->getValue('cms_content_sync_extended_entity_import_logging'));

        if ($form_state->getValue('op') == t('Login at Sync Core')) {
            $messenger = \Drupal::messenger();

            foreach (SyncCoreFactory::getAllSyncCores() as $host => $core) {
                if (!$core->featureEnabled(ISyncCore::FEATURE_REFRESH_AUTHENTICATION)) {
                    continue;
                }
                if ($core->getSyndicationService()->refreshAuthentication()) {
                    $messenger->addStatus('SUCCESS login from Sync Core at '.$host);
                } else {
                    $messenger->addError('FAILED to login from Sync Core at '.$host);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'cms_content_sync.debug',
        ];
    }
}
