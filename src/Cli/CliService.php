<?php

namespace Drupal\cms_content_sync\Cli;

use Drupal\cms_content_sync\Controller\AdvancedToSimpleFlow;
use Drupal\cms_content_sync\Controller\ContentSyncSettings;
use Drupal\cms_content_sync\Controller\FlowPull;
use Drupal\cms_content_sync\Controller\Migration;
use Drupal\cms_content_sync\Controller\PoolExport;
use Drupal\cms_content_sync\Entity\EntityStatus;
use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\Entity\Pool;
use Drupal\cms_content_sync\PullIntent;
use Drupal\cms_content_sync\PushIntent;
use Drupal\cms_content_sync\SyncCoreFlowExport;
use Drupal\cms_content_sync\SyncCoreInterface\DrupalApplication;
use Drupal\cms_content_sync\SyncCoreInterface\SyncCoreFactory;
use Drupal\cms_content_sync\SyncCorePoolExport;
use Drupal\cms_content_sync\SyncIntent;
use Drupal\Component\Uuid\Uuid;
use Drush\Exceptions\UserAbortException;
use EdgeBox\SyncCore\Exception\TimeoutException;
use EdgeBox\SyncCore\Exception\UnauthorizedException;
use Firebase\JWT\JWT;

class CliService
{
    /**
     * Export the configuration to the Sync Core.
     *
     * @param ICLIIO $io
     *                        The CLI service which allows interoperability
     * @param array  $options
     *                        An array containing the option parameters provided by Drush
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \EdgeBox\SyncCore\Exception\SyncCoreException
     * @throws \Exception
     */
    public function configuration_export($io, $options)
    {
        // Check if the site has already been registered.
        $settings = ContentSyncSettings::getInstance();
        if (Migration::alwaysUseV2() && !$settings->getSiteUuid()) {
            $io->error('The site needs to be registered first, before the configuration can be exported to the sync core.');

            return;
        }

        _cms_content_sync_report_domains();

        if (!empty($options['v2']) && 'true' === $options['v2']) {
            $io->text('Starting Pool export to v2...');
            $pool_ids = [];
            foreach (Pool::getAll() as $pool) {
                $pool_ids[] = $pool->id();
            }
            Migration::runPoolExport($pool_ids);
            $io->text('Finished Pool export.');

            $io->text('Starting Flow export to v2...');
            $flow_ids = [];
            foreach (Flow::getAll() as $flow) {
                $flow_ids[] = $flow->id();
            }
            Migration::runFlowExport($flow_ids);
            $io->text('Finished Flow export.');

            return;
        }

        $io->text('Validating Pools...');
        foreach (Pool::getAll() as $pool) {
            if (!PoolExport::validateBaseUrl($pool)) {
                throw new \Exception('The site does not have a valid base url. The base url must not contain "localhost" and is not allowed to be an IP address. The base url of the site can be configured at the CMS Content Sync settings page.');
            }

            $exporter = new SyncCorePoolExport($pool);
            $sites = $exporter->verifySiteId();

            if (!$options['force'] && $sites && count($sites)) {
                throw new \Exception('Another site with id '.array_keys($sites)[0].' and base url '.array_values($sites)[0].' already exists for the pool "'.$pool->id.'"');
            }
        }
        $io->text('Finished validating Pools.');

        $io->text('Starting Flow export...');
        $count = 0;
        foreach (Flow::getAll() as $flow) {
            $io->text('> Updating Flow '.$flow->label().'...');
            $flow->getController()->updateEntityTypeVersions();
            $io->text('> Exporting Flow '.$flow->label().'...');
            $exporter = new SyncCoreFlowExport($flow);
            $batch = $exporter->prepareBatch($options['force']);
            $io->text('>> Executing '.$batch->count().' operations...');
            $batch->executeAll();
            ++$count;
        }
        $io->text('Finished export of '.$count.' Flow(s).');

        $io->text('Deleting old configuration...');
        SyncCoreFlowExport::deleteUnusedFlows();

        $io->success('Export completed.');
    }

    /**
     * Kindly ask the Sync Core to login again.
     *
     * @param ICLIIO $io
     *                   The CLI service which allows interoperability
     *
     * @throws \EdgeBox\SyncCore\Exception\SyncCoreException
     */
    public function sync_core_login($io)
    {
        $io->text('Asking all connected Sync Cores to refresh the login to this site...');
        $io->text('Please note that this only works for old v1 Sync Cores.');

        $cores = SyncCoreFactory::getAllSyncCores();
        foreach ($cores as $host => $core) {
            if ($core->getSyndicationService()->refreshAuthentication()) {
                $io->text('SUCCESS login from Sync Core at '.$host);
            } else {
                $io->error('FAILED to login from Sync Core at '.$host);
            }
        }

        $io->success('Done.');
    }

    /**
     * Kindly ask the Sync Core to pull all entities for a specific flow, or to
     * force pull one specific entity.
     *
     * @param ICLIIO $io
     *                        The CLI service which allows interoperability
     * @param string $flow_id
     *                        The flow the entities should be pulled from
     * @param array  $options
     *                        An array containing the option parameters provided by Drush
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \EdgeBox\SyncCore\Exception\SyncCoreException
     */
    public function pull($io, $flow_id, $options)
    {
        $force = $options['force'];

        $entity_type = $options['entity_type'];
        $entity_uuid = $options['entity_uuid'];

        if ('' != $entity_uuid && null == $entity_type) {
            $io->error('If a specific entity_uuid should be pulled, the entity_type also has to be set.');

            return;
        }

        // @todo Allow pulling of all entities of a specific type for one specific flow.
        if ('' != $entity_type && null == $entity_uuid) {
            $io->error('If the entity_type option is set, the entity_uuid to be pulled also has to be specified.');

            return;
        }

        if (!is_null($entity_uuid) && !is_null($entity_uuid)) {
            if (UUID::isValid($entity_uuid)) {
                // Pull a single entity.
                // @todo Allow pull for single entities which have not been pulled before.
                FlowPull::force_pull_entity($flow_id, $entity_type, $entity_uuid);
            } else {
                $io->error('The specified entity_uuid is invalid.');
            }
        } else {
            // Pull all entities for the specified flow.
            $flows = Flow::getAll();

            foreach ($flows as $id => $flow) {
                if ($flow_id && $id != $flow_id) {
                    continue;
                }

                $result = FlowPull::pullAll($flow, $force);

                if (empty($result)) {
                    $io->text('No automated pull configured for Flow: '.$flow->label());

                    continue;
                }

                $io->text('Started pulling for Flow: '.$flow->label());

                foreach ($result as $operation) {
                    $operation->execute();

                    if (!($goal = $operation->total())) {
                        $io->text('> Nothing to do for: '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName());

                        continue;
                    }

                    $progress = 0;

                    while ($progress < $goal) {
                        if ($progress > 0) {
                            sleep(5);
                        }

                        try {
                            $progress = $operation->progress();
                        } catch (TimeoutException $e) {
                            $io->text('> Timeout when asking the Sync Core to report on the progress of pulling '.$goal.' '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName().'. Will try again in 15 seconds...');
                            sleep(15);

                            continue;
                        }

                        if ($progress == $goal) {
                            $io->text('> Finished '.$goal.' operations for '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName());
                        } elseif (0 == $progress) {
                            sleep(5);
                        } else {
                            $io->text('> Finished '.$progress.' of '.$goal.' operations for '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName().': '.floor($progress / $goal * 100).'%');
                        }
                    }
                }
            }
        }
    }

    /**
     * Kindly ask the Sync Core to pull all entities for a specific flow.
     *
     * @param ICLIIO $io
     *                        The CLI service which allows interoperability
     * @param string $flow_id
     *                        The flow the entities should be pulled from
     * @param array  $options
     *                        An array containing the option parameters provided by Drush
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \EdgeBox\SyncCore\Exception\SyncCoreException
     *
     * @deprecated Function is deprecated and is going to be removed in 2.0, use
     *   pull() instead.
     */
    public function pull_entities($io, $flow_id, $options)
    {
        $io->warning('Function is deprecated and is going to be removed in 2.0, use "cs-pull" instead.');

        $force = $options['force'];

        $flows = Flow::getAll();

        foreach ($flows as $id => $flow) {
            if ($flow_id && $id != $flow_id) {
                continue;
            }

            $result = FlowPull::pullAll($flow, $force);

            if (empty($result)) {
                $io->text('No automated pull configured for Flow: '.$flow->label());

                continue;
            }

            $io->text('Started pulling for Flow: '.$flow->label());

            foreach ($result as $operation) {
                $operation->execute();

                if (!($goal = $operation->total())) {
                    $io->text('> Nothing to do for: '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName());

                    continue;
                }

                $progress = 0;

                while ($progress < $goal) {
                    if ($progress > 0) {
                        sleep(5);
                    }

                    try {
                        $progress = $operation->progress();
                    } catch (TimeoutException $e) {
                        $io->text('> Timeout when asking the Sync Core to report on the progress of pulling '.$goal.' '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName().'. Will try again in 15 seconds...');
                        sleep(15);

                        continue;
                    }

                    if ($progress == $goal) {
                        $io->text('> Finished '.$goal.' operations for '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName());
                    } elseif (0 == $progress) {
                        sleep(5);
                    } else {
                        $io->text('> Finished '.$progress.' / '.$goal.' operations for  '.$operation->getTypeMachineName().'.'.$operation->getBundleMachineName().' from '.$operation->getSourceName().': '.floor($progress / $goal * 100).'%');
                    }
                }
            }
        }
    }

    /**
     * Kindly ask the Sync Core to force pull a specific entity.
     *
     * @param ICLIIO $io
     *                            The CLI service which allows interoperability
     * @param string $flow_id
     *                            The flow the entities should be pulled from
     * @param string $entity_type
     *                            The type of the entity that should be pulled
     * @param string $entity_uuid
     *                            The uuid of the entity that should be pulled
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     *
     * @deprecated Function is deprecated and is going to be removed in 2.0, use
     *   pull() instead.
     */
    public function force_pull_entity($io, $flow_id, $entity_type, $entity_uuid)
    {
        $io->warning('Function is deprecated and is going to be removed in 2.0, use "cs-pull" instead.');

        FlowPull::force_pull_entity($flow_id, $entity_type, $entity_uuid);
    }

    /**
     * Push all entities for a specific flow.
     *
     * @param ICLIIO $io
     *                        The CLI service which allows interoperability
     * @param string $flow_id
     *                        The flow the entities should be pulled from
     * @param array  $options
     *                        An array containing the option parameters provided by Drush
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function push($io, $flow_id, $options)
    {
        $push_mode = $options['push_mode'];
        $flows = Flow::getAll();

        if (!is_null($push_mode)) {
            if ('automatic_manual' != $push_mode && 'automatic_manual_force' != $push_mode) {
                $io->error('Invalid value detected for push_mode. Allowed values are: automatic_manual and automatic_manual_force.');

                return;
            }
        }

        foreach ($flows as $id => $flow) {
            if ($flow_id && $id != $flow_id) {
                continue;
            }

            /**
             * @var \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
             */
            $entity_type_manager = \Drupal::service('entity_type.manager');

            foreach ($flow->getController()->getEntityTypeConfig(null, null, true) as $entity_type_name => $bundles) {
                foreach ($bundles as $bundle_name => $config) {
                    if ('automatic_manual' == $push_mode || 'automatic_manual_force' == $push_mode) {
                        if (PushIntent::PUSH_AUTOMATICALLY != $config['export'] && PushIntent::PUSH_MANUALLY != $config['export']) {
                            continue;
                        }
                    } else {
                        if (PushIntent::PUSH_AUTOMATICALLY != $config['export']) {
                            continue;
                        }
                    }

                    $storage = $entity_type_manager->getStorage($entity_type_name);

                    $query = $storage
                        ->getQuery();

                    // Files don't have bundles, so this would lead to a fatal error then.
                    if ($storage->getEntityType()->getKey('bundle')) {
                        $query = $query->condition($storage->getEntityType()
                            ->getKey('bundle'), $bundle_name);
                    }

                    $ids = $query->execute();
                    $total = count($ids);

                    if (!$total) {
                        $io->text('Skipping '.$entity_type_name.'.'.$bundle_name.' as no entities match.');

                        continue;
                    }

                    $success = 0;
                    $io->text('Starting to push '.$total.' '.$entity_type_name.'.'.$bundle_name.' entities.');

                    foreach ($ids as $id) {
                        $entity = $storage->load($id);

                        /**
                         * @var \Drupal\cms_content_sync\Entity\EntityStatus[] $entity_status
                         */
                        $entity_status = EntityStatus::getInfosForEntity($entity->getEntityTypeId(), $entity->uuid(), ['flow' => $flow->id()]);

                        if ('automatic_manual' == $push_mode && (empty($entity_status) || is_null($entity_status[0]->getLastPush()))) {
                            continue;
                        }

                        /**
                         * @var \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
                         */
                        $entity_type_manager = \Drupal::service('entity_type.manager');

                        $entity = $entity_type_manager
                            ->getStorage($entity_type_name)
                            ->load($id);

                        try {
                            PushIntent::pushEntity($entity, PushIntent::PUSH_FORCED, SyncIntent::ACTION_CREATE, $flow);
                            ++$success;
                        } catch (\Exception $exception) {
                            \Drupal::logger('cms_content_sync')
                                ->notice(
                                    'Entity could not be pushed, reason: %exception<br>Flow: @flow_id',
                                    [
                                        '%exception' => $exception->getMessage(),
                                        '@flow_id' => $flow_id,
                                    ]
                                );
                        }
                    }

                    $io->text('Successfully pushed '.$success.' entities.');

                    if ($total != $success) {
                        $io->text('Failed to push '.($total - $success).' entities.');
                    }
                }
            }
        }
    }

    /**
     * Reset the status entities for a specific or all pool/s.
     *
     * @param ICLIIO $io
     *                        The CLI service which allows interoperability
     * @param array  $options
     *                        An array containing the option parameters provided by Drush
     *
     * @throws \Drush\Exceptions\UserAbortException
     */
    public function reset_status_entities($io, $options = ['pool_id' => null])
    {
        $pool_id = empty($options['pool_id']) ? null : $options['pool_id'];

        if (empty($pool_id)) {
            $io->warning(dt('Are you sure you want to reset the status entities for all pools?'));
        } else {
            $io->warning(dt('Are you sure you want to reset the status entities for the pool: '.$pool_id.'?'));
        }
        $io->warning(dt('By resetting the status of all entities, the date of the last pull and the date of the last push date will be reset. The dates will no longer be displayed until the content is pulled or pushed again and all entities will be pushed / pulled again at the next synchronization regardless of whether they have changed or not.'));

        if (!$io->confirm(dt('Do you want to continue?'))) {
            throw new UserAbortException();
        }

        empty($pool_id) ? Pool::resetStatusEntities() : Pool::resetStatusEntities($pool_id);
        $io->success('Status entities have been reset and entity caches are invalidated.');
    }

    /**
     * Check the flags for an entity.
     *
     * @param ICLIIO $io
     *                            The CLI service which allows interoperability
     * @param string $entity_uuid
     *                            The uuid of the entity the flags should be checked for
     * @param array  $options
     *                            An array containing the option parameters provided by Drush
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function check_entity_flags($io, $entity_uuid, $options = ['flag' => null])
    {
        $flag = empty($options['flag']) ? null : $options['flag'];

        /**
         * @var \Drupal\cms_content_sync\Entity\EntityStatus[] $entity_status
         */
        $entity_status = \Drupal::entityTypeManager()
            ->getStorage('cms_content_sync_entity_status')
            ->loadByProperties(['entity_uuid' => $entity_uuid]);
        if (empty($entity_status)) {
            $io->text(dt('There is no status entity existent yet for this UUID.'));
        } else {
            foreach ($entity_status as $status) {
                $result = '';
                $io->text(dt('Flow: '.$status->get('flow')->value));

                if (empty($flag)) {
                    $result .= 'FLAG_IS_SOURCE_ENTITY: '.($status->isSourceEntity() ? 'TRUE' : 'FALSE').PHP_EOL;
                    $result .= 'FLAG_PUSH_ENABLED: '.($status->isPushEnabled() ? 'TRUE' : 'FALSE').PHP_EOL;
                    $result .= 'FLAG_PUSHED_AS_DEPENDENCY: '.($status->isPushedAsDependency() ? 'TRUE' : 'FALSE').PHP_EOL;
                    $result .= 'FLAG_EDIT_OVERRIDE: '.($status->isOverriddenLocally() ? 'TRUE' : 'FALSE').PHP_EOL;
                    $result .= 'FLAG_USER_ENABLED_PUSH: '.($status->didUserEnablePush() ? 'TRUE' : 'FALSE').PHP_EOL;
                    $result .= 'FLAG_DELETED: '.($status->isDeleted() ? 'TRUE' : 'FALSE').PHP_EOL;
                } else {
                    switch ($flag) {
            case 'FLAG_IS_SOURCE_ENTITY':
              $status->isSourceEntity() ? $result .= 'TRUE' : $result .= 'FALSE';

              break;

            case 'FLAG_PUSH_ENABLED':
              $status->isPushEnabled() ? $result .= 'TRUE' : $result .= 'FALSE';

              break;

            case 'FLAG_PUSHED_AS_DEPENDENCY':
              $status->isPushedAsDependency() ? $result .= 'TRUE' : $result .= 'FALSE';

              break;

            case 'FLAG_EDIT_OVERRIDE':
              $status->isOverriddenLocally() ? $result .= 'TRUE' : $result .= 'FALSE';

              break;

            case 'FLAG_USER_ENABLED_PUSH':
              $status->didUserEnablePush() ? $result .= 'TRUE' : $result .= 'FALSE';

              break;

            case 'FLAG_DELETED':
              $status->isDeleted() ? $result .= 'TRUE' : $result .= 'FALSE';

              break;
          }
                }
                $io->text(dt($result));
            }
        }
    }

    /**
     * Register this site to the Content Sync backend. Please visit https://app.cms-content-sync.io/sites/register-multiple to get the required IDs and token.
     *
     * @param ICLIIO $io the CLI service which allows interoperability
     * @param string $environment_type either production, staging, testing or local
     * @param string $contract The UUID of the contract. Please visit https://app.cms-content-sync.io/sites/register-multiple to get the required IDs and token.
     * @param string $project The UUID of the project. Please visit https://app.cms-content-sync.io/sites/register-multiple to get the required IDs and token.
     * @param string $token A JWT token to verify your request. Please visit https://app.cms-content-sync.io/sites/register-multiple to get the required IDs and token.
     * @param array $options provide require_fixed_ip as "true" to use our proxy with a static outbound IP
     */
    public function register($io, string $environment_type, string $contract, string $project, string $token, array $options)
    {
        $require_fixed_ip = empty($options['require_fixed_ip']) ? null : 'true' === $options['require_fixed_ip'];

        $settings = ContentSyncSettings::getInstance();

        // Already registered if this exists.
        $uuid = $settings->getSiteUuid();

        $params = [
            'environmentType' => $environment_type,
            'useProxy' => $require_fixed_ip,
            'contractUuid' => $contract,
            'projectUuid' => $project,
            'uuid' => $uuid,
        ];

        $tks = explode('.', $token);
        list(, $bodyb64) = $tks;
        $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64));
        if (!empty($payload->syncCoreBaseUrl)) {
            DrupalApplication::get()->setSyncCoreUrl($payload->syncCoreBaseUrl);
        }

        $core = SyncCoreFactory::getSyncCoreV2();

        try {
            $core->registerNewSiteWithToken($params, $token);

            // Clear feature flag cache.
            SyncCoreFactory::clearCache();

            _cms_content_sync_report_domains();

            $io->text(dt('Site was registered successfully.'));
        }
        // The site registration JWT expires after only 5 minutes, then the user must get a new one.
        catch (UnauthorizedException $e) {
            $io->error(dt('Your registration token expired. Please try again.'));
        }
    }

    /**
     * Migrate your Flows to the new format.
     *
     * @param ICLIIO $io
     *                                     The CLI service which allows interoperability
     * @param string $run                  pass "1" to run the migration
     * @param string $flow_machine_name    the Flow machine name to migrate
     *
     * @throws \Exception
     */
    public function migrateFlow($io, ?string $run = null, ?string $flow_machine_name = null)
    {
        $possible = true;
        foreach (Flow::getAll(false) as $flow) {
            if ($flow_machine_name) {
                if ($flow->id() !== $flow_machine_name) {
                    continue;
                }
            }

            if (!AdvancedToSimpleFlow::plainContent($flow, $run)) {
                $possible = false;
            }
        }

        if (!$possible) {
            // Won't display messages if we throw an exception.
            //throw new \Exception("Migration not possible.");
        }
    }

    /**
     * Start a migration from v1 to v2 or view the status of a running migration.
     *
     * @param ICLIIO $io
     *                        The CLI service which allows interoperability
     * @param array  $options
     *                        An array containing the option parameters provided by Drush
     *
     * @throws \Drush\Exceptions\UserAbortException
     */
    public function migrate($io, string $action, ?string $direction, ?string $flow_id, ?string $entity_type_name, ?string $bundle_name)
    {
        if (Migration::useV2()) {
            $io->error(dt('This site has already been migrated to v2.'));

            return;
        }

        if ('switch' === $action) {
            Migration::runSwitch(true);
            $io->success(dt('Switched site to use v2 from now on.'));

            return;
        }
        if ('help' === $action) {
            $io->text(dt(<<<'EOL'
Use this command to migrate from Content Sync v1 to Content Sync v2.
The migration is divided into several steps that must be executed in the right order.
Each migration step must be executed *at all sites* before continuing with the next step.
While the migration is still ongoing, content can still be synchronized with your existing v1 Sync Core.

*** IT'S IMPORTANT THAT YOU DON'T MIGRATE ONE SITE AFTER ANOTHER FROM START TO FINISH. ***
*** INSTEAD, FINISH EACH STEP ON ALL SITES BEFORE MOVING ON TO THE NEXT STEP. ***

The individual steps are:
1. Register sites at the new Sync Core
   > (see https://app.cms-content-sync.io/sites/register-multiple)
2. Export existing configuration to the new Sync Core
   > drush cse --v2=true
3. Test push content
   Open Content > Sync Health > Entity Status in the Drupal UI at a pushing site.
   Select a node you want to test with and use the "Test push to v2" operation.
   Check the synchronization progress in the UI (you will get a link to it in the success message).
   After it's done, verify that the content has been pulled by the remote site(s) and that everything is in order.
3. Start pushing existing content from all sites
   > drush csmig start push
4. Watch push progress
   > drush csmig status push
   *** WAIT FOR ALL OPERATIONS TO HAVE FINISHED BEFORE CONTINUING. ***
   If there are any issues, please use the Migration UI at the Site tab in the Content Sync settings
   for troubleshooting. To watch the status across all sites use status-all instead:
   > drush csmig status-all push
5. Start mapping existing content that has been pushed by other sites to the v2 Sync Core with the command above.
   > drush csmig start map
6. Watch mapping progress
   > drush csmig status map
   *** WAIT FOR ALL OPERATIONS TO HAVE FINISHED BEFORE CONTINUING. ***
   If there are any issues, please use the Migration UI at the Site tab in the Content Sync settings
   for troubleshooting. To watch the status across all sites use status-all instead:
   > drush csmig status-all map
7. Switch to make v2 the default Sync Core
   > drush csmig switch
EOL));

            return;
        }

        if (!$direction) {
            $io->error(dt('Direction is required.'));

            return;
        }
        if ('push' !== $direction && 'map' !== $direction) {
            $io->error(dt('Direction must be "push" or "map".'));

            return;
        }
        $is_push = 'push' === $direction;

        $all_flows = Flow::getAll();
        $flow = $flow_id ? $all_flows[$flow_id] : null;
        if ($flow_id && !$flow) {
            $io->error(dt('Unknown Flow ID.'));

            return;
        }

        if ($flow && $entity_type_name && $bundle_name) {
            if ($is_push ? !$flow->getController()->canPushEntityType($entity_type_name, $bundle_name, PushIntent::PUSH_ANY) : !$flow->getCotroller()->canPullEntity($entity_type_name, $bundle_name, PullIntent::PULL_FORCED)) {
                $io->error(dt("The given Flow can't handle this entity type."));

                return;
            }
        }

        Migration::useV2(true);

        $syndication = SyncCoreFactory::getSyncCoreV2()->getSyndicationService();

        $print_status = function ($current, $total, $statuses) use ($io) {
            $percent = floor($current / $total * 100);
            $io->text(dt('Progress is at @progress% (@current / @total):', ['@progress' => $percent, '@current' => $current, '@total' => $total]));

            foreach ($statuses as $status => $status_count) {
                $percentage = floor($status_count / $total * 100);
                $io->text(dt('- @status: @percentage% (@count):', ['@status' => $status, '@percentage' => $percentage, '@count' => $status_count]));
            }
        };
        if ('status-all' === $action) {
            $service = $is_push ? $syndication->massPush()->isInitialMigration(true) : $syndication->massPull()->isInitialMigration(true);
            $service->includeOtherSites(true);

            $print_status($service->progress(), $service->total(), $service->getByStatus());

            return;
        }

        /**
         * @var EdgeBox\SyncCore\Interfaces\Syndication\IMassPull[]|EdgeBox\SyncCore\Interfaces\Syndication\IMassPush[] $services
         */
        $services = [];
        $flows = $flow ? [$flow_id => $flow] : $all_flows;
        foreach ($flows as $flow_machine_name => $flow) {
            foreach ($flow->getController()->getEntityTypeConfig() as $type => $type_config) {
                if ($entity_type_name && $entity_type_name !== $type) {
                    continue;
                }
                foreach ($type_config as $bundle => $bundle_config) {
                    if ($bundle_name && $bundle_name !== $bundle) {
                        continue;
                    }
                    if ($is_push) {
                        if (PushIntent::PUSH_AUTOMATICALLY === $bundle_config['export'] || PushIntent::PUSH_MANUALLY === $bundle_config['export']) {
                            $services[] = $syndication->massPush()
                                ->withFlow($flow_machine_name)
                                ->withNamespaceMachineName($type)
                                ->withEntityTypeMachineName($bundle)
                                ->withEntityTypeVersion($bundle_config['version'])
                                ->isInitialMigration(true);
                        }
                    } else {
                        if (PullIntent::PULL_AUTOMATICALLY === $bundle_config['import'] || PullIntent::PULL_MANUALLY === $bundle_config['import']) {
                            $services[] = $syndication->massPull()
                                ->withFlow($flow_machine_name)
                                ->withNamespaceMachineName($type)
                                ->withEntityTypeMachineName($bundle)
                                ->withEntityTypeVersion($bundle_config['version'])
                                ->isInitialMigration(true);
                        }
                    }
                }
            }
        }
        if (!count($services)) {
            $io->error(dt('There are no matching Flows to migrate at this site.'));

            return;
        }

        if ('status' === $action) {
            /**
             * @var int[] $statuses_all
             */
            $statuses_all = [];
            $total = 0;
            $current = 0;
            foreach ($services as $service) {
                $statuses = $service->getByStatus();
                $total += $service->total();
                $current += $service->progress();
                foreach ($statuses as $status => $status_count) {
                    $statuses_all[$status] = (isset($statuses_all[$status]) ? $statuses_all[$status] : 0) + $status_count;
                }
            }
            if (!$total) {
                $io->error(dt("The migration hasn't been started yet."));

                return;
            }

            $print_status($current, $total, $statuses_all);
        } elseif ('start' === $action) {
            $count = 0;
            $count_failed = 0;
            foreach ($services as $service) {
                try {
                    $service->execute();
                    $io->text(dt('> @flow.@type.@bundle started', [
                        '@flow' => $service->getFlow(),
                        '@type' => $service->getNamespaceMachineName(),
                        '@bundle' => $service->getEntityTypeMachineName(),
                    ]));
                    ++$count;
                } catch (\Exception $e) {
                    $io->error(dt('> @flow.@type.@bundle failed to start: @error', [
                        '@flow' => $service->getFlow(),
                        '@type' => $service->getNamespaceMachineName(),
                        '@bundle' => $service->getEntityTypeMachineName(),
                        '@error' => $e->getMessage(),
                    ]));
                    ++$count_failed;
                }
            }
            $io->text('');
            $io->success(dt('Started @count migrations. Run "drush csmig status '.$direction.'" to see the status.', [
                '@count' => $count,
            ]));
            if ($count_failed) {
                $io->text('');
                $io->error(dt('*** FAILED TO START @count MIGRATIONS ***', [
                    '@count' => $count_failed,
                ]));
                $io->text('');
            }
        } else {
            $io->error(dt('Action must be "start", "status" or "switch". Use "help" to get detailed migration instructions.'));

            return;
        }
    }
}
