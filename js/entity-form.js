(function ($) {

  'use strict';

  function showFieldGroups(checkbox, show) {
    var fieldgroups = checkbox.parent().siblings().filter(function () {
      return /field-group/.test($(this).attr("class"));
    });
    if (show) {
      fieldgroups.show();
    } else {
      fieldgroups.hide();
    }
  }

  var overridden = {};

  Drupal.behaviors.entityForm = {
    attach: function (context, settings) {
      $('.cms-content-sync-edit-override', context).each(function () {
        var checkbox = $(this);

        if (checkbox.data("cms-content-sync--processed-override")) {
          return;
        }
        checkbox.data("cms-content-sync--processed-override", true);

        var name = checkbox.attr("name");

        // We have to prevent users from checking the box, making changes
        // and then unchecking it. Otherwise paragraph revisions will be messed
        // up.
        // So we hide the checkbox and instead show buttons to make changes
        // or to reset the content.
        var isChecked = checkbox.is(':checked');
        var id = checkbox.attr('data-cms-content-sync-edit-override-id');
        checkbox.hide();
        checkbox.parent().find("label").hide();

        // Element was reloaded through AJAX which means it's using a different
        // custom ID now, but we have to restore the previous state. Otherwise
        // the user will have to click the same override button again.
        if (overridden[name]) {
          showFieldGroups(checkbox, true);
          if (!isChecked) {
            checkbox.prop("checked", true);
          }
          return;
        }

        showFieldGroups(checkbox, false);

        var elements = $('.cms-content-sync-edit-override-id-' + id);
        elements.addClass('cms-content-sync-edit-override-hide');

        checkbox.click(function (e) {
          var override = checkbox.is(':checked');
          showFieldGroups(checkbox, override);
          if (override) {
            elements.removeClass('cms-content-sync-edit-override-hide');
          } else {
            elements.addClass('cms-content-sync-edit-override-hide');
          }
        });

        var container = $("<div />");
        if(isChecked) {
          $('<button type="button" class="button" />')
            .text("Update locally")
            .click(function () {
              container.empty();

              showFieldGroups(checkbox, true);
              overridden[name] = true;

              var elements = $('.cms-content-sync-edit-override-id-' + id);
              elements.removeClass('cms-content-sync-edit-override-hide');
            })
            .appendTo(container);
          $('<button type="button" class="button button--danger" />')
            .text("Remove local changes")
            .click(function () {
              checkbox.prop('checked', false);

              container.empty();
              $('<span style="padding:10px; color: #f00;" />')
                .text("Your local changes will be removed when you submit the form.")
                .appendTo(container);

              var elements = $('.cms-content-sync-edit-override-id-' + id);
              elements.addClass('cms-content-sync-edit-override-hide');
            })
            .appendTo(container);
        }
        else {
          $('<button type="button" class="button" />')
            .text("Make local changes")
            .click(function () {
              checkbox.prop('checked', true);

              container.empty();
              $('<span style="padding:10px; color: #0f0;" />')
                .text("You can now make local changes that will persist. Remote updates to this content will be ignored.")
                .appendTo(container);

              showFieldGroups(checkbox, true);
              overridden[name] = true;
              var elements = $('.cms-content-sync-edit-override-id-' + id);
              elements.removeClass('cms-content-sync-edit-override-hide');
            })
            .appendTo(container);
        }

        container.appendTo(checkbox.parent());
      });

      $('.cms-content-sync-edit-override-disabled', context).each(function () {
        var element = $(this);

        if (element.data("cms-content-sync--processed-override-disabled")) {
          return;
        }
        element.data("cms-content-sync--processed-override-disabled", true);

        if (!element.is(':disabled')) {
          element = element.find(':disabled');
        }
        element
          .not(':button')
          .removeAttr('disabled')
          .attr('readonly', 'readonly');
      });

      $(context)
        .find('#ajax-pool-selector-wrapper')
        .once('content-sync-pool-search')
        .each(function () {
          function update(e) {
            var text = input.val();
            parent.find('label').each(function () {
              var label = $(this);
              var checkbox = label.siblings('#' + label.attr("for"));

              if (!text || label.text().toLowerCase().indexOf(text) >= 0) {
                label.show();
                checkbox.show();
              } else {
                label.hide();
                checkbox.hide();
              }
            });
          }

          var container = $(this);
          var parent = container.find('.form-checkboxes');

          var labels = parent.find('label');

          // Don't show for select boxes
          // Don't show for less than 10 checkboxes / radios
          if (labels.length < 10) {
            return;
          }

          var input = $('<input type="text" placeholder="search..." />')
            .keyup(update)
            .keypress(update)
            .change(update)
            .prependTo(parent);
        });
    }
  };

})(jQuery, drupalSettings);
