<?php

namespace Drupal\cms_content_sync_views\Plugin\Action;

use Drupal\cms_content_sync\Controller\Migration;
use Drupal\cms_content_sync\Plugin\Type\EntityHandlerPluginManager;

/**
 * Push entity of status entity.
 *
 * @Action(
 *   id = "push_status_entity_to_v2",
 *   label = @Translation("Test push to v2"),
 *   type = "cms_content_sync_entity_status"
 * )
 */
class PushStatusEntityToV2 extends PushStatusEntity
{
    /**
     * {@inheritdoc}
     */
    public function execute($entity = null)
    {
        /** @var \Drupal\cms_content_sync\Entity\EntityStatus $entity */
        if (is_null($entity)) {
            return;
        }

        try {
            $source = $entity->getEntity();
            if (empty($source)) {
                \Drupal::messenger()->addMessage(t('The Entity @type @uuid doesn\'t exist locally, push skipped.', [
                    '@type' => $entity->get('entity_type')->getValue()[0]['value'],
                    '@uuid' => $entity->get('entity_uuid')->getValue()[0]['value'],
                ]), 'warning');

                return;
            }

            $pool = $entity->getPool();
            if (!$pool->v2Ready()) {
                \Drupal::messenger()->addMessage(t('The Pool for @type %label has not been exported to the new Sync Core, push skipped. Please go to Admin > Configuration > Web services > Content Sync to export the Pool first.', [
                    '@type' => $entity->get('entity_type')->getValue()[0]['value'],
                    '%label' => $source->label(),
                ]), 'warning');

                return;
            }

            $flow = $entity->getFlow();
            if (!$flow->v2Ready()) {
                \Drupal::messenger()->addMessage(t('The Flow for @type %label has not been exported to the new Sync Core, push skipped. Please go to Admin > Configuration > Web services > Content Sync to export the Flow first.', [
                    '@type' => $entity->get('entity_type')->getValue()[0]['value'],
                    '%label' => $source->label(),
                ]), 'warning');

                return;
            }

            Migration::useV2(true);
            // Reset client cache to avoid sending our requests to v1.
            $pool->getClient(true);
            parent::execute($entity);
            // Reset client cache to avoid sending additional requests to v2 after switching back.
            $pool->getClient(true);
            Migration::useV2(false);

            Migration::entityUsedV2($flow->id, $source->getEntityTypeId(), $source->bundle(), $source->uuid(), EntityHandlerPluginManager::getIdOrNull($source), true);
        } finally {
            Migration::useV2(false);
        }
    }
}
