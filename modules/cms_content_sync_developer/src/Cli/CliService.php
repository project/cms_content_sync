<?php

namespace Drupal\cms_content_sync_developer\Cli;

use Drupal\cms_content_sync\Entity\Flow;
use Drush\Exceptions\UserAbortException;

class CliService
{
    public static $forceEntityDeletion = false;

    /**
     * Update the local entity type versions, so add unknown fields for example.
     *
     * @param $io
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function configuration_export($io)
    {
        $flows = Flow::getAll(false);

        foreach ($flows as $flow) {
            $flow->getController()->updateEntityTypeVersions();
            $flow->resetVersionWarning();
        }

        $io->text('Flows updated');
    }

    /**
     * Force the deletion of an entities and skip the syndication.
     *
     * @param $io
     * @param $entity_type
     * @param $options
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Drupal\Core\Entity\EntityStorageException
     * @throws \Drush\Exceptions\UserAbortException
     */
    public function force_entity_deletion($io, $entity_type, $options)
    {
        self::$forceEntityDeletion = true;

        $bundle = $options['bundle'];
        $entity_uuid = $options['entity_uuid'];

        if ((isset($bundle, $entity_uuid)) || (!isset($bundle) && !isset($entity_uuid))) {
            $io->error('Either the bundle OR the entity_uuid option must be set.');

            return;
        }

        if (isset($entity_uuid)) {
            $entity = \Drupal::service('entity.repository')->loadEntityByUuid($entity_type, $entity_uuid);

            if (!$entity) {
                $io->error('An entity of type '.$entity_type.' having the uuid '.$entity_uuid.' does not exist.');

                return;
            }

            if (!$io->confirm(dt('Do you really want to delete the entity of type '.$entity_type.' having the uuid: '.$entity_uuid.' '))) {
                throw new UserAbortException();
            }

            $entity->delete();
            $io->success('The '.$entity_type.' having the uuid '.$entity_uuid.' has been deleted.');

            return;
        }

        if (isset($bundle)) {
            if (!$io->confirm(dt('Do you really want to delete all entities of the type: '.$entity_type.' having the bundle: '.$bundle.' ?'))) {
                throw new UserAbortException();
            }

            $bundle_key = \Drupal::entityTypeManager()
                ->getStorage($entity_type)
                ->getEntityType()->getKey('bundle');

            if ('menu_link_content' == $entity_type) {
                $bundle_key = 'menu_name';
            }

            $entities = \Drupal::entityTypeManager()
                ->getStorage($entity_type)
                ->loadByProperties([$bundle_key => $bundle]);

            foreach ($entities as $entity) {
                $entity->delete();
            }

            $io->success('All entities of type: '.$entity_type.' and bundle: '.$bundle.' have been deleted.');

            return;
        }
    }
}
