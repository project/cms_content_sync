<?php

namespace Drupal\cms_content_sync_migrate_acquia_content_hub;

use Drupal\cms_content_sync\Entity\EntityStatus;
use Drupal\cms_content_sync\Entity\Flow;
use Drupal\cms_content_sync\PushIntent;
use Drupal\cms_content_sync_migrate_acquia_content_hub\Form\MigrationBase;
use Drupal\Core\Controller\ControllerBase;

class CreateStatusEntities extends ControllerBase
{
    /**
     * Collect relevant nodes.
     *
     * @param $flow_id
     * @param $flow_configurations
     * @param $pools
     * @param $type
     * @param bool $execute
     * @param mixed $pool_id
     * @param mixed $tags
     */
    public function prepare($flow_id, $flow_configurations, $pool_id, $type, $tags = '')
    {
        $operations = [];

        if ('push' == $type) {
            foreach ($flow_configurations as $type => $type_config) {
                foreach ($type_config as $bundle => $bundle_config) {
                    if (PushIntent::PUSH_AUTOMATICALLY != $bundle_config['push_configuration']['behavior']) {
                        continue;
                    }

                    $entity_type = \Drupal::entityTypeManager()->getDefinition($type);
                    $ids = \Drupal::entityQuery($type)->condition($entity_type->getKey('bundle'), $bundle)->execute();
                    foreach ($ids as $id) {
                        $operations[] = [
                            __NAMESPACE__.'\CreateStatusEntities::execute',
                            [$type, $id, $flow_id, $pool_id, 'push'],
                        ];
                    }
                }
            }

            return $operations;
        }

        $tags = MigrationBase::getTermsFromFilter($tags);
        if (empty($tags)) {
            return $operations;
        }

        $ids = [];
        foreach ($tags as $tag) {
            $ids[] = $tag->id();
        }

        $query = \Drupal::database()->select('taxonomy_index', 'ti');
        $query->fields('ti', ['nid']);
        $query->condition('ti.tid', $ids, 'IN');
        $result = $query->execute()->fetchCol();

        foreach ($result as $nid) {
            $operations[] = [
                __NAMESPACE__.'\CreateStatusEntities::execute',
                ['node', $nid, $flow_id, $pool_id, 'pull'],
            ];
        }

        return $operations;
    }

    /**
     * Batch create Status Entities for collected nodes.
     *
     * @param $nid
     * @param $flow_id
     * @param $bundle_id
     * @param $pools
     * @param $field_name
     * @param $type
     * @param mixed $entity_type
     * @param mixed $entity_id
     * @param mixed $pool_id
     */
    public static function execute($entity_type, $entity_id, $flow_id, $pool_id, $type)
    {
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);

        // Drupal doesn't reliably update their taxonomy_index table when a node gets deleted.
        if (!$entity) {
            return;
        }

        // If a node has a match, create a status entity.
        // Ensure that a status entity does not already exist.
        $entity_status = EntityStatus::getInfoForEntity($entity_type, $entity->uuid(), $flow_id, $pool_id);
        if (!$entity_status) {
            $data = [
                'flow' => $flow_id,
                'pool' => $pool_id,
                'entity_type' => $entity_type,
                'entity_uuid' => $entity->uuid(),
                'entity_type_version' => Flow::getEntityTypeVersion($entity_type, $entity->bundle()),
                'flags' => 0,
                'source_url' => null,
            ];

            if ('node' == $entity_type && 'pull' == $type) {
                $data['last_'.$type] = $entity->getChangedTime();
            }

            $entity_status = EntityStatus::create($data);

            if ('push' == $type) {
                $entity_status->isPushEnabled(true);
                $entity_status->isSourceEntity(true);
            }

            $entity_status->save();
        }
    }
}
