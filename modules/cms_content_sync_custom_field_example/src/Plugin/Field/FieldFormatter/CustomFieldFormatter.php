<?php

namespace Drupal\cms_content_sync_custom_field_example\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'CustomFieldFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "cs_custom_field_formatter",
 *   label = @Translation("Content Sync - Custom Field Formatter"),
 *   field_types = {
 *     "cs_custom_field",
 *   }
 * )
 */
class CustomFieldFormatter extends FormatterBase
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];

        foreach ($items as $delta => $item) {
            if (isset($item->cs_text_field)) {
                $elements[$delta] = [
                    '#type' => 'processed_text',
                    '#text' => $item->cs_text_field,
                    '#langcode' => $item->getLangcode(),
                ];
            }
        }

        return $elements;
    }
}
