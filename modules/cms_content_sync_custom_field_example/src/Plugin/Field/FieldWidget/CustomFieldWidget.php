<?php

/**
 * @file
 * Contains \Drupal\cms_content_sync_custom_field_example\Plugin\Field\FieldWidget\CustomFieldExampleWidget.
 */

namespace Drupal\cms_content_sync_custom_field_example\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'CustomFieldWidget' widget.
 *
 * @FieldWidget(
 *   id = "cs_custom_field_widget",
 *   label = @Translation("Content Sync - Custom Field Widget"),
 *   field_types = {
 *     "cs_custom_field"
 *   }
 * )
 */
class CustomFieldWidget extends WidgetBase
{
    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
    {
        $element['cs_text_field'] = [
            '#title' => $this->t('Content Sync - Text Field'),
            '#type' => 'textarea',
            '#default_value' => isset($items[$delta]->cs_text_field) ? $items[$delta]->cs_text_field : null,
        ];

        return $element;
    }
}
