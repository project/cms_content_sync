<?php

/**
 * @file
 * Contains \Drupal\cms_content_sync_custom_field_example\Plugin\Field\FieldType\CustomField.
 */

namespace Drupal\cms_content_sync_custom_field_example\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'CustomField' field type.
 *
 * @FieldType(
 *   id = "cs_custom_field",
 *   label = @Translation("Content Sync - Custom Field"),
 *   description = @Translation("Content Sync - Custom field."),
 *   default_widget = "cs_custom_field_widget",
 *   default_formatter = "cs_custom_field_formatter",
 * )
 */
class CustomField extends FieldItemBase
{
    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field)
    {
        return [
            'columns' => [
                'cs_text_field' => [
                    'type' => 'text',
                    'size' => 'big',
                    'not null' => false,
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
        $value = $this->get('cs_text_field')->getValue();

        return null === $value || '' === $value;
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
    {
        $properties['cs_text_field'] = DataDefinition::create('string')
            ->setLabel(t('Content Sync - Text Field'));

        return $properties;
    }
}
