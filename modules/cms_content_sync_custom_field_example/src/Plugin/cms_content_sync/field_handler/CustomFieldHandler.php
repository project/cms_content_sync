<?php

namespace Drupal\cms_content_sync_custom_field_example\Plugin\cms_content_sync\field_handler;

use Drupal\cms_content_sync\Plugin\cms_content_sync\field_handler\DefaultFieldHandler;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Providing a field handler for the custom field.
 *
 * @FieldHandler(
 *   id = "cms_content_sync_custom_field_handler",
 *   label = @Translation("Custom"),
 *   weight = 50
 * )
 */
class CustomFieldHandler extends DefaultFieldHandler
{
    /**
     * {@inheritdoc}
     */
    public static function supports($entity_type, $bundle, $field_name, FieldDefinitionInterface $field)
    {
        $custom_field_types = [
            'cs_custom_field',
        ];

        // Return TRUE for this handler to support the provided field. You usually want to make this decision based on the field type (in our case "cs_custom_field"), but you can also use other information like the field name.
        return in_array($field->getType(), ['cs_custom_field']);
    }
}
