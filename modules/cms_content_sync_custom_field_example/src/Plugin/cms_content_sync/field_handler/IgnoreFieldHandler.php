<?php

namespace Drupal\cms_content_sync_custom_field_example\Plugin\cms_content_sync\field_handler;

use Drupal\cms_content_sync\Plugin\FieldHandlerBase;
use Drupal\cms_content_sync\PullIntent;
use Drupal\cms_content_sync\PushIntent;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Providing an example how to ignore a specific field.
 *
 * Ensure to set the weight within the FieldHandler description lower then
 * "90" to override the default content sync field handler.
 *
 * @FieldHandler(
 *   id = "cms_content_sync_ignore_field_handler",
 *   label = @Translation("Ignore Field"),
 *   weight = 80
 * )
 */
class IgnoreFieldHandler extends FieldHandlerBase
{
    /**
     * Ignore the field "field_ignore_example" from being pushed and pulled.
     *
     * @param mixed $entity_type
     * @param mixed $bundle
     * @param mixed $field_name
     */
    public static function supports($entity_type, $bundle, $field_name, FieldDefinitionInterface $field)
    {
        return 'field_ignore_example' == $field_name;
    }

    /**
     * Overwrite the default content sync pull method.
     */
    public function pull(PullIntent $intent)
    {
        // Do nothing and just return true so Content Sync knows everything is fine.
        return true;
    }

    /**
     * Overwrite the default content sync push method.
     */
    public function push(PushIntent $intent)
    {
        // If the field is required, we need to send a value to the Sync Core.
        // Otherwise the entity can't be pushed as it violates the entity type
        // definition. You can also set any dummy value here instead of the real
        // value if you don't want to send it to the Sync Core.
        if ($this->fieldDefinition->isRequired()) {
            return parent::push($intent);
        }

        return true;
    }
}
